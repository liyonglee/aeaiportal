<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link rel="stylesheet" href="css/jPagesStyle.css">
<link rel="stylesheet" href="css/jPages.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/pagination.css" />
<style>
.tooltipsy li{
list-style: none;
}
</style>
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/jquery.pagination.js"></script>
<script type="text/javascript" src="js/tooltipsy.min.js"></script>
<script language="javascript">
function refreshContent(curNodeId){
	if (curNodeId){
		$('#columnId').val(curNodeId);
	}
	doSubmit({actionType:'query'});
}
var selectedImage;
function selectPicture(imgObj,resId){
	$("#RES_ID").val(resId);
	if (selectedImage){
		selectedImage.css({'border':"0px solid #FFCC66"});
	}
	selectedImage = $(imgObj);
	selectedImage.css({'border':"2px solid #FFCC66"}); 
}

function showPicture(resId){
	var reqUrl = "/portal/index?WcmPictureResourceEdit&actionType=getImageObject&resId="+resId;
	sendRequest(reqUrl,{onComplete:function(responseText){
		var selectImgObject = jQuery.parseJSON(responseText);
		parent.afterSelectImage(selectImgObject);
		parent.PopupBox.closeCurrent();	
	}});
}    
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;padding:0px;" cellpadding="1" cellspacing="0">
<tr>
	<td valign="top">
    <div class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">&nbsp;&nbsp;图片分组列表</h3>
    <div id="treeArea" style="overflow:auto; height:445px;width:203px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
    <%=pageBean.getStringValue("menuTreeSyntax")%></div>
    </div>
    <b class="b9"></b>
    </div>
    <input type="hidden" id="columnId" name="columnId" value="<%=pageBean.inputValue("columnId")%>" />
    <input type="hidden" id="targetParentId" name="targetParentId" value="" />
    </td>
	<td width="85%" valign="top">
<div id="content" class="defaults">
<ul id="itemContainer" style="min-height: 450px;">
<%=pageBean.inputValue("imageItemsContent")%>
</ul>
<div id="Pagination" class="pagination">
</div>
</div>

<div id="filterBox" class="sharp color2" style="position:absolute;top:30px;display:none; z-index:10; width:480px;">
<b class="b9"></b>
<div class="content">
<h3>&nbsp;&nbsp;条件过滤框</h3>
<table class="detailTable" cellpadding="0" cellspacing="0" style="width:99%;margin:1px;">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="resName" label="名称" name="resName" type="text" value="<%=pageBean.inputValue("resName")%>" size="24" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>后缀</th>
	<td><input id="suffix" label="后缀" name="suffix" type="text" value="<%=pageBean.inputValue("suffix")%>" size="24" class="text" /></td>
</tr>
</table>
<div style="width:100%;text-align:center;">
<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
&nbsp;&nbsp;
<input type="button" name="button" id="button" value="清空" class="formbutton" onclick="clearFilter()" />
&nbsp;&nbsp;<input type="button" name="button" id="button" value="关闭" class="formbutton" onclick="javascript:$('#filterBox').hide();" /></div>
</div>
<b class="b9"></b>
</div>
<input type="hidden" id="_tabId_" name="_tabId_" value="<%=pageBean.inputValue("_tabId_")%>" />
<input type="hidden" name="RES_ID" id="RES_ID" value="" />
<input type="hidden" name="curColumnId" id="curColumnId" value="" />
<script language="JavaScript">
setRsIdTag('RES_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
</form>
</body>
</html>
<script language="javascript">
$(function(){
	var recCountPerPage = 15;
	var mainNumCount = 8;
	var sideNumCount = 2;
	var recordsCount = <%=pageBean.inputValue("recordsCount")%>;
	function pageselectCallback(page_index, jq){
		var items_per_page = recCountPerPage;
		var startIndex = page_index*items_per_page;
		var endIndex = Math.min((page_index+1) * items_per_page,recordsCount);
		$("#itemContainer li").hide();
		$("#itemContainer li").slice(startIndex,endIndex).show(); 
		return false;
	}
	var optInit = {callback: pageselectCallback,items_per_page:recCountPerPage,num_display_entries:mainNumCount,num_edge_entries:sideNumCount,prev_text:'向前',next_text:'向后'}
	$("#Pagination").pagination(recordsCount,optInit);

    $("#itemContainer li img").tooltipsy({
        alignTo: 'cursor',
        offset: [10, 10],    	
        content: function ($el, $tip) {
            var options = jQuery.parseJSON($el.attr('options'));
            var temp = "<li>名称:"+options.name+"</li><li>宽度:"+options.width+"</li><li>高度:"+options.height+"</li><li>大小:"+options.size+"</li>";
        	return temp;
        },    	
        css: {
            'padding': '10px',
            'max-width': '200px',
            'color': '#303030',
            'background-color': '#f5f5b5',
            'border': '1px solid #deca7e',
            '-moz-box-shadow': '0 0 10px rgba(0, 0, 0, .5)',
            '-webkit-box-shadow': '0 0 10px rgba(0, 0, 0, .5)',
            'box-shadow': '0 0 10px rgba(0, 0, 0, .5)',
            'text-shadow': 'none'
        }
    });
});
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveRecord(){
	if (!validate()){
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if (responseText == 'success'){
			parent.refreshContent();
			parent.PopupBox.closeCurrent();
		}
	}});	
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="RES_NAME" label="名称" name="RES_NAME" type="text" value="<%=pageBean.inputValue("RES_NAME")%>" size="24" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>宽度</th>
	<td><input id="RES_WIDTH" label="宽度" name="RES_WIDTH" type="text" value="<%=pageBean.inputValue("RES_WIDTH")%>" size="24" class="text" />px
</td>
</tr>
<tr>
	<th width="100" nowrap>高度</th>
	<td><input id="RES_HEIGHT" label="高度" name="RES_HEIGHT" type="text" value="<%=pageBean.inputValue("RES_HEIGHT")%>" size="24" class="text" />px
</td>
</tr>
<tr>
	<th width="100" nowrap>大小</th>
	<td><input id="RES_SIZE" label="大小" name="RES_SIZE" type="text" value="<%=pageBean.inputValue("RES_SIZE")%>" size="24" class="text" />byte</td>
</tr>
<tr>
	<th width="100" nowrap>后缀</th>
	<td><input id="RES_SUFFIX" label="后缀" name="RES_SUFFIX" type="text" value="<%=pageBean.inputValue("RES_SUFFIX")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>图片路径</th>
	<td><input id="RES_LOCATION" label="路径" name="RES_LOCATION" type="text" value="<%=pageBean.inputValue("RES_LOCATION")%>" size="60" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>缩略图路径</th>
	<td><input name="RES_THUMB_LOCATION" type="text" class="textarea" id="RES_THUMB_LOCATION" value="<%=pageBean.inputValue("RES_THUMB_LOCATION")%>" size="60" label="描述" /></td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" name="GRP_ID" id="GRP_ID" value="<%=pageBean.inputValue("GRP_ID")%>"/>
<input type="hidden" id="RES_ID" name="RES_ID" value="<%=pageBean.inputValue4DetailOrUpdate("RES_ID","")%>" />
<input type="hidden" id="GRP_ID" name="GRP_ID" value="<%=pageBean.inputValue("GRP_ID")%>" />
</form>
<script language="javascript">
lengthValidators[0].set(64).add("RES_NAME");
intValidator.add("RES_WIDTH");
intValidator.add("RES_HEIGHT");
lengthValidators[1].set(64).add("RES_SIZE");
lengthValidators[2].set(32).add("RES_SUFFIX");
lengthValidators[3].set(256).add("RES_LOCATION");
lengthValidators[4].set(256).add("RES_DESCRIPTION");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>wcm</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript" src="js/jquery.zclip.js"></script>
<script language="javascript">
function doRefresh(nodeId){
	$('#COL_ID').val(nodeId);
	doSubmit({actionType:'refresh'});
}
var targetTreeBox;
function showParentSelectBox(){
	if (!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标目录',{size:'normal',width:'300px',top:'3px'});
	}
	var handlerId = "ColumnParentSelect";
	var url = 'index?'+handlerId+'&COL_ID='+$("#COL_ID").val();
	targetTreeBox.sendRequest(url);
}
function doChangeParent(){
	postRequest('form1',{actionType:'changeParent',onComplete:function(responseText){
		if (responseText == 'success'){
			doRefresh($('#COL_ID').val());			
		}else {
			alert('迁移父节点出错啦！');
		}
	}});
}
function doSave(){
	if (checkSave()){
		$("#operaType").val('update');
		doSubmit({actionType:'save',checkUnique:'true'});
	}
}
function checkSave(){
	var result = true;
	if (validation.checkNull($('#COL_NAME').val())){
		writeErrorMsg($("#COL_NAME").attr("label")+"不能为空!");
		selectOrFocus('COL_NAME');
		return false;
	}
	if (validation.checkNull($('#PT_TITLE').val())){
		writeErrorMsg($("#PT_TITLE").attr("label")+"不能为空!");
		selectOrFocus('PT_TITLE');
		return false;
	}
	if (validation.checkNull($('#COL_CODE').val())){
		writeErrorMsg($("#COL_CODE").attr("label")+"不能为空!");
		selectOrFocus('COL_CODE');
		return false;
	}	
	var reg = /^[0-9a-z-]+$/;
	if (!reg.test($("#COL_CODE").val())) {
		writeErrorMsg($("#COL_CODE").attr("label")+"应为数字或者小写字母组成！");
		selectOrFocus('COL_CODE');
		return false;
	}		
	return result;
}
function doMoveUp(){
	doSubmit({actionType:'moveUp'});
}
function doMoveDown(){
	doSubmit({actionType:'moveDown'});
}
function doCopyCurrent(){
	doSubmit({actionType:'copyCurrent'});
}
function doDelete(){
	if (confirm('确定要进行节点删除操做吗？')){
		var url = '<%=pageBean.getHandlerURL()%>&actionType=isExistRelation&COL_ID='+$("#COL_ID").val();
		sendRequest(url,{onComplete:function(responseText){
			if (responseText == 'existSecurityRelation'){
				writeErrorMsg('该分组存在授权关联，请先删除授权关联！');
			}
			else if (responseText == 'existInfomationRelation'){
				writeErrorMsg('该分组存在关联信息，请先删除关联信息！');
			}
			else{
				doSubmit({actionType:'delete'});
			}
		}});
	}
}
function doInsertChild(){
	if (checkInsertChild()){
		$("#operaType").val('insert');
		doSubmit({actionType:'insertChild'});
	}
}
function checkInsertChild(){
	var result = true;
	if (validation.checkNull($('#CHILD_COL_NAME').val())){
		writeErrorMsg($("#CHILD_COL_NAME").attr("label")+"不能为空!");
		selectOrFocus('CHILD_COL_NAME');
		return false;
	}
	if (validation.checkNull($('#CHILD_PT_TITLE').val())){
		writeErrorMsg($("#CHILD_PT_TITLE").attr("label")+"不能为空!");
		selectOrFocus('CHILD_PT_TITLE');
		return false;
	}
	if (validation.checkNull($('#CHILD_COL_CODE').val())){
		writeErrorMsg($("#CHILD_COL_CODE").attr("label")+"不能为空!");
		selectOrFocus('CHILD_COL_CODE');
		return false;
	}	
	var reg = /^[0-9a-z-]+$/;
	if (!reg.test($("#CHILD_COL_CODE").val())) {
		writeErrorMsg($("#CHILD_COL_CODE").attr("label")+"应为数字或者小写字母组成！");
		selectOrFocus('CHILD_COL_CODE');
		return false;
	}	
	return result;
}
function doCancel(){
	doRefresh($('#COL_ID').val());
}
function focusTab(tabId){
	var reqUrl = "<%=pageBean.getHandlerURL()%>&actionType=focusTab&currentTabId="+tabId;
	sendRequest(reqUrl,{onComplete:function(responseText){
		
	}});
}
function showSecurityConfig(){
	var url = "index?SecurityAuthorizationConfig&resourceType=InfoColumn&resourceId=<%=pageBean.inputValue("COL_ID")%>&randomKey="+Math.random();
	$('#SecurityFrame').attr('src',url);
}

var addRefColumnsBox;
function addRefColumns(){
	var title = "关联栏目";
	if (!addRefColumnsBox){
		addRefColumnsBox = new PopupBox('addRefColumnsBox',title,{size:'big',width:'300px',height:'400px',top:'3px'});
	}
    var url = "/portal/index?RelColumnsSelect"; 
    addRefColumnsBox.sendRequest(url);	
}

function setupRefColumns(columns){
	var reqURL = "<%=pageBean.getHandlerURL()%>&actionType=setupRefColumns&COL_ID=<%=pageBean.inputValue("COL_ID")%>&newRelColumns="+columns;
	sendRequest(reqURL,{onComplete:function(responseText){
		eval(responseText);
		PopupBox.closeCurrent();
	}});	
}

function moveUpRefColumn(){
	if (!isValid($("#refColumnIds").val())){
		writeErrorMsg('请先选中一条关联栏目!');
		return;
	}
	if (ele("refColumnIds").selectedIndex == 0){
		writeErrorMsg('该关联栏目是第一条，不能向上！');
		return;
	}
	postRequest('form1',{actionType:'moveUpRefColumn',onComplete:function(responseText){
		eval(responseText);
	}});	
}
function moveDownRefColumn(){
	if (!isValid($("#refColumnIds").val())){
		writeErrorMsg('请先选中一条关联栏目!');
		return;
	}
	if (ele("refColumnIds").selectedIndex == (ele("refColumnIds").options.length-1)){
		writeErrorMsg('该关联栏目是最末条，不能向下！');
		return;
	}	
	postRequest('form1',{actionType:'moveDownRefColumn',onComplete:function(responseText){
		eval(responseText);
	}});	
}
function delRefColumn(){
	if (!isValid($("#refColumnIds").val())){
		writeErrorMsg('请先选中一条关联栏目!');
		return;
	}
	if (confirm("确定要删除该栏目关联么？")){
		postRequest('form1',{actionType:'delRefColumn',onComplete:function(responseText){
			eval(responseText);
		}});		
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;">
<tr>
	<td valign="top">
	<div id="leftTree" class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">&nbsp;&nbsp;分组列表</h3>        
	<div id="treeArea" style="overflow:auto; height:420px;width:230px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
	<%=pageBean.getStringValue("menuTreeSyntax")%></div>
    <b class="b9"></b>
    </div>
	</td>
	<td width="85%" valign="top">
<div class="photobg1" id="tabHeader">
<div class="newarticle1"  onclick="focusTab('base')">基本信息</div>
<div class="newarticle1" onclick="showSecurityConfig();focusTab('security')">安全管理</div>
</div>    
<div class="photobox newarticlebox" id="Layer0" style="height:427px;">
	<fieldset style="padding:3px;margin-top:10px;">
	  <legend style="font-weight: bolder;">编辑当前节点</legend>
	    <div id="__ToolBar__" style="margin-top: 2px">
   		<table border="0" cellpadding="0" cellspacing="1">
	    <tr>
		<td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doSave()"<%}%> class="bartdx" hotKey="E" align="center"><input id="saveImgBtn" value="&nbsp;" title="保存" type="button" class="saveImgBtn" style="margin-right:0px;" />保存</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doDelete()"<%}%> class="bartdx" hotKey="D" align="center"><input id="delImgBtn" value="&nbsp;" title="删除" type="button" class="delImgBtn" style="margin-right:0px;" />删除</td>
		<td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doCopyCurrent()"<%}%> class="bartdx" align="center"><input id="copyImgBtn" value="&nbsp;" title="复制" type="button" class="copyImgBtn" style="margin-right:0px;" />复制</td>    
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="showParentSelectBox()"<%}%> class="bartdx" align="center"><input id="moveImgBtn" value="&nbsp;" title="迁移" type="button" class="moveImgBtn" style="margin-right:0px;" />迁移</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveUp()"<%}%> class="bartdx" align="center"><input id="upImgBtn" value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveDown()"<%}%> class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td>
        <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" ><a id="copyInfoId" style="display:inline-block;width:74px;height:14px;">复制信息ID</a></td>
	    </tr></table></div>    
	    <table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="COL_NAME" label="名称" name="COL_NAME" type="text" value="<%=pageBean.inputValue("COL_NAME")%>" size="30" class="text" />
</td>
	<th>标题</th>
	<td><input id="PT_TITLE" label="Portlet标题" name="PT_TITLE" type="text" value="<%=pageBean.inputValue("PT_TITLE")%>" size="30" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="COL_CODE" label="编码" name="COL_CODE" type="text" value="<%=pageBean.inputValue("COL_CODE")%>" size="30" class="text" />
</td>
	<th width="100" nowrap>启用</th>
	<td>&nbsp;<%=pageBean.selectRadio("COL_ENABLED")%></td>
</tr>
<tr>
	<th width="100" nowrap>页面</th>
	<td><input id="MENU_CODE" label="页面" name="MENU_CODE" type="text" value="<%=pageBean.inputValue("MENU_CODE")%>" size="30" class="text" /></td>
	<th>备注</th>
	<td><input name="COL_DESC" type="text" class="textarea" id="COL_DESC" value="<%=pageBean.inputValue("COL_DESC")%>" size="30" label="备注" /></td>
</tr>
<tr>
	<th width="100" nowrap>关联栏目</th>
	<td colspan="3">
	<span style="float:left"><select name="refColumnIds" size="6" id="refColumnIds" style="width:300px;"><%=pageBean.selectValue("refColumnIds")%>
      </select></span>
      <span>
      <input name="addKeyWordRef" type="button" value="添加" style="margin:2px;" onclick="addRefColumns()" />
      <br />
      <input name="delKeyWordRef" type="button" value="上移" style="margin:2px;" onclick="moveUpRefColumn()" />
      <br />
      <input name="delKeyWordRef" type="button" value="下移" style="margin:2px;" onclick="moveDownRefColumn()" />
      <br />
      <input name="delKeyWordRef" type="button" value="删除" style="margin:2px;" onclick="delRefColumn()" />
      </span>            
    </td>
</tr>
	    </table>
	</fieldset>	
    
	<fieldset style="margin-top: 10px;padding:3px;">
	  <legend style="font-weight: bolder;">添加子节点</legend>
	    <div id="__ToolBar__">
		<table border="0" cellpadding="0" cellspacing="1">
	    <tr>
		<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doInsertChild()"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" style="margin-right:0px;" />新增</td>
		<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doCancel()"><input value="&nbsp;" title="取消" type="button" class="cancelImgBtn" style="margin-right:0px;" />取消</td>    
	    </tr></table>
	    </div>     
	    <table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="CHILD_COL_NAME" label="名称" name="CHILD_COL_NAME" type="text" value="<%=pageBean.inputValue("CHILD_COL_NAME")%>" size="30" class="text" />
</td>
	<th>标题</th>
	<td><input id="CHILD_PT_TITLE" label="标题" name="CHILD_PT_TITLE" type="text" value="<%=pageBean.inputValue("CHILD_PT_TITLE")%>" size="30" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="CHILD_COL_CODE" label="编码" name="CHILD_COL_CODE" type="text" value="<%=pageBean.inputValue("CHILD_COL_CODE")%>" size="30" class="text" /></td>
	<th>启用</th>
	<td>&nbsp;<%=pageBean.selectRadio("CHILD_COL_ENABLED")%></td>
</tr>
	    </table>
	</fieldset>  
</div>
<div class="photobox newarticlebox" id="Layer1" style="height:427px;display:none;overflow:hidden;">
<iframe id="SecurityFrame" src="" width="100%" height="430" frameborder="0" scrolling="no"></iframe>
</div>            
    </td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value=""/>
<input type="hidden" id="COL_ID" name="COL_ID" value="<%=pageBean.inputValue("COL_ID")%>" />
<input type="hidden" id="COL_PID" name="COL_PID" value="<%=pageBean.inputValue("COL_PID")%>" />
<input type="hidden" id="COL_ORDERNO" name="COL_ORDERNO" value="<%=pageBean.inputValue("COL_ORDERNO")%>" />
</form>
</body>
</html>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
$(function(){
	if ("security" == "<%=pageBean.getStringValue("currentTabId")%>"){
		showSecurityConfig();	
		tab.focus(1);		
	}
	postRequest('form1',{actionType:'retrieveRefColumns',onComplete:function(responseText){
		eval(responseText);
	}});	
});
$(function(){
	$("#copyInfoId").zclip({
		path:"<%=request.getContextPath()%>/js/ZeroClipboard.swf",
		copy:$("#COL_ID").val()
	});
})
$(window).load(function() {
	setTimeout(function(){
		resetTabHeight(70);
		resetTreeHeight(70);
	},1);	
});
<%if(pageBean.getBoolValue("isRootNode")){%>
setImgDisabled('saveImgBtn',true);
setImgDisabled('delImgBtn',true);
setImgDisabled('copyImgBtn',true);
setImgDisabled('moveImgBtn',true);
setImgDisabled('upImgBtn',true);
setImgDisabled('downImgBtn',true);
<%}%>
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
<script language="javascript">
</script>

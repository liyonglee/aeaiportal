<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function selectRequest(idValue){
	doSubmit({actionType:'showImageUploadPage'});
}
function setSelectTempValue(idValue,nameValue){
	ele('columnId').value = idValue;
}
function doSelectRequest(){
	if (!isValid(ele('columnId').value)){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	selectRequest(ele('columnId').value,'');
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div style="font-size:14px;font-weight:bold;">请选择栏目</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" nowrap>
<div id="selectTreeContainer" style="height:312px;overflow-y:auto" ondblclick="doSelectRequest()">
<script language="javascript">
<%=pageBean.inputValue("pickTreeSyntax")%>
</script>
</div>	
	</td>
  </tr>
  <tr>
    <td align="center">
    <input class="formbutton" type="button" name="Button23" value="下一步" onclick="doSelectRequest()"/>&nbsp; &nbsp;
    <input class="formbutton" type="button" name="Button22" value="取消" onclick="javascript:parent.PopupBox.closeCurrent();"/></td>
  </tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="columnId" id="columnId" value="<%=pageBean.inputValue("columnId")%>" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>

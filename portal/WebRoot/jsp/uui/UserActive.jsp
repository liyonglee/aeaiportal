<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>数通畅联-用户注册</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<div style="margin:auto;width:690px;">
<br />
<br />
<br />
<br />
<%if (pageBean.getAttribute("errorMsg") != null){%>
<span style="font-size: 16px;color:red;">
<%=pageBean.getStringValue("errorMsg") %>
</span>
<%}else{ 
String userId = pageBean.inputValue("userId");
String activeRedirectURL = pageBean.inputValue("activeRedirectURL");
%>
<span style="font-size: 16px;">
<%=userId%>账户激活成功！请点击<a href="<%=activeRedirectURL%>">这里</a>登录！
</span>
<%}%>
</div>
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>数通畅联-密码找回</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function findPassword(){
	if (!validate()){
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'checkUserInfo',onComplete:function(responseText){
		//alert("responseText is " + responseText);
		if (responseText == ''){
			doSubmit({actionType:'findPassword'});		
		}else{
			hideSplash();
			changeValideCode();
			$("#valideCode").val("");
			writeErrorMsg(responseText);
		}
	}});
}
function changeValideCode(){
	document.getElementById('valideCodeImg').src = "safecode?"+Math.floor(Math.random()*100000);
}
</script>
</head>
<body>
<div style="margin:auto;width:690px;">
<br />
<br /><br /><br />
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="80" nowrap>用户名</th>
	<td><input name="USER_CODE" type="text" class="text" id="USER_CODE" value="" size="30" maxlength="15" label="用户名" /></td>
</tr>
<tr>
	<th width="80" nowrap>邮件</th>
	<td><input id="USER_MAIL" label="邮件" name="USER_MAIL" type="text" value="" size="30" maxlength="32" class="text" />注册时候填写的邮件</td>
</tr>
<tr>
	<th width="80" nowrap>验证码</th>
	<td><span style="float: left;padding-top:3px;"><input id="valideCode" label="验证码" name="valideCode" type="text" value="" size="30" maxlength="16" class="text" /></span><span><span><img id="valideCodeImg" onclick="changeValideCode();" title="点击切换验证码图片" src="safecode"></img></span></span></td>
</tr>
</table>
<div><input id="regButton" type="button" name="button" value="找回密码" style="height: 30px;width: 100px;font-size: 15px;font-weight: bolder;margin-top: 10px;" onclick="findPassword()" /></div>
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
<script language="javascript">
requiredValidator.add("USER_CODE");
requiredValidator.add("USER_MAIL");
requiredValidator.add("valideCode");
charNumValidator.add("USER_CODE");
emailValidator.add("USER_MAIL");

new BlankTrimer("USER_CODE");
new BlankTrimer("USER_MAIL");

$("#USER_CODE").focus();
</script>
</div>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>

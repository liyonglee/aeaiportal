<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String contentIdParamMode = (String)request.getAttribute("contentIdParamMode");
%>
<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
<tr>
      <td width="120">展现模板ID</td>
      <td>
        <input data-role="none" name="templateId" type="text" id="templateId" value="${templateId}" size="40" /></td>
    </tr>
	<tr>
      <td width="120">信息ID参数</td>
      <td>
        <input data-role="none" name="contentIdParamKey" type="text" id="contentIdParamKey" value="${contentIdParamKey}" size="60" /></td>
    </tr>
	<tr>
      <td width="120">参数方式</td>
      <td>
      <input data-role="none" type="radio" name="contentIdParamMode" id="byRetrieve" value="byRetrieve" />直接获取
      &nbsp;&nbsp;
	   <input data-role="none" type="radio" name="contentIdParamMode" id="byParse" value="byParse" />URL解析       
       </td>
    </tr>     
	<tr>
      <td colspan="2" align="center">
        <input data-role="none" type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input data-role="none" type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">
<%if ("byRetrieve".equals(contentIdParamMode)){%>
$('#<portlet:namespace/>Form #byRetrieve').attr("checked","checked");
<%}else{%>
$('#<portlet:namespace/>Form #byParse').attr("checked","checked");
<%}%>

$(document).ready(function(){
	var idParam1 = $('#<portlet:namespace/>Form #templateId').val();
	var url1 = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam1 + '&type=portlet';
	sendRequest(url1,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #templateId').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
});
</script>
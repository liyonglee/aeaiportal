<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<%@ page language="java" import="com.agileai.portal.bizmoduler.sso.SSOTicket" %>
<%@ page language="java" import="com.agileai.portal.bizmoduler.sso.SSOTicketEntry" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="refreshLastAcessTime" var="refreshLastAcessTimeURL">
	<portlet:param name="ssoAppId" value="${ssoAppId}"></portlet:param>
</portlet:resourceURL>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
String ssoAppId = (String)request.getAttribute("ssoAppId");
boolean isSessionExpired = pageBean.getBoolValue("isSessionExpired");
//System.out.println("isSessionExpired is " + isSessionExpired);
if (isSessionExpired){
SSOTicket ssoTicket = (SSOTicket)pageBean.getAttribute("ssoTicket");
%>
<form id="<portlet:namespace />form1" name="<portlet:namespace />form1" method="post" action="<%=ssoTicket.getAppURL()%>" target="<portlet:namespace />reloginFrame">
<%
if (ssoTicket != null){
	List<SSOTicketEntry> entryList = ssoTicket.getSsoTicketEntryList();
	for (int i=0;i < entryList.size();i++){
		SSOTicketEntry entry = entryList.get(i);
		String paramCode = entry.getParamCode();
		String paramValue = entry.getParamValue();
%>
<input type="hidden" name="<%=paramCode%>" value="<%=paramValue%>" />
<%}}%>
</form>
<iframe id="<portlet:namespace />reloginFrame" name="<portlet:namespace />reloginFrame" src="" width="1" height="0" frameborder="0"></iframe>
<iframe id="<portlet:namespace />contentFrame" src="" width="200" height="10" frameborder="0"></iframe>
<script language="javascript">
$('#<portlet:namespace />form1').submit();
$('#<portlet:namespace />reloginFrame').load(function(){
	sendAction('${refreshLastAcessTimeURL}',{onComplete:function(response){
		var targetURL = "${targetURL}";
		/*
		if (targetURL.endWith("&")){
			targetURL = targetURL + "randomnumber=" + Math.floor(Math.random()*100000);
		}
		else if (targetURL.endWith("?")){
			targetURL = targetURL + "randomnumber=" + Math.floor(Math.random()*100000); 
		}
		else{
			if (targetURL.indexOf("?") > 0){
				targetURL = targetURL + "&randomnumber="+ Math.floor(Math.random()*100000);	
			} else{
				targetURL = targetURL + "?randomnumber=" + Math.floor(Math.random()*100000);
			}
		}
		*/
		$('#<portlet:namespace />contentFrame').attr('src',targetURL);		
		$('#<portlet:namespace />contentFrame').attr('height','${height}');
		$('#<portlet:namespace />contentFrame').attr('width','${width}');
		
		$('#<portlet:namespace />contentFrame').load(function(){
			$("#<portlet:namespace />reloginFrame").remove();
		});
				
		
		if('${autoExtend}' == 'Y') {
		    $('#<portlet:namespace />contentFrame').load(function(){
				resetPageBodyHeight('<portlet:namespace />contentFrame')       
		    });
		}
	}});
});
</script>
<%}else{%>
<iframe id="<portlet:namespace />contentFrame" src="" width="100%" height="0" frameborder="0"></iframe>
<script language="javascript">
sendAction('${refreshLastAcessTimeURL}',{onComplete:function(response){
	var targetURL = "${targetURL}";
	/*
	if (targetURL.endWith("&")){
		targetURL = targetURL + "randomnumber=" + Math.floor(Math.random()*100000);
	}
	else if (targetURL.endWith("?")){
		targetURL = targetURL + "randomnumber=" + Math.floor(Math.random()*100000); 
	}
	else{
		if (targetURL.indexOf("?") > 0){
			targetURL = targetURL + "&randomnumber="+ Math.floor(Math.random()*100000);	
		} else{
			targetURL = targetURL + "?randomnumber=" + Math.floor(Math.random()*100000);
		}
	}
	*/
	$('#<portlet:namespace />contentFrame').attr('src',targetURL);
	$('#<portlet:namespace />contentFrame').attr('height','${height}');
	$('#<portlet:namespace />contentFrame').attr('width','${width}');
	if('${autoExtend}' == 'Y') {
	    $('#<portlet:namespace />contentFrame').load(function(){
			resetPageBodyHeight('<portlet:namespace />contentFrame')       
	    });
	}	
}});
</script>
<%}%>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>
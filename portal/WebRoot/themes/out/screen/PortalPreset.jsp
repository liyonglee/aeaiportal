<!doctype html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
String menuBarJson = menuBar.getMenuBarJson(null,menuItem.getCode(),request);
String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>大屏幕</title>
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/page.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/layout.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css" type="text/css" />	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/dcmegamenu.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skins/red.css" type="text/css" />		
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/boxy.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/gt_grid.css" type="text/css" />	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/china/skinstyle.css"  type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/vista/skinstyle.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/mac/skinstyle.css" type="text/css" />
<%if ("edit".equals(windowMode)){%>	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.qtip.css" type="text/css" />
<%}%>	
<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=menuItem.getCustomCssURL()%>" type="text/css" />
<%}%>			
    <style type="text/css">
	body, tbody, th, td{
		font-family:Verdana,Arial,Helvetica,sans-serif;
		font-size:12px;
		margin:0;
		padding:0;
	}
    </style>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery1.7.1.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.hoverIntent.minified.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.dcmegamenu.1.3.3.js" language="javascript"></script>       
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/FusionCharts_pc.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.iframe.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>    
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>
    <script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_msg_cn.js"></script>
	<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_grid_all.js"></script>
<%if ("edit".equals(windowMode)){%>	
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.qtip.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/imagesloaded.pkgd.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>	
<%}%>	
<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
	<script type="text/javascript" src="<%=menuItem.getCustomJsURL()%>"></script>
<%}%>	
	<script type="text/javascript">
	$.ajaxSetup({cache:false});
	
	var __renderPortlets = new Map();	
	var _directConfigPortletBox;
	function directConfigDecoratorRequest(portletId){
		if (!_directConfigPortletBox){
			_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'380px'});
		}
		var url = '/portal/index?PagePortletConfig&portletId='+portletId;
		_directConfigPortletBox.sendRequest(url);	
	}
	$(function(){
		<%if ("edit".equals(windowMode)){%>
		$(".content > h3").hover(
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeIn('fast');
		  }, 
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeOut('fast');
		  }
		);
		<%}%>
		$(".content > h3").find("span[id=stateAnchor]").hide();
		PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
	}); 	
	</script>  	
</head>
<body>
<div id="container">
    <div id="menubar" class="red"></div>
    <div id="pageBody">
	    <pt:layout />
    </div>
</div>
</body>
</html>
<script type="text/javascript">
var menudata = <%=menuBarJson%>;
var topMenuSelected;
var topMenuSelectedUrl;
var subMenuSelected;
var topMenuSelectedId;
var menuHtml = "";

function createMenu(){
	var data = menudata.menus;
	menuHtml = menuHtml + '<ul class="mega-menu">';
	for(var i = 0; i < data.length; i++) {
		var itemData = data[i];
		if (itemData.selected){
			topMenuSelected = itemData.text;
			topMenuSelectedUrl = itemData.href;
			topMenuSelectedId = itemData.name;
		}
		buildMenuHtml(itemData);
	}
	menuHtml = menuHtml + '</ul>'; 
	$('#menubar').append(menuHtml);
}

function buildMenuHtml(itemData){
	var name = itemData.name;
	var itemType = itemData.type;
	var itemText = itemData.text;
	var submenuHtml = "";
	if(itemType == 'folder'){
		menuHtml = menuHtml + '<li><a href="javascript:void(0)">'+itemText+'</a><ul>';
		for (var j=0;j < itemData.menus.length;j++){
			var submenuData = itemData.menus[j];
			buildMenuHtml(submenuData);
		}
		menuHtml = menuHtml + '</ul></li>'; 
	}
	else if (itemType == 'page'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li><a href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
	else if (itemType == 'link'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li><a hideFocus="true" href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
}

$(document).ready(function($){
	createMenu();
	$('.mega-menu').dcMegaMenu({
		rowItems: '3',
		speed: 'fast',
		effect: 'fade'
	});
});
</script>
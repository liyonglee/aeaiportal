<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.agileai.portal.driver.model.MenuItem"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="com.agileai.hotweb.common.BeanFactory" %>
<%@ page import="com.agileai.hotweb.domain.core.Profile"%>
<%@ page import="com.agileai.hotweb.domain.core.User"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%
Profile profile = (Profile)request.getSession(false).getAttribute(Profile.PROFILE_KEY);
User user = (User)profile.getUser();
boolean isAdmin = "admin".equals(user.getUserCode());
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
String menuBarJson = menuBar.getMenuBarJson(user,menuItem.getCode(),request);
String currentVisitPath = menuBar.getCurrentPath(request,menuItem);

String DefaultInNavId = BeanFactory.instance().getAppConfig().getConfig("GlobalConfig","DefaultInNavId");
String windowState= (String)request.getAttribute("WINDOW_STATE_KEY");
String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
String modeText = "view".equals(windowMode)?"编辑组件":"展示信息";
String hostIp = request.getRemoteAddr();
if ("0:0:0:0:0:0:0:1".equals(hostIp)){
	hostIp = "localhost";
}
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>门户管理控制台</title>
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/page.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/layout.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css" type="text/css" />	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/boxy.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/dcmegamenu.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skins/blue.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/gt_grid.css" type="text/css" />	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/china/skinstyle.css"  type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/vista/skinstyle.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/mac/skinstyle.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=menuItem.getCustomCssURL()%>" type="text/css" />
<%}%>		
    <style type="text/css">
	body, tbody, th, td{
		font-family:Verdana,Arial,Helvetica,sans-serif;
		font-size:12px;
		margin:0;
		padding:0;
	}
	body{
		background: url("/portal/images/sitelayout/bg/noise.png") repeat scroll 0 20px #FFFFFF;
	}	
    </style>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery1.7.1.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/FusionCharts_pc.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.hoverIntent.minified.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.dcmegamenu.1.3.3.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.iframe.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>    
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>    
    <script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_msg_cn.js"></script>
	<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_grid_all.js"></script>
<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
	<script type="text/javascript" src="<%=menuItem.getCustomJsURL()%>"></script>
<%}%>	
	<script type="text/javascript">
	$.ajaxSetup({cache:false});
	
	var __renderPortlets = new Map();	
	var turnRef = 63;
	<%if("normal".equals(windowState)){ %>
		turnRef = 171;
	<%}%>
	function resetPageBodyHeight(id){
		if (ele(id)){
			var frm=document.getElementById(id);
			var wantedHeight = $(window).height()-turnRef;
			//alert('wantedHeight is ' + wantedHeight);
			var subWeb=document.frames?document.frames[id].document:frm.contentDocument;
			if (document.getElementById){
				if (frm && !window.opera){
					if (frm.contentDocument && frm.contentDocument.body.offsetHeight){
						frm.height = frm.contentDocument.body.offsetHeight;
						if (frm.height < wantedHeight){					
							frm.height = wantedHeight+"px";
						}
					}else if(frm.Document && frm.Document.body.scrollHeight){
						if (subWeb.body.scrollHeight < wantedHeight){					
							frm.height = wantedHeight+"px";
						}
						else{
							frm.height = (subWeb.body.scrollHeight)+"px";
						}				
					}
				}
			}		
		}
	}
	var _directConfigPortletBox;
	function directConfigDecoratorRequest(portletId){
		if (!_directConfigPortletBox){
			_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'380px'});
		}
		var url = '/portal/index?PagePortletConfig&portletId='+portletId;
		_directConfigPortletBox.sendRequest(url);	
	}
	$(function(){
		$(".content > h3").hover(
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeIn('fast');
		  }, 
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeOut('fast');
		  }
		);
		$(".content > h3").find("span[id=stateAnchor]").hide();
		PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
	}); 	
	</script>    
</head>
<body onLoad="checkForRefresh()">
<input type="hidden" name="currentUserId" id="currentUserId" value="<%=user.getUserCode()%>" />
<div id="container" style="width:1200px;background-color: white;">
<%if("normal".equals(windowState)){ %>
    <div id="header"><span style="padding:0; margin:0px; text-align:left;"><img src="<c:out value="${pageContext.request.contextPath}"/>/images/sitelayout/logo.gif" height="60" /></span>
    </div>
<%} %>
	<div id="menubar" class="blue"></div>
    <div id="navigater" style="margin-bottom: 5px"><span id="logout" style="float:right">您好，<%=user.getUserName()%>&nbsp;&nbsp;<%if (isAdmin){%><a href="javascript:void(0)" class="button-link green" onClick="changeMode('<c:out value="${pageContext.request.contextPath}"/>')" ><%=modeText%></a><%}%>&nbsp;&nbsp;<a href="javascript:void(0)" class="button-link orange" onClick="javascript:window.location='/portal/index?Login&actionType=logout'">退出系统</a></span><span id="now_place">当前位置：<span style="color:#333;font-family:Verdana, Geneva, sans-serif;"><%=currentVisitPath%></span></span></div> 
    <div id="pageBody">
	    <pt:layout />
    </div>
<%if("normal".equals(windowState)){ %>
    <jsp:directive.include file="../includes/footer.jsp" />
<%} %>
</div>
</body>
</html>
<script type="text/javascript">
var menudata = <%=menuBarJson%>;
var topMenuSelected;
var topMenuSelectedUrl;
var subMenuSelected;
var topMenuSelectedId;
var menuHtml = "";

function createMenu(){
	var data = menudata.menus;
	menuHtml = menuHtml + '<ul class="mega-menu">';
	for(var i = 0; i < data.length; i++) {
		var itemData = data[i];
		if (itemData.selected){
			topMenuSelected = itemData.text;
			topMenuSelectedUrl = itemData.href;
			topMenuSelectedId = itemData.name;
		}
		buildMenuHtml(itemData);
	}
	menuHtml = menuHtml + '</ul>'; 
	$('#menubar').append(menuHtml);
}

function buildMenuHtml(itemData){
	var name = itemData.name;
	var itemType = itemData.type;
	var itemText = itemData.text;
	var submenuHtml = "";
	if(itemType == 'folder'){
		menuHtml = menuHtml + '<li><a href="javascript:void(0)">'+itemText+'</a><ul>';
		for (var j=0;j < itemData.menus.length;j++){
			var submenuData = itemData.menus[j];
			buildMenuHtml(submenuData);
		}
		menuHtml = menuHtml + '</ul></li>'; 
	}
	else if (itemType == 'page'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li><a href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
	else if (itemType == 'link'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li><a hideFocus="true" href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
}

$(document).ready(function($){
	createMenu();
	$('.mega-menu').dcMegaMenu({
		rowItems: '3',
		speed: 'fast',
		effect: 'fade'
	});
});
</script>
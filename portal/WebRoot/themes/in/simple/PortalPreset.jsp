<!doctype html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt"%>
<%@ page import="com.agileai.hotweb.domain.core.Profile"%>
<%@ page import="com.agileai.hotweb.domain.core.User"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%
Profile profile = (Profile)request.getSession(false).getAttribute(Profile.PROFILE_KEY);
User user = (User)profile.getUser();
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
String menuBarJson = menuBar.getMenuBarJson(user,menuItem.getCode(),request);
String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>最简主题</title>
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/page.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/layout.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css" type="text/css" />	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/dcmegamenu.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skins/red.css" type="text/css" />		
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/boxy.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/gt_grid.css" type="text/css" />	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/china/skinstyle.css"  type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/vista/skinstyle.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/mac/skinstyle.css" type="text/css" />
	<%if ("edit".equals(windowMode)){%>
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.qtip.css" type="text/css" />
	<%}%>	
<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=menuItem.getCustomCssURL()%>" type="text/css" />
<%}%>			
    <style type="text/css">
	body, tbody, th, td{
		font-family:Verdana,Arial,Helvetica,sans-serif;
		font-size:12px;
		margin:0;
		padding:0;
	}
    </style>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery1.7.1.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.hoverIntent.minified.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.dcmegamenu.1.3.3.js" language="javascript"></script>       
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/FusionCharts_pc.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.iframe.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>    
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>
    <script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_msg_cn.js"></script>
	<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_grid_all.js"></script>
<%if ("edit".equals(windowMode)){%>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.qtip.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/imagesloaded.pkgd.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>	
<%}%>	
<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
	<script type="text/javascript" src="<%=menuItem.getCustomJsURL()%>"></script>
<%}%>	
	<script type="text/javascript">
	$.ajaxSetup({cache:false});
	
	var __renderPortlets = new Map();	
	var _directConfigPortletBox;
	function directConfigDecoratorRequest(portletId){
		if (!_directConfigPortletBox){
			_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'380px'});
		}
		var url = '/portal/index?PagePortletConfig&portletId='+portletId;
		_directConfigPortletBox.sendRequest(url);	
	}
	$(function(){
		<%if ("edit".equals(windowMode)){%>
		$(".content > h3").hover(
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeIn('fast');
		  }, 
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeOut('fast');
		  }
		);
		<%}%>
		$(".content > h3").find("span[id=stateAnchor]").hide();
		PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
	}); 	
	</script>  	
</head>
<body>
<div id="container" style="width:99%">
<input type="hidden" name="currentUserId" id="currentUserId" value="<%=user.getUserCode()%>" />
    <div id="pageBody">
	    <pt:layout />
    </div>
</div>
</body>
</html>
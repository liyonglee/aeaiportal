var launchIframeWindow;

function submitAction(actionURL,params)
{
	var theForm;
	var theParam = "";

	if (params && params.formId){
		theForm = $("#"+params.formId);
	}else if (params && params.form){
		theForm = params.form;
	}else if (params && params.eventId){
		theForm = closestForm(params.eventId);
	}
	theForm.attr('action',actionURL);
	theForm.attr('method','post');	
	theForm.submit();
}

function fireAction(actionURL){
	window.location = actionURL;
}

function postAction(resourceURL,params)
{
	var theForm;
	var theParam = "";

	if (params && params.formId){
		theForm = $("#"+params.formId);
	}else if (params && params.form){
		theForm = params.form;
	}else if (params && params.eventId){
		theForm = closestForm(params.eventId);
	}
	
	var theParam = theForm.serialize();
	var asynchronousMode = true;
	if (params && params.asynchronous){
		asynchronousMode = stringToBoolean(params.asynchronous);
	}
	if (params && params.onComplete){
		$.ajax({url:resourceURL,type:'POST',data:theParam,async:asynchronousMode
			   ,dataType:'text',success:params.onComplete});
	}else{
		$.ajax({url:resourceURL,type:'POST',data:theParam,dataType:"script"});
	}
}
function sendAction(resourceURL,params) {
	var theParam = '';
	if (params){
		var asynchronousMode = true;
		if (params.asynchronous){
			asynchronousMode = stringToBoolean(params.asynchronous);
		}
		if (params.data){
			theParam = 	params.data;		
		}
		if (params.onComplete){
			var curDataType = "text";
			if (params.dataType){
				curDataType = params.dataType; 
			}
			$.ajax({url:resourceURL,type:'GET',data:theParam,async:asynchronousMode,dataType:curDataType,success:params.onComplete});			
		}else{
			$.ajax({url:resourceURL,type:'GET',data:theParam,async:asynchronousMode,dataType:"script"});		
		}
	}else{
		$.ajax({url:resourceURL,type:'GET',data:theParam,dataType:"script"});
	}
}
function closestForm(targetObjectOrId){
	if (typeof(targetObjectOrId) == 'string'){
		return $("#"+targetObjectOrId).closest('form');	
	}else{
		return $(targetObjectOrId).closest('form');
	}
}

function stringToBoolean(string){
	switch(string.toLowerCase()){
			case "true": case "yes": case "1": return true;
			case "false": case "no": case "0": case null: return false;
			default: return Boolean(string);
	}
}
String.prototype.replaceAll = function(s1,s2){   
	return this.replace(new RegExp(s1,"gm"),s2);   
};
String.prototype.endWith=function(s){
	if(s==null||s==""||this.length==0||s.length>this.length)
		return false;
	if(this.substring(this.length-s.length)==s)
		return true;
	else
		return false;
	return true;
}
String.prototype.startWith=function(s){
	if(s==null||s==""||this.length==0||s.length>this.length)
		return false;
	if(this.substr(0,s.length)==s)
		return true;
	else
		return false;
	return true;
}

function popupMonitorWindow(wmId){
	var boxTitle = "WEB图形监视画面";
	var boxWidth = document.body.clientWidth-20;
	var boxHeight = document.body.clientHeight-38;
	var targetURL = "/portal_portlets/index?WmDiagramDisplay&RID="+wmId+"&isMax=true";
	window.scrollTo(0,0);
	$(document.body).css('overflow','hidden');
	qBox.iFLoad({title:boxTitle,scrolling:'auto',src:targetURL,center:true,draggable: false,w:boxWidth,h:boxHeight,afterHide:function(){
		$(document.body).css('overflow','visible');
	}});
}

function popupGridFieldsConfig(pageId,portletId){
	var boxTitle = "表格字段配置";
	var targetURL = "/portal_portlets/index?GridFieldsConfigManageList&pageCode="+pageId+"&portletCode="+portletId;
	$(document.body).css('overflow','hidden');
	qBox.iFLoad({title:boxTitle,scrolling:'auto',src:targetURL,center:true,draggable: false,w:900,h:500,afterHide:function(){
		$(document.body).css('overflow','visible');
	}});
}

function popupQueryBarConfig(pageId,portletId){
	var boxTitle = "扩展查询配置";
	var targetURL = "/portal_portlets/index?ExtQueryBarConfigManageList&pageId="+pageId+"&portletId="+portletId;
	//$(document.body).css('overflow','hidden');
	qBox.iFLoad({title:boxTitle,scrolling:'auto',src:targetURL,center:true,draggable: false,w:900,h:500,afterHide:function(){
		//$(document.body).css('overflow','visible');
	}});
}

function popupLinkBarConfig(pageId,portletId){
	var boxTitle = "扩展链接";
	var targetURL = "/portal_portlets/index?ExtBarLinkManageList&pageId="+pageId+"&portletId="+portletId;
	//$(document.body).css('overflow','hidden');
	qBox.iFLoad({title:boxTitle,scrolling:'auto',src:targetURL,center:true,draggable: false,w:900,h:500,afterHide:function(){
		//$(document.body).css('overflow','visible');
	}});
}

function doAjaxRefreshSelf(params, portletId) {
	var datas = parsePortalParams(params);
	var targetPage = parsePageId();
	if (!targetPage.endWith(".ptml")) {
		targetPage = targetPage + ".ptml";
	}
	datas = 'targetPage=' + targetPage + '&' + datas;
	$.ajax({
		type : "POST",
		url : "/portal/ParamInteractive",
		data : datas,
		dataType : 'text',
		success : function(msg) {
			__renderPortlets.get(portletId)();
		}
	});
}

function closeMonitorWindow(){
	qBox.Close();
}

function doSendTopicMessage(topicName,messageText){
	amq.sendMessage("topic://"+topicName,messageText);
}

function doAjaxRefreshSelf(params,portletId){
	var datas = parsePortalParams(params);
	var targetPage = parsePageId();
	if (!targetPage.endWith(".ptml")){
		targetPage = targetPage+".ptml";
	}
	datas = 'targetPage='+targetPage+'&'+datas;
	$.ajax({
	   type: "POST",
	   url: "/portal/ParamInteractive",
	   data:datas,
	   dataType:'text',
	   success: function(msg){
			__renderPortlets.get(portletId)();
	   }
	}); 
}

function doRefreshPortalPage(params){
	var datas = parsePortalParams(params);
	$.ajax({
	   type: "POST",
	   url: "/portal/ParamInteractive",
	   data:datas,
	   dataType:'text',
	   success: function(msg){
		 window.location.reload();
	   }
	}); 
}

function parsePageId(){
	var result = null;
	var currentURL = window.location.href;
	var portalIndex = currentURL.indexOf('/request/');
	if (portalIndex < 0){
		portalIndex = currentURL.indexOf('/website/');
		var urlTemp = currentURL.substring(portalIndex+9,currentURL.length);
		var firstSplashIndex = urlTemp.indexOf("/",3);
		if (firstSplashIndex < 0){
			firstSplashIndex = urlTemp.length;
		}
		//alert("pageId is " + urlTemp.substring(0,firstSplashIndex));
		result = urlTemp.substring(0,firstSplashIndex);		
	}else{
		var urlTemp = currentURL.substring(portalIndex+9,currentURL.length);
		var firstSplashIndex = urlTemp.indexOf("/",3);
		if (firstSplashIndex < 0){
			firstSplashIndex = urlTemp.length;
		}
		//alert("pageId is " + urlTemp.substring(0,firstSplashIndex));
		result = urlTemp.substring(0,firstSplashIndex);		
	}
	//alert(result);
	return result;
}
function doAjaxRefreshPortalPage(params,options){
	var datas = parsePortalParams(params);
	var targetPage = parsePageId();
	if (!targetPage.endWith(".ptml")){
		targetPage = targetPage+".ptml";
	}
	datas = 'targetPage='+targetPage+'&'+datas;
	$.ajax({
	   type: "POST",
	   url: "/portal/ParamInteractive",
	   data:datas,
	   dataType:'text',
	   success: function(msg){
		   var keys = __renderPortlets.keys();
		   for (var i=0;i < keys.length;i++){
			   	var key = keys[i];
			   	if (options && options.curPortletId){
			   		if (key == options.curPortletId){
			   			continue;			   			
			   		}
			   	}
			   	__renderPortlets.get(key)();
		   }
	   }
	}); 
}

function doLinkPortalPage(targetURL,newPage){
	if (targetURL.startWith("/portal/")){
		var currentURL = parsePageId();
		var actionURL = "/portal/index?Portal&actionType=copySessionParams&targetURL="+targetURL+"&currentUserId="+$('#currentUserId').val()+"&currentURL="+currentURL;
		sendAction(actionURL,{onComplete:function(obj){
		   if (newPage == true){
				window.open(targetURL);
		   }else{
			   window.location.href = targetURL;
		   }			   
		}});		
	}else{
		if (newPage == true){
			window.open(targetURL);
		}else{
			window.location.href = targetURL;
		}		
	}
}

function doRedirectPortalPage(params){
	var datas = parsePortalParams(params);
	var targetURL = parsePortalTargetUrl(params);
	if (!targetURL.endWith(".ptml")){
		targetURL = targetURL+".ptml";
	}
	$.ajax({
	   type: "POST",
	   url: "/portal/ParamInteractive",
	   data:datas,
	   dataType:'text',
	   success: function(msg){
		   var currentURL = window.location.href;
		   var portalIndex = currentURL.indexOf('/request/');
		   if (portalIndex < 0){
			   window.location.href = "/portal/website/"+targetURL;
		   }else{
			   window.location.href = "/portal/request/"+targetURL;			   
		   }
	   }
	}); 	
}

function doPopupPortalPage(params){
	var datas = parsePortalParams(params);
	var targetURL = parsePortalTargetUrl(params);
	if (!targetURL.endWith(".ptml")){
		targetURL = targetURL+".ptml";
	}	
	$.ajax({
	   type: "POST",
	   url: "/portal/ParamInteractive",
	   data:datas,
	   dataType:'text',
	   success: function(){
		   var fullTargetURL = "";
		   var currentURL = window.location.href;
		   var portalIndex = currentURL.indexOf('/request/');
		   if (portalIndex < 0){
				fullTargetURL = "/portal/website/"+targetURL;
		   }else{
			   fullTargetURL = "/portal/request/"+targetURL;
		   }
		   window.open(fullTargetURL);
	   }
	}); 
}

function doPopupWebPage(params,options){
	var datas = parsePortalParams(params);
	var targetURL = parsePortalTargetUrl(params);
	if (!targetURL.endWith(".ptml")){
		targetURL = targetURL+".ptml";
	}
	$.ajax({
	   type: "POST",
	   url: "/portal/ParamInteractive",
	   data:datas,
	   dataType:'text',
	   success: function(msg){
		   var currentURL = window.location.href;
		   var portalIndex = currentURL.indexOf('/request/');
		   var boxURL;
		   if (portalIndex < 0){
			   boxURL = "/portal/website/"+targetURL;
		   }else{
			   boxURL = "/portal/request/"+targetURL;			   
		   }
		   var boxWidth = "1000";
		   var boxHeight = "600";
		   
		   if (options && options.width){
			   boxWidth = options.width;
		   }
		   if (options && options.height){
			   boxHeight = options.height;
		   }
		   var actionURL = "/portal/index?Portal&actionType=retrievePageName&targetPage="+targetURL;
		   sendAction(actionURL,{dataType:'json',onComplete:function(obj){
			   var boxTitle = obj.title;
			   qBox.iFLoad({title:boxTitle,src:boxURL,w:boxWidth,h:(parseInt(boxHeight)+30)});
			   if (options && options.x){
				   qBox.aDgs[0].moveToX(options.x);
			   }
			   if (options && options.y){
				   qBox.aDgs[0].moveToY(options.y);
			   }			   
		   }});
	   }
	}); 	
}

function logout(){
	window.location='/portal/index?Login&actionType=logout';
}

function doPopupPageBox(url,params){
	qBox.iFLoad({title:params.title,src:url,w:params.width,h:params.height});
}

function doPopupSSOBox(params){
	var datas = parsePortalParams(params);
	$.ajax({
	   type: "POST",
	   url: "/portal/index?SSORedirectBoxy&actionType=popupRequest",
	   data:datas,
	   dataType:'json',
	   success: function(obj){
			var boxTitle = obj.title;
			var boxWidth = obj.width;
			var boxHeight = obj.height;
			var boxSSOAppId = obj.ssoAppId;
			var boxConfigId = obj.configId;
			var ssoURL = "/portal_portlets/index?SSORedirectBoxy&ssoAppId="+boxSSOAppId+"&currentUserId=" + $('#currentUserId').val()+"&configId="+boxConfigId;
			Boxy.load(ssoURL,{title:boxTitle,center:true,unloadOnHide:true,modal:true,draggable: true,afterShow:function(){
				if (boxHeight && boxWidth){
					this.resize(boxWidth,boxHeight);
				}
			}});
	   }
	}); 
}

function doPopupGridBox(params,options){	
	var datas = parsePortalParams(params);
	var targetPage = parsePageId();
	if (!targetPage.endWith(".ptml")){
		targetPage = targetPage+".ptml";
	}	
	$.ajax({
		   type: "POST",
		   url: "/portal_portlets/index?DataGridBoxy&actionType=popupRequest&"+datas,
		   data:'',
		   dataType:'json',
		   success: function(obj){
				var boxTitle = obj.title;
				var boxWidth = obj.width;
				var boxHeight = obj.height;
				var boxGridCode = obj.gridCode;
				var gridBoxURL = "/portal_portlets/index?DataGridBoxy&gridCode="+boxGridCode+"&targetPage="+targetPage+"&__randomnumber="+Math.floor(Math.random()*100000);

				if (boxHeight && boxWidth){
					qBox.iFLoad({title:boxTitle,src:gridBoxURL,w:boxWidth,h:(parseInt(boxHeight)+30)});
				} else {
					qBox.iFLoad({title:boxTitle,src:gridBoxURL});
				}
				if (options.x){
					qBox.aDgs[0].moveToX(options.x);
				}
				if (options.y){
					qBox.aDgs[0].moveToY(options.y);
				}
		   }
	}); 
}

function parsePortalParams(params){
	var result = "";
	var keyValuePairs = params.split(',');
	for (var i=0;i < keyValuePairs.length;i++){
		var keyValuePair = keyValuePairs[i].split(":");
		var key = keyValuePair[0];
		var value = encodeURIComponent(keyValuePair[1]);
		if (keyValuePair.length > 2){
			for (var j=2;j < keyValuePair.length;j++){
				value = value + ":" + keyValuePair[j];
			}
		}
		result = result+"&"+key+"="+value;
	}
	if (result.length > 0 && result.substring(0,1) == '&'){
		result = result.substring(1,result.length);
	}
	if (document.getElementById("currentUserId")){
		result = result + '&currentUserId=' + $('#currentUserId').val();		
	}
	//alert("result is " + result);
	return result;
}
function parsePortalTargetUrl(params){
	var keyValuePairs = params.split(',');
	for (var i=0;i < keyValuePairs.length;i++){
		var keyValuePair = keyValuePairs[i].split(":");
		var key = keyValuePair[0];
		var value = keyValuePair[1];
		if ('targetPage' == key){
			return value;
		}
	}
	return null;
}


function triggleWindow(contextPath){
	var url = contextPath+"/index?Portal&actionType=triggleWindow";
	sendRequest(url,{onComplete:function(responseText){
		resetWindow();
	}});
}
function changeMode(contextPath){
	var url = contextPath+"/index?Portal&actionType=changeMode";
	sendRequest(url,{onComplete:function(responseText){
		resetWindow();
	}});
}

var _modifyPasswordBox;
function openModifyPasswordBox(){
	if (!_modifyPasswordBox){
		_modifyPasswordBox = new PopupBox('_modifyPasswordBox','修改个人密码',{top:'50px',size:'normal',width:'500px',height:'280px'});
	}
	var url = '/portal/index?ModifyPassword';
	_modifyPasswordBox.sendRequest(url);	
}

var _personalThemeBox;
function openPersonalThemeBox(navId){
	if (!_personalThemeBox){
		_personalThemeBox = new PopupBox('_personalThemeBox','个性主题选择',{top:'50px',size:'normal',width:'600px',height:'280px'});
	}
	var url = '/portal/index?PersonalThemeSelectList&navId='+navId;
	_personalThemeBox.sendRequest(url);	
}

var _personalFavoriteConfigManageBox;
function personalFavoriteConfigManageBox(navId,menuItemId){
	if (!_personalFavoriteConfigManageBox){
		_personalFavoriteConfigManageBox = new PopupBox('_personalFavoriteConfigManageBox','收藏夹设置',{top:'50px',size:'normal',width:'320px',height:'430px'});
	}
	var url = '/portal/index?PersonalFavoriteConfig&menuItemId='+menuItemId+'&navId='+navId;
	_personalFavoriteConfigManageBox.sendRequest(url);	
}


function addFavoritePage(menuBarId,menuItemId){
	var url = "/portal/index?Portal&actionType=isExistFavoritePage&menuBarId="+menuBarId+"&menuItemId="+menuItemId;
	var localURL = window.location.href;
	var currentMenuId = null;
	if (localURL.indexOf("currentMenuId=") > 0){
		var index = localURL.indexOf("currentMenuId=")+14;
		currentMenuId = localURL.substring(index,localURL.length);
		url = url+"&currentMenuId="+currentMenuId
	}
	sendRequest(url,{onComplete:function(responseText){
		if ("Y" == responseText){
			alert('该页面在收藏夹中存在，请检查确认！');
		}else{
			url = "/portal/index?Portal&actionType=addFavoritePage&menuBarId="+menuBarId+"&menuItemId="+menuItemId;
			if (currentMenuId){
				url = url+"&currentMenuId="+currentMenuId
			}
			sendRequest(url,{onComplete:function(responseText){
				$("#favoriteMenu #splitLine").nextAll().remove();
				$("#favoriteMenu #splitLine").after(responseText);
				
				$("#favoriteMenu #splitLine").nextAll().hover(function() {
					$(this).children('a:first').addClass("hov");
				}, function() {
					$(this).children('a:first').removeClass("hov");		
				});	
			}});				
		}
	}});		
}

function resetWindow(){
	window.location.reload();
}

function checkForRefresh() {
  if (top.location.href != self.location.href) {
		top.location.href=self.location.href;
  }
}
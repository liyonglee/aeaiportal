<%@page import="com.agileai.common.KeyGenerator"%>
<%@ page language="java" import="com.agileai.portal.driver.model.PortletWindowConfig" %>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%
String windowMode = (String)request.getAttribute("WINDOW_MODE_KEY");
String portletId = (String)request.getAttribute("portlet");
PortletWindowConfig windowConfig = (PortletWindowConfig)request.getAttribute(portletId);
String height = windowConfig.getHeight();
if (!"auto".equals(height)){
	height = height+"px";
}
String bodyStyle = "style=\"";
String defStyle = windowConfig.getBodyStyle();
bodyStyle = bodyStyle + defStyle;
bodyStyle = bodyStyle+"\"";

String shortPortletId = KeyGenerator.instance().shortKey(portletId);
String portletMode = (String)request.getAttribute(portletId+"PortletMode");
%>
<pt:portlet portletId="${portlet}">
<div id="portlet<%=shortPortletId%>" class="sharp">
  <div class="content" style="border-width:0;height:<%=height%>">
    <div <%=bodyStyle%> class="portletBox"><pt:render/></div>
  </div>
</div>
<%if ("edit".equals(windowMode)){%>
<pt:buildModeURL/>
<%}%>  
</pt:portlet>
<%if ("edit".equals(windowMode)){%>
<script>
var portletContextMenu<%=shortPortletId%> = new PortletContextMenu('contextMenu<%=shortPortletId%>','portlet<%=shortPortletId%>');
<%if ("edit".equals(portletMode)){%>
portletContextMenu<%=shortPortletId%>.bindItem("View","V",function(){self.location='<%=pageContext.getAttribute("ViewURL"+portletId)%>'});
<%}else if("view".equals(portletMode)){%>
portletContextMenu<%=shortPortletId%>.bindItem("Edit","E",function(){self.location='<%=pageContext.getAttribute("EditURL"+portletId)%>'});
<%}%>
portletContextMenu<%=shortPortletId%>.setup();
</script>
<%}%>
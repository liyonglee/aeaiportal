package com.agileai.portal.bizmoduler.uui;

import java.util.Date;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.BaseService;

public class UserLocalManageImpl extends BaseService implements UserLocalManage{
	@Override
	public DataRow retrieveUserRecord(String userCode) {
		String statementId = "SecurityGroup8Associates.getSecurityUserRecord";
		DataParam param = new DataParam("USER_CODE",userCode);
		return this.daoHelper.getRecord(statementId, param);
	}

	@Override
	public void updateUserTokends(String userCode, String tokeId, Date tokenTime) {
		String statementId = "SecurityGroup8Associates.updateSecurityUserTokens";
		DataParam param = new DataParam("USER_CODE",userCode,"TOKEN_ID",tokeId,"TOKEN_TIME",tokenTime);
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public void updateUserStatus(String userCode, String status) {
		String statementId = "SecurityGroup8Associates.updateSecurityUserStatus";
		DataParam param = new DataParam("USER_CODE",userCode,"USER_STATE",status);
		this.daoHelper.updateRecord(statementId, param);
	}
}

package com.agileai.portal.bizmoduler.wcm;

import com.agileai.hotweb.bizmoduler.core.TreeAndContentManageImpl;

public class WcmPictureGroup8ContentManageImpl
        extends TreeAndContentManageImpl
        implements WcmPictureGroup8ContentManage {
    public WcmPictureGroup8ContentManageImpl() {
        super();
        this.columnIdField = "GRP_ID";
        this.columnParentIdField = "GRP_PID";
        this.columnSortField = "GRP_ORDERNO";
    }
}

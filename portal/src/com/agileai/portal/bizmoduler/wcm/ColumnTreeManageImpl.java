package com.agileai.portal.bizmoduler.wcm;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeManageImpl;
import com.agileai.portal.bizmoduler.ColumnTreeManage;
import com.agileai.portal.filter.UserCacheManager;

public class ColumnTreeManageImpl
        extends TreeManageImpl
        implements ColumnTreeManage {
    public ColumnTreeManageImpl() {
        super();
        this.idField = "COL_ID";
        this.nameField = "COL_NAME";
        this.parentIdField = "COL_PID";
        this.sortField = "COL_ORDERNO";
    }

	public void insertChildRecord(DataParam param) {
		super.insertChildRecord(param);
		UserCacheManager.getOnly().truncateUsers();
	}	    
    
	@Override
	public boolean existRelInfomation(String columnId) {
		String statementId = this.sqlNameSpace+"."+"queryRelInfoCount";
		DataRow record = this.daoHelper.getRecord(statementId, columnId);
		Integer count = record.getInt("REL_COUNT");
		return count > 0;
	}

	@Override
	public List<DataRow> findRelColumnRecords(String columnId) {
		String statementId = this.sqlNameSpace+"."+"findRelColumnRecords";
		DataParam param = new DataParam("columnId",columnId);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}
	private List<String> getRefColumnIdList(List<DataRow> records){
		List<String> result = new ArrayList<String>();
		for (int i=0;i < records.size();i++){
			DataRow row = records.get(i);
			result.add(row.getString("COL_REL_ID"));
		}
		return result;
	}
	@Override
	public void addRelColumns(String columnId, List<String> refColumnList) {
		List<DataRow> records = this.findRelColumnRecords(columnId);
		int maxSort = 1;
		if (records != null && records.size() > 0){
			DataRow lastRow = records.get(records.size()-1);
			maxSort = lastRow.getInt("COL_SORT");
		}
		List<String> refColumnIdList = getRefColumnIdList(records);
		List<DataParam> paramList = new ArrayList<DataParam>();
		for (int i=0;i < refColumnList.size();i++){
			String refColumnId = refColumnList.get(i);
			if (refColumnIdList.contains(refColumnId)){
				continue;
			}
			DataParam param = new DataParam();
			param.put("COL_ID",columnId);
			param.put("COL_SORT",(maxSort+i+1));
			param.put("COL_REL_ID",refColumnId);
			paramList.add(param);
		}
		if (paramList.size() > 0){
			String statementId = this.sqlNameSpace+"."+"insertRelColumnRecord";;
			this.daoHelper.batchInsert(statementId, paramList);			
		}
	}

	@Override
	public void delRelColumn(String columnId, String refColumnId) {
		String statementId = this.sqlNameSpace+"."+"deleteRelColumnRecord";
		DataParam param = new DataParam();
		param.put("COL_ID",columnId,"COL_REL_ID",refColumnId);
		this.daoHelper.deleteRecords(statementId, param);
	}

	@Override
	public void moveUpColumn(String columnId, String refColumnId) {
		List<DataRow> records = this.findRelColumnRecords(columnId);
		DataRow beforeRow = null;
		DataRow currentRow = null;
		for(int i=0;i < records.size();i++){
			DataRow row = records.get(i);
			String tempRefColumnId = row.getString("COL_REL_ID");
			if (refColumnId.equals(tempRefColumnId)){
				currentRow = row;
				beforeRow = records.get(i-1);
				int currentSort = currentRow.getInt("COL_SORT");
				int beforeSort = beforeRow.getInt("COL_SORT");
				currentRow.put("COL_SORT",beforeSort);
				beforeRow.put("COL_SORT",currentSort);
				break;
			}
		}
		
		if (beforeRow != null && currentRow != null){
			List<DataParam> paramList = new ArrayList<DataParam>();
			DataParam beforeParam = beforeRow.toDataParam();
			DataParam currentParam = currentRow.toDataParam();
			paramList.add(beforeParam);
			paramList.add(currentParam);
			String statementId = this.sqlNameSpace+"."+"updateRelColumnRecord";
			this.daoHelper.batchUpdate(statementId, paramList);
		}
	}

	@Override
	public void moveDownColumn(String columnId, String refColumnId) {
		List<DataRow> records = this.findRelColumnRecords(columnId);
		DataRow currentRow = null;
		DataRow nextRow = null;
		for(int i=0;i < records.size();i++){
			DataRow row = records.get(i);
			String tempRefColumnId = row.getString("COL_REL_ID");
			if (refColumnId.equals(tempRefColumnId)){
				currentRow = row;
				nextRow = records.get(i+1);
				int currentSort = currentRow.getInt("COL_SORT");
				int nextSort = nextRow.getInt("COL_SORT");
				currentRow.put("COL_SORT",nextSort);
				nextRow.put("COL_SORT",currentSort);
				break;
			}
		}
		
		if (nextRow != null && currentRow != null){
			List<DataParam> paramList = new ArrayList<DataParam>();
			DataParam nextParam = nextRow.toDataParam();
			DataParam currentParam = currentRow.toDataParam();
			paramList.add(nextParam);
			paramList.add(currentParam);
			String statementId = this.sqlNameSpace+"."+"updateRelColumnRecord";
			this.daoHelper.batchUpdate(statementId, paramList);
		}
	}
}

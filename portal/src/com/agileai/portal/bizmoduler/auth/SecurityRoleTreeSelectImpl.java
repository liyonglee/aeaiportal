package com.agileai.portal.bizmoduler.auth;

import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;

public class SecurityRoleTreeSelectImpl
        extends TreeSelectServiceImpl
        implements SecurityRoleTreeSelect {
    public SecurityRoleTreeSelectImpl() {
        super();
    }
}

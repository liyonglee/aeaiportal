package com.agileai.portal.bizmoduler.sso;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public interface UserSSOService {
	public interface AppType{
		public static final String BS = "BS";
		public static final String CS = "CS";
	}
	public interface AuthType{
		public static final String CAS = "CAS";
		public static final String FORM = "FORM";
	}
	
	public static final String RedirectURLKey = "_redirect_url_";
	
	List<DataRow> findViewApplications(String userId);
	List<DataRow> findEditApplications(String userId);
	boolean isExistRelApplication(String appId);
	void addUserApplication(String appId,String userId);
	void editUserApplication(DataParam param,List<DataParam> paramEntrys);
	void changeApplicationSort(String userId,String ticketId, boolean isUp);
	void delUserApplication(String ticketId);
	
	DataRow findUserApplication(String ticketId);
	
	List<DataRow> findUserMappingItem(String ticketId);
	
	SSOTicket findSSOTicket(String ticketId);
	SSOTicket findDummySSOTicket(String appId);
}

package com.agileai.portal.bizmoduler.sso;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubServiceImpl;
import com.agileai.util.StringUtil;

public class SSOApplicationManageImpl extends MasterSubServiceImpl implements SSOApplicationManage {
	
	public SSOApplicationManageImpl(){
		super();
	}
	
	public String[] getTableIds() {
		List<String> temp = new ArrayList<String>();
		temp.add("param");
		return  temp.toArray(new String[]{});
	}
	
	public List<DataRow> findSubRecords(String subId, DataParam param) {
		List<DataRow> result = null;
		String statementId = sqlNameSpace+"."+"find"+StringUtil.upperFirst(subId)+"Records";
		result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
	
	public void saveSubRecords(DataParam param, List<DataParam> insertRecords, List<DataParam> updateRecords) {
		String subId = param.get("currentSubTableId");
		String curTableName = subTableIdNameMapping.get(subId);
		if (insertRecords != null && insertRecords.size() > 0){
			String sortField = subTableIdSortFieldMapping.get(subId);
			int sortValue = 0;
			if (!StringUtil.isNullOrEmpty(sortField)){
				sortValue = getNewMaxSort(subId,param);	
			}
			for (int i=0;i < insertRecords.size();i++){
				DataParam paramRow = insertRecords.get(i);
				processDataType(paramRow, curTableName);	
				processPrimaryKeys(curTableName,paramRow);
				
				if (!StringUtil.isNullOrEmpty(sortField)){
					paramRow.put(sortField,sortValue+i);
				}
			}
		}
		String statementId = sqlNameSpace+"."+"insert"+StringUtil.upperFirst(subId)+"Record";
		this.daoHelper.batchInsert(statementId, insertRecords);
		
		statementId = sqlNameSpace+"."+"update"+StringUtil.upperFirst(subId)+"Record";
		this.daoHelper.batchUpdate(statementId, updateRecords);
	}
}
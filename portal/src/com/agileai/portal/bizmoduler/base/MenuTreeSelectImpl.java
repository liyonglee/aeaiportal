package com.agileai.portal.bizmoduler.base;

import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;


public class MenuTreeSelectImpl extends TreeSelectServiceImpl implements MenuTreeSelect {
	public MenuTreeSelectImpl(){
		super();
		this.queryPickTreeRecordsSQL = "queryTreeRecords";
	}
}

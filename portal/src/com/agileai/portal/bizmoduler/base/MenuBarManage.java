package com.agileai.portal.bizmoduler.base;

import com.agileai.hotweb.bizmoduler.core.TreeManage;

public interface MenuBarManage extends TreeManage{
	boolean isCodeDuplicate(String navId,String menuId,String menuCode);
}

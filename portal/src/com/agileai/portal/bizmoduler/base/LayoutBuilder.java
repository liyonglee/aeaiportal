package com.agileai.portal.bizmoduler.base;

import java.util.List;

import com.agileai.portal.driver.model.LayoutContainer;
import com.agileai.portal.driver.model.Page;
import com.agileai.portal.driver.model.PortletWindowConfig;
import com.agileai.util.StringUtil;

public class LayoutBuilder {
	private boolean belongLoginedNavigater = false;
	
	public LayoutBuilder(boolean belongLoginedNavigater){
		this.belongLoginedNavigater = belongLoginedNavigater;
	}
	public void buildLayoutHtml(Page page,StringBuffer html) {
		html.append("<table class=\"topDesignLayout\" border='1'>");
		if (page.getMainContainerType().equals(LayoutContainer.ContainerType.COLUMN)){
			List<LayoutContainer> layoutContainerList = page.getLayoutContainers();
			html.append("<tr>");
			for (int i=0;i < layoutContainerList.size();i++){
				LayoutContainer layoutContainer = layoutContainerList.get(i);
				String width="";
				String gridWidth = layoutContainer.getWidth();
				if (!StringUtil.isNullOrEmpty(gridWidth)){
					width = "width=\""+gridWidth+"\"";
				}
				if (layoutContainer.hasChildContainer()){
					html.append("<td class=\"topColContainer\" ").append(width).append(">");
					this.buildLayout(layoutContainer, html);
					html.append("</td>");
				}else{
					String containerId = layoutContainer.getId();
					html.append("<td class=\"leafColContainer\" ").append(width).append(">");
					html.append("<div class=\"droppable\" id=\"").append(containerId).append("\">");
					List<PortletWindowConfig> pagePortletList = layoutContainer.getPortlets();
					if (pagePortletList == null || pagePortletList.isEmpty()){
						buildContainerHtml(layoutContainerList.size()==1,layoutContainer,false,true,html);
					}else{
						buildContainerHtml(layoutContainerList.size()==1,layoutContainer,true,true,html);
						this.printPortlet(layoutContainer,pagePortletList,html);						
					}
					html.append("</div>");
					html.append("</td>");
				}
			}
			html.append("</tr>");
		}else{
			List<LayoutContainer> layoutContainerList = page.getLayoutContainers();
			for (int i=0;i < layoutContainerList.size();i++){
				LayoutContainer layoutContainer = layoutContainerList.get(i);
				html.append("<tr>");
				if (layoutContainer.hasChildContainer()){
					html.append("<td class=\"topRowContainer\">");
					this.buildLayout(layoutContainer, html);
					html.append("</td>");
				}else{
					String containerId = layoutContainer.getId();
					html.append("<td class=\"leafRowContainer\">");
					html.append("<div class=\"droppable\" id=\"").append(containerId).append("\">");
					List<PortletWindowConfig> pagePortletList = layoutContainer.getPortlets();
					if (pagePortletList == null || pagePortletList.isEmpty()){
						buildContainerHtml(layoutContainerList.size()==1,layoutContainer,false,false,html);
					}else{
						buildContainerHtml(layoutContainerList.size()==1,layoutContainer,true,false,html);
						this.printPortlet(layoutContainer,pagePortletList,html);						
					}	
					html.append("</div>");
					html.append("</td>");
				}
				html.append("</tr>");
			}
		}
		html.append("</table>");			
	}
	
	private void buildLayout(LayoutContainer layoutContainer,StringBuffer html) {
		if (layoutContainer.getContainerType().equals(LayoutContainer.ContainerType.COLUMN)){
			html.append("<table class=\"designLayout\"  border='1'>");
			List<LayoutContainer> childLayoutContainerList = layoutContainer.getChildContainerList();
			for (int i=0;i < childLayoutContainerList.size();i++){
				LayoutContainer childContainer = childLayoutContainerList.get(i);
				html.append("<tr>");
				String width="";
				String gridWidth = childContainer.getWidth();
				if (!StringUtil.isNullOrEmpty(gridWidth)){
					width = "width=\""+gridWidth+"\"";
				}
				if (childContainer.hasChildContainer()){
					html.append("<td class=\"rowContainer\" ").append(width).append(">");
					this.buildLayout(childContainer, html);
					html.append("</td>");
				}else{
					String containerId = childContainer.getId();
					html.append("<td class=\"leafRowContainer\" ").append(width).append(">");
					html.append("<div class=\"droppable\" id=\"").append(containerId).append("\">");
					List<PortletWindowConfig> pagePortletList = childContainer.getPortlets();
					if (pagePortletList == null || pagePortletList.isEmpty()){
						buildContainerHtml(childLayoutContainerList.size()==1,childContainer,false,false,html);
					}else{
						buildContainerHtml(childLayoutContainerList.size()==1,childContainer,true,false,html);
						this.printPortlet(childContainer,pagePortletList,html);						
					}	
					html.append("</div>");
					html.append("</td>");
				}
				html.append("</tr>");
			}
			html.append("</table>");
		}else{
			html.append("<table class=\"designLayout\"  border='1'>");
			html.append("<tr>");
			List<LayoutContainer> childLayoutContainerList = layoutContainer.getChildContainerList();
			for (int i=0;i < childLayoutContainerList.size();i++){
				LayoutContainer childContainer = childLayoutContainerList.get(i);
				String width="";
				String gridWidth = childContainer.getWidth();
				if (!StringUtil.isNullOrEmpty(gridWidth)){
					width = "width=\""+gridWidth+"\"";
				}
				if (childContainer.hasChildContainer()){
					html.append("<td class=\"colContainer\" ").append(width).append(">");
					this.buildLayout(childContainer, html);
					html.append("</td>");
				}
				else{
					String containerId = childContainer.getId();
					html.append("<td class=\"leafColContainer\" ").append(width).append(">");
					html.append("<div class=\"droppable\" id=\"").append(containerId).append("\">");
					List<PortletWindowConfig> pagePortletList = childContainer.getPortlets();
					if (pagePortletList == null || pagePortletList.isEmpty()){
						buildContainerHtml(childLayoutContainerList.size()==1,childContainer,false,true,html);
					}else{
						buildContainerHtml(childLayoutContainerList.size()==1,childContainer,true,true,html);
						this.printPortlet(childContainer,pagePortletList,html);						
					}	
					html.append("</div>");
					html.append("</td>");
				}
			}
			html.append("</tr>");
			html.append("</table>");
		}
	}
	private void buildContainerHtml(boolean isOnly,LayoutContainer layoutContainer,boolean hasPortlet,boolean isColContainer,StringBuffer html){
		String containerId = layoutContainer.getId();
		String curContainerTypeName = isColContainer?"列容器":"行容器";
		html.append("<span style='float:left;height:21px;'>&nbsp;"+curContainerTypeName+"</span>");
		html.append("<span style='float:right;height:21px;'>");
		if (!hasPortlet){
			html.append("<img src='images/layout/addChildContainer.gif' style='margin:0px 2px;' width='16' height='16' onclick=\"addChildContainerRequest('").append(containerId).append("')\" title='添加子容器' />");
		}
		if (isColContainer){
			html.append("<img src='images/layout/addColumn.gif' style='margin:0px 2px;' width='16' height='16' onclick=\"splitContainerRequest('").append(containerId).append("')\" title='扩展列容器' />");			
		}else{
			html.append("<img src='images/layout/addRow.gif' style='margin:0px 2px;' width='16' height='16' onclick=\"splitContainerRequest('").append(containerId).append("')\" title='扩展行容器' />");
		}
		html.append("<img src='images/layout/setupWidth.gif' style='margin:0px 2px;' width='16' height='16' onclick=\"setupWidthRequest('").append(containerId).append("')\" title='设置宽度' />");			
		html.append("<img src='images/layout/addPortlet.gif' style='margin:0px 2px;' width='16' height='16' onclick=\"addPortletRequest('").append(containerId).append("')\" title='添加Portlet' />");

		html.append("<img src='images/layout/delete.gif' style='margin:0px 2px;' width='16' height='16' onclick=\"deleteContainer('").append(containerId).append("')\" title='删除容器' />");
		html.append("</span>");
	}
	private void printPortlet(LayoutContainer layoutContainer,List<PortletWindowConfig> pagePortletList,StringBuffer html){
		for (int i=0;i < pagePortletList.size();i++){
			PortletWindowConfig pagePortlet = pagePortletList.get(i);
			this.buildPortletHtml(pagePortlet,layoutContainer, i==0, i==pagePortletList.size()-1, html);
		}
	}
	private void buildPortletHtml(PortletWindowConfig pagePortlet,LayoutContainer layoutContainer,boolean isTop,boolean isBottom,StringBuffer html){
		String portletId = pagePortlet.getId();
		String containerId = layoutContainer.getId();
		html.append("<div class='portletArea'>");
		html.append("<span class='draggable' containerId='").append(containerId).append("' id='").append(portletId).append("'>").append(pagePortlet.getTitle()).append("</span>");
		html.append("<span style='float:right;height:25px;padding-top:3px;'>");
		
		html.append("<img src='images/layout/language.png' style='margin:0px 2px;' width='16' height='16' onclick=\"configLanguage('").append(portletId).append("','").append(pagePortlet.getTitle()).append("')\" title='多语言设置' />");
		html.append("<img src='images/layout/config.gif' style='margin:0px 2px;' width='16' height='16' onclick=\"configPortletRequest('").append(portletId).append("','").append(containerId).append("')\" title='配置Portlet' />");
		if (pagePortlet.isStored() && belongLoginedNavigater){
			html.append("<img src='images/layout/assign.gif' style='margin:0px 2px;' width='16' height='16' onclick=\"configSecurityRequest('").append(portletId).append("')\" title='安全设置' />");
		}
		if (!isTop){
			html.append("<img src='images/layout/movePortletUp.gif' style='margin:0px 2px;' width='16' height='16' onclick=\"moveUp('").append(portletId).append("','").append(containerId).append("')\" title='上移' />");
		}
		if (!isBottom){
			html.append("<img src='images/layout/movePortletDown.gif' style='margin:0px 2px;' width='16' height='16' onclick=\"moveDown('").append(portletId).append("','").append(containerId).append("')\" title='下移' />");
		}
		html.append("<img src='images/layout/delete.gif' style='margin:0px 2px;' width='16' height='16' onclick=\"delPortlet('").append(portletId).append("','").append(containerId).append("')\" title='删除Portlet' />");
		html.append("</span>");
		
		html.append("</div>");
	}
}

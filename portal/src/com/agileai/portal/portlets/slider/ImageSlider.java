package com.agileai.portal.portlets.slider;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.Resource;
import com.agileai.portal.driver.common.PortletRequestHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.portlets.PortletCacheManager;
import com.agileai.util.StringUtil;

public class ImageSlider extends GenericPotboyPortlet {
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String sliderWidth = preferences.getValue("sliderWidth", null);
		String sliderHeight = preferences.getValue("sliderHeight", null);
		String isLazyLoad = preferences.getValue("isLazyLoad", null);
		String isContent = preferences.getValue("isContent", null);
		String contentId = preferences.getValue("contentId", null);
		String dataURL = preferences.getValue("dataURL", null);
		
		if (!StringUtil.isNullOrEmpty(sliderWidth) && !StringUtil.isNullOrEmpty(sliderHeight) 
				&& !StringUtil.isNullOrEmpty(isLazyLoad) && !StringUtil.isNullOrEmpty(isContent) 
				&& (!StringUtil.isNullOrEmpty(contentId) || !StringUtil.isNullOrEmpty(dataURL))){
			request.setAttribute("isSetting", "Y");
		}else{
			request.setAttribute("isSetting", "N");
		}
		
		String isSetting = (String)request.getAttribute("isSetting");
		String html = "";
		try {
			if ("Y".equals(isSetting)){
				if ("N".equals(isLazyLoad)){
					if ("Y".equals(isContent)){
						String urlPrefix = PortletCacheManager.buildDataURLPefix(request);
						dataURL = urlPrefix+"/resource?ContentProvider&contentId="+contentId;
						html = this.retrieveHtml(preferences, dataURL);
					}else{
						dataURL = getDataURL(request);
						if (dataURL.startsWith("/portal")){
							HttpServletRequest servletRequest = PortletRequestHelper.getRequestContext(request).getServletRequest();
							String serverName = servletRequest.getServletContext().getInitParameter("serverName");
							dataURL = serverName + dataURL;
						}
						html = this.retrieveHtml(preferences, dataURL);
					}
				}
			}
		} catch (Exception e) {
			request.setAttribute("sliderHtml", "");
		}
		request.setAttribute("sliderHtml", html);
		request.setAttribute("sliderWidth", sliderWidth);
		request.setAttribute("sliderHeight", sliderHeight);
		request.setAttribute("isLazyLoad", isLazyLoad);
		request.setAttribute("isContent", isContent);			
		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String sliderWidth = preferences.getValue("sliderWidth", null);
		String sliderHeight = preferences.getValue("sliderHeight", null);
		String isLazyLoad = preferences.getValue("isLazyLoad", null);
		String isContent = preferences.getValue("isContent", "Y");
		String contentId = preferences.getValue("contentId", null);
		String dataURL = preferences.getValue("dataURL", null);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		String cacheMinutes = preferences.getValue("cacheMinutes", defaultCacheMinutes);
		
		request.setAttribute("sliderWidth", sliderWidth);
		request.setAttribute("sliderHeight", sliderHeight);
		request.setAttribute("isLazyLoad", isLazyLoad);
		request.setAttribute("isContent", isContent);
		request.setAttribute("contentId", contentId);
		request.setAttribute("dataURL", dataURL);
		request.setAttribute("defaultVariableValues", defaultVariableValues);
		request.setAttribute("isCache", isCache);
		request.setAttribute("cacheMinutes", cacheMinutes);
		
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String sliderWidth = request.getParameter("sliderWidth");
		String sliderHeight = request.getParameter("sliderHeight");
		String isLazyLoad = request.getParameter("isLazyLoad");
		String isContent = request.getParameter("isContent");
		String contentId = request.getParameter("contentId");
		String dataURL = request.getParameter("dataURL");
		String defaultVariableValues = request.getParameter("defaultVariableValues");
		String isCache = request.getParameter("isCache");
		String cacheMinutes = request.getParameter("cacheMinutes");
		
		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("sliderWidth", sliderWidth);
		preferWapper.setValue("sliderHeight", sliderHeight);
		preferWapper.setValue("isLazyLoad", isLazyLoad);
		preferWapper.setValue("isContent", isContent);
		preferWapper.setValue("contentId", contentId);
		preferWapper.setValue("dataURL", dataURL);
		preferWapper.setValue("defaultVariableValues", defaultVariableValues);
		preferWapper.setValue("isCache", isCache);
		preferWapper.setValue("cacheMinutes", cacheMinutes);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
	
	private String retrieveHtml(PortletPreferences preferences,String dataURL) throws Exception{
		String ajaxData = "";
		String cacheMinutes = (String)preferences.getValue("cacheMinutes",defaultCacheMinutes);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		ajaxData = PortletCacheManager.getOnly().getCachedData(isCache, dataURL, cacheMinutes);
		return ajaxData;
	}
	
	@Resource(id="getAjaxData")
	public void getAjaxData(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		String ajaxData = "";
		try {
			PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
			String isContent = preferences.getValue("isContent", null);
			String contentId = preferences.getValue("contentId", null);
			String dataURL = null;
			if ("Y".equals(isContent)){
				String urlPrefix = PortletCacheManager.buildDataURLPefix(request);
				dataURL = urlPrefix+"/resource?ContentProvider&contentId="+contentId;
			}else{
				dataURL = getDataURL(request);
			}
			ajaxData = retrieveHtml(preferences, dataURL);
		} catch (Exception e) {
			this.logger.error("获取取数据失败getAjaxData", e);
		}
		PrintWriter writer = response.getWriter();
		writer.print(ajaxData);
		writer.close();
	}
}

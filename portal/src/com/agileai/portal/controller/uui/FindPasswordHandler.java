package com.agileai.portal.controller.uui;

import java.util.Date;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.auth.SecurityGroupManage;
import com.agileai.portal.bizmoduler.uui.UserLocalManage;
import com.agileai.portal.extend.uui.UserSyncHelper;
import com.agileai.portal.wsclient.uui.ResultStatus;
import com.agileai.portal.wsclient.uui.UserSync;

public class FindPasswordHandler
        extends BaseHandler {
	
    public FindPasswordHandler() {
        super();
        this.serviceId = buildServiceId(SecurityGroupManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    	
    }

    protected SecurityGroupManage getService() {
        return (SecurityGroupManage) this.lookupService(this.getServiceId());
    }
    
    @PageAction
	public ViewRenderer findPassword(DataParam param){
		String userCode = param.get("USER_CODE");
		String mailAddress = param.get("USER_MAIL");
		String url = null;
		if (sendResetPasswordMail(userCode,mailAddress)){
			url = getHandlerURL(RegistryResultHandler.class)+"&operaType=findPassword&result=success&mailAddress="+mailAddress;
		}else{
			url = getHandlerURL(RegistryResultHandler.class)+"&operaType=findPassword&result=failure&mailAddress="+mailAddress;
		}
		return new RedirectRenderer(url);
	}
    
    private boolean sendResetPasswordMail(String userCode,String mailAddress){
    	boolean result = false;
    	try {
    		UserSync userSync = UserSyncHelper.getUserSyncService();
    		String tokenId = UserSyncHelper.genTokenId("sendMail");
    		String storeTokenId = String.valueOf(System.currentTimeMillis());
			String mailContent = UserSyncHelper.createFindPwdMailContent(userCode, storeTokenId);
			mailContent = mailContent.replaceAll("[\\x00-\\x08\\x0b-\\x0c\\x0e-\\x1f]", "");  
			String mailSubject = "数通畅联--找回密码链接";
			ResultStatus resultStatus = userSync.sendMail(tokenId, mailAddress,mailSubject,mailContent);
			if (resultStatus.isSuccess()){
				Date storeTokenTime = new Date();
				UserLocalManage userLocalManage = this.lookupService(UserLocalManage.class);
				userLocalManage.updateUserTokends(userCode, storeTokenId, storeTokenTime);	
				result = true;
			}
		} catch (Exception e) {
			result = false;
			log.error(e.getLocalizedMessage(), e);
		}
    	return result;    	
    }
    
	
	@PageAction
	public ViewRenderer checkUserInfo(DataParam param){
		String rspText = FAIL;
		String valideCode = param.get("valideCode");
		String safecode = (String)this.request.getSession().getAttribute("safecode");
		if (safecode != null && safecode.equals(valideCode)){
			String mailAddress = param.get("USER_MAIL");
			UserLocalManage securityAuthorizationConfig = (UserLocalManage)this.lookupService(UserLocalManage.class);
			String userCode = param.get("USER_CODE");
			DataRow row = securityAuthorizationConfig.retrieveUserRecord(userCode);
			if (row != null && row.size() > 0){
				if("1".equals(row.stringValue("USER_STATE"))){
					if (mailAddress != null && mailAddress.equals(row.get("USER_MAIL"))){
						rspText = "";
					}else{
						rspText = "邮件地址跟注册所用邮件地址不一致，请确认！";	
					}					
				}else{
					rspText = userCode+"账户尚未激活，不能找回密码！！";	
				}
			}else{
				rspText = userCode+"用户不存在！";
			}
		}else{
			rspText = "验证码不正确！";
		}
		return new AjaxRenderer(rspText);
	}

}
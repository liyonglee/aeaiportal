package com.agileai.portal.controller.base;

import javax.servlet.http.HttpSession;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.driver.model.Theme;
import com.agileai.util.StringUtil;

public class ThemeLayoutConfigHandler extends BaseHandler{
	
	public ThemeLayoutConfigHandler(){
		super();
	}
	@PageAction
	public ViewRenderer setWindowSize(DataParam param){
		String rspText = FAIL;
		HttpSession session = this.request.getSession();
		if (StringUtil.isNotNullNotEmpty(param.get(Theme.WindowScreenWidth))){
			session.setAttribute(Theme.WindowScreenWidth,param.get(Theme.WindowScreenWidth));
		}
		if (StringUtil.isNotNullNotEmpty(param.get(Theme.WindowScreenHeight))){
			session.setAttribute(Theme.WindowScreenHeight,param.get(Theme.WindowScreenHeight));
		}
		return new AjaxRenderer(rspText);
	}
}
package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PtHandlerManage;

public class PtHandlerManageEditHandler
        extends StandardEditHandler {
    public PtHandlerManageEditHandler() {
        super();
        this.listHandlerClass = PtHandlerManageListHandler.class;
        this.serviceId = buildServiceId(PtHandlerManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("ACCESS_TYPE",
                     FormSelectFactory.create("ACCESS_TYPE")
                                      .addSelectedValue(getOperaAttributeValue("ACCESS_TYPE",
                                                                               "system")));
    }

    protected PtHandlerManage getService() {
        return (PtHandlerManage) this.lookupService(this.getServiceId());
    }
}

package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.driver.model.LayoutContainer;

public class ChildContainerAddHandler extends BaseHandler{
	public ChildContainerAddHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		LayoutManageHandler layoutManageHandler = new LayoutManageHandler();
		layoutManageHandler.setRequest(this.request);
		String containerId = param.get(LayoutManageHandler.CONTAINER_ID);
		LayoutContainer curLayoutContainer = layoutManageHandler.retrieveLayoutContainer(containerId);
		String containerType = curLayoutContainer.getContainerType();
		String childContainerType = null;
		String childContainerTypeName = null;
		if (LayoutContainer.ContainerType.ROW.equals(containerType)){
			childContainerType = LayoutContainer.ContainerType.COLUMN;
			childContainerTypeName = "列容器";
		}else{
			childContainerType = LayoutContainer.ContainerType.ROW;
			childContainerTypeName = "行容器";
		}
		this.setAttribute("childContainerType", childContainerType);
		this.setAttribute("childContainerTypeName", childContainerTypeName);
		return new LocalRenderer(getPage());
	}	
}

package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PtStaticDataManage;

public class PtStaticDataManageListHandler
        extends StandardListHandler {
    public PtStaticDataManageListHandler() {
        super();
        this.editHandlerClazz = PtStaticDataManageEditHandler.class;
        this.serviceId = buildServiceId(PtStaticDataManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("dataType",
                     FormSelectFactory.create("STATIC_DATA_TYPE")
                                      .addSelectedValue(param.get("dataType")));
        setAttribute("dataGrp",
                     FormSelectFactory.create("STATIC_DATA_GRP")
                                      .addSelectedValue(param.get("dataGrp")));
        initMappingItem("DATA_TYPE",
                        FormSelectFactory.create("STATIC_DATA_TYPE").getContent());
        initMappingItem("DATA_GRP",
                        FormSelectFactory.create("STATIC_DATA_GRP").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "dataCode", "");
        initParamItem(param, "dataType", "");
        initParamItem(param, "dataGrp", "");
    }

    protected PtStaticDataManage getService() {
        return (PtStaticDataManage) this.lookupService(this.getServiceId());
    }
}

package com.agileai.portal.controller.base;

import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.MenuItemPersonalManage;
import com.agileai.portal.driver.AttributeKeys;
import com.agileai.portal.driver.common.UserPersonalHelper;
import com.agileai.portal.driver.model.Decorator;
import com.agileai.portal.driver.model.MenuItem;
import com.agileai.portal.driver.model.PersonalFavorite;
import com.agileai.portal.driver.model.PersonalSetting;
import com.agileai.portal.driver.model.PortletWindowConfig;
import com.agileai.portal.driver.model.Theme;
import com.agileai.portal.driver.service.DriverConfiguration;
import com.agileai.portal.driver.service.PortalConfigService;
import com.agileai.portal.driver.tags.LayoutTag;
import com.agileai.portal.driver.util.DecoratorUtil;
import com.agileai.util.StringUtil;

public class PersonalPagePortletConfigHandler extends BaseHandler{
	public static final String PORTLET_RES_NAME = "portletResName";
	public static final String PORTLET_TITLE = "title";
	public static final String PORTLET_HEIGHT = "height";
	
	public PersonalPagePortletConfigHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param) {
		String navId = param.get("navId");
		this.setAttributes(param);
		String menuItemId = param.get("menuItemId");
		String portletId = param.get(LayoutManageHandler.PORTLET_ID);
		
		PortalConfigService portalConfigService = (PortalConfigService)BeanFactory.instance().getBean("PortalConfigService");
		PortletWindowConfig srcPortletConfig = portalConfigService.getPortletWindowConfig(portletId);
		
		PortletWindowConfig portletConfig = srcPortletConfig.clone();
		
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		HashMap<String, String> portletConfigAttributes = personalSetting.getPortletConfig(portletId);
		String decortorId = srcPortletConfig.getDecoratorId();
		Theme theme = null;
		if (Decorator.ExtendDecoratorId.equals(decortorId)){
			String themeId = retrieveThemeId(navId,menuItemId); 
			theme = getPortalConfigService().getTheme(themeId);
			decortorId = theme.getDecoratorId();
		}
		LayoutTag.mergePortletWindowConfig(this.request.getServletContext(),decortorId,portletConfig, portletConfigAttributes);

		this.setAttribute(PORTLET_RES_NAME, portletConfig.getPortletName());
		this.setAttribute("menuItemId", param.get("menuItemId"));
		
		DecoratorUtil decoratorUtil = new DecoratorUtil(this.request.getServletContext());
		List<String> personalPropertyList = decoratorUtil.getDecoratorPersonalPropertyList(decortorId);
		
		for (int i=0;i < personalPropertyList.size();i++){
			String key = personalPropertyList.get(i);
			String value = portletConfig.getDecorParams().get(key);
			if (StringUtil.isNullOrEmpty(value) && Decorator.ExtendDecoratorId.equals(portletConfig.getDecoratorId())){
				value = theme.getDecorParams().get(key);
			}
			this.setAttribute(key, value);
		}
		
		this.setAttribute(PORTLET_TITLE, portletConfig.getTitle());
		this.setAttribute(PORTLET_HEIGHT, portletConfig.getHeight());
		
		String jspPage = decoratorUtil.getDecoratorPersonalEditPage(decortorId);
		return new LocalRenderer(jspPage);
	}
	
	private String retrieveThemeId(String navId,String menuItemId){
		MenuItem menuItem = getPortalConfigService().getMenuItem(menuItemId);
		String result = menuItem.getThemeId();
		User user = (User)this.getUser();
		if (user != null){
			Theme defaultTheme = getPortalConfigService().getTheme(result);
			if (defaultTheme.isPersonalable()){
				PersonalFavorite personalFavorite = (PersonalFavorite)user.getExtendProperties().get(AttributeKeys.PERSONAL_FAVORITE_OBJECT_KEY);
				if (personalFavorite != null && !StringUtil.isNullOrEmpty(personalFavorite.getThemeId(navId))){
					result = personalFavorite.getThemeId(navId);
				}				
			}
		}
		return result;
	}	

	private PortalConfigService getPortalConfigService(){
		DriverConfiguration driverConfiguration = (DriverConfiguration)this.request.getServletContext().getAttribute(AttributeKeys.DRIVER_CONFIG);
		return driverConfiguration.getPortalConfigService();
	}
	
	public ViewRenderer doSaveAction(DataParam param){
		String navId = param.get("navId");
		String menuItemId = param.get("menuItemId");
		String portletId = param.get(LayoutManageHandler.PORTLET_ID);
		
		User user = (User)this.getUser();
		String userCode = user.getUserCode();

		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		HashMap<String,String> portletAttrs = personalSetting.getPortletConfig(portletId);
		
		PortalConfigService portalConfigService = (PortalConfigService)BeanFactory.instance().getBean("PortalConfigService");
		PortletWindowConfig portletConfig = portalConfigService.getPortletWindowConfig(portletId);
		
		DecoratorUtil decoratorUtil = new DecoratorUtil(this.request.getServletContext());
		String decortorId = portletConfig.getDecoratorId();
		if (Decorator.ExtendDecoratorId.equals(decortorId)){
			String themeId = retrieveThemeId(navId,menuItemId); 
			Theme theme = getPortalConfigService().getTheme(themeId);
			decortorId = theme.getDecoratorId();
		}
		List<String> personalPropertyList = decoratorUtil.getDecoratorPersonalPropertyList(decortorId);
		
		for (int i=0;i < personalPropertyList.size();i++){
			String key = personalPropertyList.get(i);
			String value = param.get(key);
			portletAttrs.put(key, value);
		}
		
		MenuItemPersonalManage menuItemPersonalManage = this.lookupService(MenuItemPersonalManage.class);
		menuItemPersonalManage.savePersonalSetting(userCode, menuItemId, personalSetting);

		String rspText = SUCCESS;
		return new AjaxRenderer(rspText);
	}
}
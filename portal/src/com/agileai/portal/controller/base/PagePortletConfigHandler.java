package com.agileai.portal.controller.base;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.base.PtDecoratorManage;
import com.agileai.portal.driver.model.Decorator;
import com.agileai.portal.driver.model.LayoutContainer;
import com.agileai.portal.driver.model.PortletWindowConfig;
import com.agileai.portal.driver.service.PortalConfigService;
import com.agileai.portal.driver.util.DecoratorUtil;
import com.agileai.util.StringUtil;

public class PagePortletConfigHandler extends BaseHandler{
	public static final String PORTLET_TITLE = "title";
	public static final String PORTLET_HEIGHT = "height";
	
	public static final String PORTLET_RES_NAME = "portletResName";
	public static final String PORTLET_DECORATOR = "portletDecorator";
	public static final String PORTLET_BODY_STYLE = "portletBodyStyle";
	public static final String PTLET_VISIABLE = "portletVisiable";
	
	public PagePortletConfigHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		this.setAttributes(param);
		String portletId = param.get(LayoutManageHandler.PORTLET_ID);

		PortletWindowConfig pagePortlet = null;
		String containerId = param.get(LayoutManageHandler.CONTAINER_ID);
		
		if (StringUtil.isNullOrEmpty(containerId)){
			PortalConfigService portalConfigService = (PortalConfigService)BeanFactory.instance().getBean("PortalConfigService");
			pagePortlet = portalConfigService.getPortletWindowConfig(portletId);
		}else{
			LayoutManageHandler layoutManageHandler = new LayoutManageHandler();
			layoutManageHandler.setRequest(this.request);
			LayoutContainer curLayoutontainer = layoutManageHandler.retrieveLayoutContainer(containerId);
			pagePortlet = curLayoutontainer.getPortlet(portletId);
		}
		
		String portletName = pagePortlet.getTitle();
		String portletDecorator = pagePortlet.getDecoratorId();
		String portletHeight = pagePortlet.getHeight();
		String portletBodyStyle = pagePortlet.getBodyStyle();
		String portletVisiable = pagePortlet.isVisiable()?"Y":"N";
		if (StringUtil.isNotNullNotEmpty(param.get(PORTLET_DECORATOR))){
			portletDecorator = param.get(PORTLET_DECORATOR);
		}
		
		PtDecoratorManage decortorManage = this.lookupService(PtDecoratorManage.class);
		DataParam queryParam = new DataParam();
		List<DataRow> records = decortorManage.findRecords(queryParam);
		DataRow row = new DataRow();
		row.put("DECOR_ID", Decorator.ExtendDecoratorId);
		row.put("DECOR_NAME", "继承");
		records.add(0, row);
		FormSelect decoratorSelect = new FormSelect();
		decoratorSelect.setKeyColumnName("DECOR_ID");
		decoratorSelect.setValueColumnName("DECOR_NAME");
		decoratorSelect.putValues(records);
		decoratorSelect.setSelectedValue(portletDecorator);
		
		FormSelect portletVisiableSelect = FormSelectFactory.create("BOOL_DEFINE");
		
		this.setAttribute(PORTLET_DECORATOR,decoratorSelect);
		this.setAttribute(PORTLET_TITLE, portletName);
		this.setAttribute(PORTLET_RES_NAME, pagePortlet.getPortletName());
		this.setAttribute(PORTLET_HEIGHT, portletHeight);
		this.setAttribute(PORTLET_BODY_STYLE, portletBodyStyle);
		this.setAttribute(PTLET_VISIABLE, portletVisiableSelect.addSelectedValue(portletVisiable));
		
		String decoratorId = pagePortlet.getDecoratorId();
		if (StringUtil.isNotNullNotEmpty(param.get(PORTLET_DECORATOR))){
			decoratorId = param.get(PORTLET_DECORATOR);
		}
		DecoratorUtil decoratorUtil = new DecoratorUtil(this.request.getServletContext());
		List<String> decoratorPropertyList = decoratorUtil.getDecoratorPropertyList(decoratorId);
		for (int i=0;i < decoratorPropertyList.size();i++){
			String key = decoratorPropertyList.get(i);
			String value = pagePortlet.getDecorParams().get(key);
			this.setAttribute(key, value);
		}
		
		String jspPage = decoratorUtil.getDecoratorEditPage(decoratorId);
		return new LocalRenderer(jspPage);
	}
	
	public ViewRenderer doSaveAction(DataParam param){
		String portletId = param.get(LayoutManageHandler.PORTLET_ID);
		String containerId = param.get(LayoutManageHandler.CONTAINER_ID);
		PortletWindowConfig pagePortlet = null;
		PortalConfigService portalConfigService = (PortalConfigService)this.lookupService("PortalConfigService");
		if (StringUtil.isNullOrEmpty(containerId)){
			pagePortlet = portalConfigService.getPortletWindowConfig(portletId);
		}else{
			LayoutManageHandler layoutManageHandler = new LayoutManageHandler();
			layoutManageHandler.setRequest(this.request);
			LayoutContainer curLayoutontainer = layoutManageHandler.retrieveLayoutContainer(containerId);
			pagePortlet = curLayoutontainer.getPortlet(portletId);
		}
		
		String portletName = param.get(PORTLET_TITLE);
		String portletDecorator = param.get(PORTLET_DECORATOR);
		String portletHeight = param.get(PORTLET_HEIGHT);
		String portletBodyStyle = param.get(PORTLET_BODY_STYLE);
		String portletVisiable = param.get(PTLET_VISIABLE);
		
		pagePortlet.setTitle(portletName);
		pagePortlet.setHeight(portletHeight);
		pagePortlet.setBodyStyle(portletBodyStyle);
		pagePortlet.setVisiable(!"N".equals(portletVisiable));
		if (!portletDecorator.equals(pagePortlet.getDecoratorId())){
			pagePortlet.getDecorParams().clear();
		}
		DecoratorUtil decoratorUtil = new DecoratorUtil(this.request.getServletContext());
		List<String> decoratorPropertyList = decoratorUtil.getDecoratorPropertyList(portletDecorator);
		for (int i=0;i < decoratorPropertyList.size();i++){
			String key = decoratorPropertyList.get(i);
			String value = param.get(key);
			pagePortlet.setPropertyValue(key, value);
		}
		pagePortlet.setDecoratorId(portletDecorator);
		
		String rspText = "Portlet配置保存至内存！";
		if (StringUtil.isNullOrEmpty(containerId)){
			portalConfigService.savePortletWindowConfig(pagePortlet);
			String pageId = pagePortlet.getPageId();
			portalConfigService.refreshPorletConfig(true,pageId);
			rspText = "OK";
		}
		return new AjaxRenderer(rspText);
		
	}
}

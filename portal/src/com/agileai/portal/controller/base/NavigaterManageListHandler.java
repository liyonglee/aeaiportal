package com.agileai.portal.controller.base;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.Resource;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.SecurityAuthorizationConfig;
import com.agileai.portal.bizmoduler.base.MenuBarManage;
import com.agileai.portal.bizmoduler.base.NavigaterManage;

public class NavigaterManageListHandler extends StandardListHandler{
	public NavigaterManageListHandler(){
		super();
		this.editHandlerClazz = NavigaterManageEditHandler.class;
		this.serviceId = buildServiceId(NavigaterManage.class);
	}
	protected void processPageAttributes(DataParam param) {
		setAttribute("navType",FormSelectFactory.create("NAVIGATER_TYPE").addSelectedValue(param.get("navType")));
		setAttribute("isValid",FormSelectFactory.create("SYS_VALID_TYPE").addSelectedValue(param.get("isValid")));
		initMappingItem("NAV_TYPE",FormSelectFactory.create("NAVIGATER_TYPE").getContent());
		initMappingItem("NAV_ISVALID",FormSelectFactory.create("SYS_VALID_TYPE").getContent());		
	}
	protected void initParameters(DataParam param) {
		initParamItem(param, "navName", "");
		initParamItem(param, "navType", "");
		initParamItem(param, "isValid", "1");		
	}
	public ViewRenderer doDeleteAction(DataParam param){
		String navId = param.get("NAV_ID");
		MenuBarManage service = this.lookupService(MenuBarManage.class);
		param.put("navId",navId);
		boolean isExistSecurityRelation = isExistSecurityRelation(navId, Resource.Type.Navigater);
		if (isExistSecurityRelation){
			this.setErrorMsg("该节点存在授权关联，先删除相关授权信息！");	
			return this.prepareDisplay(param);
		}else{
			List<DataRow> menuRecords = service.findTreeRecords(param);
			if (menuRecords != null && menuRecords.size() > 1){
				this.setErrorMsg("该导航下还存在菜单节点，不能删除！");
				return this.prepareDisplay(param);
			}else{
				return super.doDeleteAction(param);
			}
		}
	}
	private boolean isExistSecurityRelation(String resourceId,String resourceType){
		boolean result = false;
		SecurityAuthorizationConfig authorizationConfig = this.lookupService(SecurityAuthorizationConfig.class);
		List<DataRow> roleList = authorizationConfig.retrieveRoleList(resourceType, resourceId);
		if (roleList != null && roleList.size() > 0){
			result = true;
		}
		List<DataRow> userList = authorizationConfig.retrieveUserList(resourceType, resourceId);
		if (userList != null && userList.size() > 0){
			result = true;
		}
		List<DataRow> groupList = authorizationConfig.retrieveGroupList(resourceType, resourceId);
		if (groupList != null && groupList.size() > 0){
			result = true;
		}
		return result;
	}		
	
	public ViewRenderer doConfigMenuAction(DataParam param){
		this.storeParam(param);
		return new DispatchRenderer(getHandlerURL(MenuBarManageHandler.class));
	}
	protected NavigaterManage getService() {
		return (NavigaterManage)this.lookupService(this.getServiceId());
	}
}

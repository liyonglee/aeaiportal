package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.base.PtThemeManage;
import com.agileai.portal.driver.AttributeKeys;
import com.agileai.portal.driver.service.DriverConfiguration;
import com.agileai.portal.driver.service.PortalConfigService;

public class PtThemeManageEditHandler extends StandardEditHandler{
	public PtThemeManageEditHandler(){
		super();
		this.listHandlerClass = PtThemeManageListHandler.class;
		this.serviceId = buildServiceId(PtThemeManage.class);
	}
	
	protected void processPageAttributes(DataParam param) {
		setAttribute("THEME_TYPE",FormSelectFactory.create("THEME_TYPE").addSelectedValue(getAttributeValue("THEME_TYPE")));
		setAttribute("PERSONALABLE",FormSelectFactory.create("BOOL_DEFINE").addSelectedValue(getAttributeValue("PERSONALABLE","N")).addHasBlankValue(false));
		setAttribute("BODY_HEIGHT", getAttribute("BODY_HEIGHT","auto"));

		setAttribute("THEME_WIDTH_TYPE",FormSelectFactory.create("THEME_WIDTH_TYPE").addSelectedValue(getAttributeValue("THEME_WIDTH_TYPE","fixed")));
		setAttribute("THEME_WIDTH", getAttribute("THEME_WIDTH","1000"));
		setAttribute("THEME_MARGIN", getAttribute("THEME_MARGIN","0"));
		setAttribute("THEME_VNAV_WIDTH", getAttribute("THEME_VNAV_WIDTH","0"));
	}
	
	protected PtThemeManage getService() {
		return (PtThemeManage)this.lookupService(this.getServiceId());
	}
	
	public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updateRecord(param);
			PortalConfigService portalConfigService = this.getPortalConfigService();
			String themeId = param.get("THEME_ID");
			portalConfigService.refreshTheme(themeId);
		}
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	
	private PortalConfigService getPortalConfigService(){
		DriverConfiguration driverConfiguration = (DriverConfiguration)this.dispatchServlet.getServletContext().getAttribute(AttributeKeys.DRIVER_CONFIG);
		return driverConfiguration.getPortalConfigService();
	}
}
﻿CKEDITOR.plugins.add('cmsave', {
    init: function (editor) {
    	var saveCmd= {
    		    exec:function(editor){
    		    	editor.updateElement();
    		    	postRequest('form1',{actionType:"saveInfomationContent",onComplete:function(responseText){
    		    		if (responseText == 'success'){
    		    			alert("保存正文信息成功！");
    		    		}else{
    		    			alert("保存正文信息失败，请联系管理员！");
    		    		}
    		    	}});    		    	
    		    }
    	};
    	var pluginName = 'cmsave';
        editor.addCommand(pluginName, saveCmd);
        editor.ui.addButton(pluginName,
        {
            label: '保存',
            command: pluginName,
            icon: this.path + 'images/save.gif'
        });
    }
});
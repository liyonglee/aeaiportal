﻿CKEDITOR.plugins.add('comment', {
    init: function (editor) {
    	var profileCmd= {
    		    exec:function(editor){
    		    	var columnId =document.getElementById("columnId").value;
					var editMode =document.getElementById("editMode").value;
    		    	var infomationId = document.getElementById("infomationId").value;					
    		    	var url = 'index?WcmInfomationEdit&COL_ID='+columnId+'&operaType=update&INFO_ID='+infomationId+'&editMode='+editMode;
    		    	window.location.href=url;
    		    }
    		};
    	var pluginName = 'comment';
        editor.addCommand(pluginName, profileCmd);
        editor.ui.addButton(pluginName,
        {
            label: '查看评论',
            command: pluginName,
            icon: this.path + 'images/comment.png'
        });
    }
});
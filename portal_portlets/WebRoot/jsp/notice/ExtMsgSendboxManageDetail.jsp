<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>消息提醒-详细</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link href="<%=request.getContextPath()%>/css/abutton.css" rel="stylesheet" type="text/css">
<style type="text/css">
	.bordered {
		padding-top:5px;
	}
    
	.bordered td{
	    border: 1px solid #ccc;
	    padding: 10px;
	    text-align: left;    
	}
	.div_button{
		margin-top: 10px;
	}
}
</style>
</head>
<body> 
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
	<%@include file="/jsp/inc/message.inc.jsp"%>
	<table class="bordered">
	  <tr>
		<td>主题：</td>
		<td colspan="3"><%=pageBean.inputValue("MSG_THEME")%></td>
	  </tr>
	  <tr>
		<td>收件人：</td>
		<td><%=pageBean.inputValue("MSG_RECEIVEOR_NAME")%></td>
		<td>发送时间：</td>
		<td><%=pageBean.inputValue("MSG_CREATE_TIME")%></td>
	  </tr>
	  <tr>
		<td>来自：</td>
		<td><%=pageBean.inputValue("MSG_SEND_NAME")%></td>
		<td>通知方式：</td>
		<td><%=pageBean.inputValue("MSG_NOTICE_TYPE_NAME")%></td>
	  </tr>
	  <tr height="260px" valign="top">
		<td>消息内容：</td>
		<td colspan="3"><div style="overflow-y:auto; overflow-x:auto; width:700px; height:255px;"><%=pageBean.inputValue("MSG_NOTICE_CONTENT")%></div></td>
	  </tr>
	</table>
	<div class="div_button">
		<a class="a_linked_button" href="javascript:goToBack();">返回消息列表</a>
	</div>
	<input type="hidden" name="actionType" id="actionType" value=""/>
	<input type="hidden" name="msgTheme" id="msgTheme" value="<%=pageBean.inputValue("msgTheme")%>"/>
	<input type="hidden" name="userCode" id="userCode" value="<%=pageBean.inputValue("userCode")%>"/>
	<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
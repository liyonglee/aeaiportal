<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>树形列表管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function doRefresh(nodeId){
	$('#LIST_ID').val(nodeId);
	doSubmit({actionType:'refresh'});
}
var targetTreeBox;
function showParentSelectBox(){
	if (!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标目录',{size:'normal',width:'300px',top:'3px'});
	}
	var handlerId = "ExtTreelistParentSelect";
	var url = 'index?'+handlerId+'&LIST_ID='+$("#LIST_ID").val()+'&listType='+$('#listType').val();;
	targetTreeBox.sendRequest(url);
}
function doChangeParent(){
	postRequest('form1',{actionType:'changeParent',onComplete:function(responseText){
		if (responseText == 'success'){
			doRefresh($('#LIST_ID').val());			
		}else {
			alert('迁移父节点出错啦！');
		}
	}});
}
function doSave(){
	if (checkSave()){
		$("#operaType").val('update');
		doSubmit({actionType:'save'});
	}
}
function checkSave(){
	var result = true;
if (validation.checkNull($('#LIST_CODE').val())){
	writeErrorMsg($("#LIST_CODE").attr("label")+"不能为空!");
	selectOrFocus('LIST_CODE');
	return false;
}
if (validation.checkNull($('#LIST_NAME').val())){
	writeErrorMsg($("#LIST_NAME").attr("label")+"不能为空!");
	selectOrFocus('LIST_NAME');
	return false;
}
if (validation.checkNull($('#LIST_ISVALID').val())){
	writeErrorMsg($("#LIST_ISVALID").attr("label")+"不能为空!");
	selectOrFocus('LIST_ISVALID');
	return false;
}
	return result;
}
function doMoveUp(){
	doSubmit({actionType:'moveUp'});
}
function doMoveDown(){
	doSubmit({actionType:'moveDown'});
}
function doCopyCurrent(){
	doSubmit({actionType:'copyCurrent'});
}
function doDelete(){
	if (confirm('确定要进行节点删除操做吗？')){
		doSubmit({actionType:'delete'});
	}
}
function doInsertChild(){
	if (checkInsertChild()){
		$("#operaType").val('insert');
		doSubmit({actionType:'insertChild'});
	}
}
function checkInsertChild(){
	var result = true;
if (validation.checkNull($('#CHILD_LIST_CODE').val())){
	writeErrorMsg($("#CHILD_LIST_CODE").attr("label")+"不能为空!");
	selectOrFocus('CHILD_LIST_CODE');
	return false;
}
if (validation.checkNull($('#CHILD_LIST_NAME').val())){
	writeErrorMsg($("#CHILD_LIST_NAME").attr("label")+"不能为空!");
	selectOrFocus('CHILD_LIST_NAME');
	return false;
}
if (validation.checkNull($('#CHILD_LIST_ISVALID').val())){
	writeErrorMsg($("#CHILD_LIST_ISVALID").attr("label")+"不能为空!");
	selectOrFocus('CHILD_LIST_ISVALID');
	return false;
}
	return result;
}
function doCancel(){
	doRefresh($('#LIST_ID').val());
}
function refreshListType(){
	var listTypeValue = $('#listType').val();
	$('#LIST_ID').val(listTypeValue);
	doSubmit({actionType:'refresh'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;">
<tr>
	<td valign="top">
	<div id="leftTree" class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3><span style="padding-left:5px;">类型&nbsp;<select id="listType" label="列表类型" name="listType" class="select" onchange="refreshListType()"><%=pageBean.selectValue("listType")%></select></span></h3>        
	<div id="treeArea" style="overflow:auto; height:420px;width:230px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
	<%=pageBean.getStringValue("menuTreeSyntax")%></div>
    <b class="b9"></b>
    </div>
	</td>
	<td width="85%" valign="top"> 
	<fieldset id="currentArea" style="padding:0 5px 5px 5px; height:212px; ">
	  <legend style="font-weight: bolder;">编辑当前节点</legend>
	    <div id="__ToolBar__" style="margin-top: 2px">
   		<table border="0" cellpadding="0" cellspacing="1">
	    <tr>
		<td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doSave()"<%}%> class="bartdx" hotKey="E" align="center"><input id="saveImgBtn" value="&nbsp;" title="保存" type="button" class="saveImgBtn" style="margin-right:0px;" />保存</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doDelete()"<%}%> class="bartdx" hotKey="D" align="center"><input id="delImgBtn" value="&nbsp;" title="删除" type="button" class="delImgBtn" style="margin-right:0px;" />删除</td>
		<td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doCopyCurrent()"<%}%> class="bartdx" align="center"><input id="copyImgBtn" value="&nbsp;" title="复制" type="button" class="copyImgBtn" style="margin-right:0px;" />复制</td>    
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="showParentSelectBox()"<%}%> class="bartdx" align="center"><input id="moveImgBtn" value="&nbsp;" title="迁移" type="button" class="moveImgBtn" style="margin-right:0px;" />迁移</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveUp()"<%}%> class="bartdx" align="center"><input id="upImgBtn" value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveDown()"<%}%> class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td>
	    </tr></table></div>    
	    <table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="LIST_CODE" label="编码" name="LIST_CODE" type="text" value="<%=pageBean.inputValue("LIST_CODE")%>" size="64" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="LIST_NAME" label="名称" name="LIST_NAME" type="text" value="<%=pageBean.inputValue("LIST_NAME")%>" size="64" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>是否有效</th>
	<td><select id="LIST_ISVALID" label="是否有效" name="LIST_ISVALID" class="select"><%=pageBean.selectValue("LIST_ISVALID")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>对照字段1</th>
	<td><input id="REL_FIELD1" label="对照字段1" name="REL_FIELD1" type="text" value="<%=pageBean.inputValue("REL_FIELD1")%>" size="64" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>对照字段2</th>
	<td><input id="REL_FIELD2" label="对照字段2" name="REL_FIELD2" type="text" value="<%=pageBean.inputValue("REL_FIELD2")%>" size="64" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>备注说明</th>
	<td><textarea name="LIST_DESC" cols="64" rows="2" class="text" id="LIST_DESC" label="备注说明"><%=pageBean.inputValue("LIST_DESC")%></textarea></td>
</tr>
	    </table>
	</fieldset>	
    
	<fieldset id="childArea" style="margin-top: 10px;padding: 5px;height:211px;">
	  <legend style="font-weight: bolder;">添加子节点</legend>
	    <div id="__ToolBar__">
		<table border="0" cellpadding="0" cellspacing="1">
	    <tr>
		<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doInsertChild()"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" style="margin-right:0px;" />新增</td>
		<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doCancel()"><input value="&nbsp;" title="取消" type="button" class="cancelImgBtn" style="margin-right:0px;" />取消</td>    
	    </tr></table>
	    </div>     
	    <table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="CHILD_LIST_CODE" label="编码" name="CHILD_LIST_CODE" type="text" value="<%=pageBean.inputValue("CHILD_LIST_CODE")%>" size="64" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="CHILD_LIST_NAME" label="名称" name="CHILD_LIST_NAME" type="text" value="<%=pageBean.inputValue("CHILD_LIST_NAME")%>" size="64" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>是否有效</th>
	<td><select id="CHILD_LIST_ISVALID" label="是否有效" name="CHILD_LIST_ISVALID" class="select"><%=pageBean.selectValue("CHILD_LIST_ISVALID")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>对照字段1</th>
	<td><input id="CHILD_REL_FIELD1" label="对照字段1" name="CHILD_REL_FIELD1" type="text" value="<%=pageBean.inputValue("CHILD_REL_FIELD1")%>" size="64" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>对照字段2</th>
	<td><input id="CHILD_REL_FIELD2" label="对照字段2" name="CHILD_REL_FIELD2" type="text" value="<%=pageBean.inputValue("CHILD_REL_FIELD2")%>" size="64" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>备注说明</th>
	<td><textarea name="CHILD_LIST_DESC" cols="64" rows="2" class="text" id="CHILD_LIST_DESC" label="备注说明"><%=pageBean.inputValue("CHILD_LIST_DESC")%></textarea></td>
</tr>
	    </table>
	</fieldset>          
    </td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value=""/>
<input type="hidden" id="LIST_ID" name="LIST_ID" value="<%=pageBean.inputValue("LIST_ID")%>" />
<input type="hidden" id="LIST_TYPE" name="LIST_TYPE" value="<%=pageBean.selectedValue("listType")%>" />
<input type="hidden" id="CHILD_LIST_TYPE" name="CHILD_LIST_TYPE" value="<%=pageBean.selectedValue("listType")%>" />
<input type="hidden" id="LIST_PID" name="LIST_PID" value="<%=pageBean.inputValue("LIST_PID")%>" />
<input type="hidden" id="LIST_SORT" name="LIST_SORT" value="<%=pageBean.inputValue("LIST_SORT")%>" />
</form>
</body>
</html>
<script language="javascript">
<%if(pageBean.getBoolValue("isRootNode")){%>
setImgDisabled('saveImgBtn',true);
setImgDisabled('delImgBtn',true);
setImgDisabled('copyImgBtn',true);
setImgDisabled('moveImgBtn',true);
setImgDisabled('upImgBtn',true);
setImgDisabled('downImgBtn',true);
<%}%>
$(window).load(function() {
	setTimeout(function(){
		resetTreeHeight(70);
		var areaHeight = $("#leftTree").height()/2;
		$("#currentArea").height(areaHeight-2);
		$("#childArea").height(areaHeight-2);			
	},1);	
});
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
<script language="javascript">
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>数据列表管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="LIST_CODE" label="编码" name="LIST_CODE" type="text" value="<%=pageBean.inputValue("LIST_CODE")%>" size="64" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="LIST_NAME" label="名称" name="LIST_NAME" type="text" value="<%=pageBean.inputValue("LIST_NAME")%>" size="64" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>排序</th>
	<td><input id="LIST_SORT" label="排序" name="LIST_SORT" type="text" value="<%=pageBean.inputValue("LIST_SORT")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>是否有效</th>
	<td><select id="LIST_ISVALID" label="是否有效" name="LIST_ISVALID" class="select"><%=pageBean.selectValue("LIST_ISVALID")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>对照字段1</th>
	<td><input id="REL_FIELD1" label="对照字段1" name="REL_FIELD1" type="text" value="<%=pageBean.inputValue("REL_FIELD1")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>对照字段2</th>
	<td><input id="REL_FIELD2" label="对照字段2" name="REL_FIELD2" type="text" value="<%=pageBean.inputValue("REL_FIELD2")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>备注说明</th>
	<td><textarea name="LIST_DESC" cols="64" rows="3" class="text" id="LIST_DESC" label="备注说明"><%=pageBean.inputValue("LIST_DESC")%></textarea></td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="LIST_ID" name="LIST_ID" value="<%=pageBean.inputValue4DetailOrUpdate("LIST_ID","")%>" />
<input type="hidden" id="LIST_TYPE" name="LIST_TYPE" value="<%=pageBean.inputValue("LIST_TYPE")%>" />
</form>
<script language="javascript">
requiredValidator.add("LIST_CODE");
requiredValidator.add("LIST_SORT");
intValidator.add("LIST_SORT");
requiredValidator.add("LIST_ISVALID");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>

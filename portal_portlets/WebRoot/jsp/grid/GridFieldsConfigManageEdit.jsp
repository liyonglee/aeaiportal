<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>表格字段管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save',checkUnique:'y'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="CODE" label="编码" name="CODE" type="text" value="<%=pageBean.inputValue("CODE")%>" size="24" class="text" <%=pageBean.readonly(!"insert".equals(pageBean.getOperaType()))%> />
</td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="NAME" label="名称" name="NAME" type="text" value="<%=pageBean.inputValue("NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>列类型</th>
	<td><select id="FIELD_TYPE" label="列类型" name="FIELD_TYPE" class="select"><%=pageBean.selectValue("FIELD_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>宽度</th>
	<td><input id="WIDTH" label="宽度" name="WIDTH" type="text" value="<%=pageBean.inputValue("WIDTH")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>对齐类型</th>
	<td><select id="ALIGN_TYPE" label="对齐类型" name="ALIGN_TYPE" class="select"><%=pageBean.selectValue("ALIGN_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>表头对齐类型</th>
	<td><select id="HD_ALIGN_TYPE" label="表头对齐类型" name="HD_ALIGN_TYPE" class="select"><%=pageBean.selectValue("HD_ALIGN_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>能否拖动</th>
	<td><%=pageBean.selectRadio("CAN_MOVE")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>能否排序</th>
	<td><%=pageBean.selectRadio("CAN_SORT")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>是否隐藏</th>
	<td><%=pageBean.selectRadio("IS_HIDDEN")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>是否冻结</th>
	<td><%=pageBean.selectRadio("IS_FROZE")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>是否编组</th>
	<td><%=pageBean.selectRadio("IS_GROUP")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>渲染类型</th>
	<td><select id="RENDER_TYPE" label="渲染类型" name="RENDER_TYPE" class="select"><%=pageBean.selectValue("RENDER_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>渲染函数</th>
	<td><textarea id="RENDER_FUNC" label="渲染函数" name="RENDER_FUNC" cols="60" rows="4" class="textarea"><%=pageBean.inputValue("RENDER_FUNC")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="ID" name="ID" value="<%=pageBean.inputValue("ID")%>" />
<input type="hidden" id="pageCode" name="pageCode" value="<%=pageBean.inputValue("pageCode")%>" />
<input type="hidden" id="portletCode" name="portletCode" value="<%=pageBean.inputValue("portletCode")%>" />
</form>
<script language="javascript">
requiredValidator.add("NAME");
lengthValidators[0].set().add("NAME");
requiredValidator.add("CODE");
lengthValidators[1].set(64).add("CODE");
requiredValidator.add("FIELD_TYPE");
lengthValidators[2].set(32).add("FIELD_TYPE");
requiredValidator.add("WIDTH");
intValidator.add("WIDTH");
requiredValidator.add("ALIGN_TYPE");
requiredValidator.add("HD_ALIGN_TYPE");
lengthValidators[3].set(1).add("CAN_MOVE");
lengthValidators[4].set(1).add("CAN_SORT");
lengthValidators[5].set(1).add("IS_HIDDEN");
lengthValidators[6].set(1).add("IS_FROZE");
lengthValidators[7].set(1).add("IS_GROUP");
lengthValidators[8].set(32).add("RENDER_TYPE");
lengthValidators[9].set(1024).add("RENDER_FUNC");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>

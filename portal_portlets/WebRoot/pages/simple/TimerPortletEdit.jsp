<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String isVisiable = (String)request.getAttribute("isVisiable"); 
%>
<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
    <tr>
      <td width="120">时间间隔列表</td>
      <td>
        <input type="text" name="timerRangeList" id="timerRangeList" value="${timerRangeList}" />
        （秒）      </td>
    </tr>
    <tr>
      <td width="120">默认间隔</td>
      <td>
        <input type="text" name="defaultRange" id="defaultRange" value="${defaultRange}" />
        （秒）      </td>
    </tr>
    <tr>
      <td width="120">是否显示</td>
      <td>
        <input type="radio" name="isVisiable" id="isVisiableY" value="Y" />是&nbsp;&nbsp;
        <input type="radio" name="isVisiable" id="isVisiableN" value="N" />否</td>
    </tr>  
    <tr>
      <td width="120">触发函数</td>
      <td><textarea name="jsHandler" cols="64" rows="5" id="jsHandler">${jsHandler}</textarea></td>
    </tr>            
	<tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">	
<%if ("Y".equals(isVisiable)){%>
	$('#<portlet:namespace/>Form #isVisiableY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isVisiableN').attr("checked","checked");
<%}%>
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String isCache = (String)request.getAttribute("isCache"); 
%>
<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
	<tr>
		<td width="120">是否自动隐藏&nbsp;<span style="color: red;">*</span></td>
		<td>
			<input type="radio" name="isHide" id="isHide" value="true"  <c:if test="${isHide}">checked="checked"</c:if>/>是&nbsp;&nbsp;
			<input type="radio" name="isHide" id="isHide" value="false" <c:if test="${!isHide}">checked="checked"</c:if>/>否
		</td>
	</tr>
	<tr>
		<td width="120">是否立即加载&nbsp;<span style="color: red;">*</span></td>
		<td>
			<input type="radio" name="immediateLoad" id="immediateLoad" value="true"  <c:if test="${immediateLoad}">checked="checked"</c:if>/>是&nbsp;&nbsp;
			<input type="radio" name="immediateLoad" id="immediateLoad" value="false" <c:if test="${!immediateLoad}">checked="checked"</c:if>/>否
		</td>
	</tr>	
	<tr>
		<td width="120">显示时间&nbsp;<span style="color: red;">*</span></td>
		<td><input type="text" size="50" name="time" id="time" value="${time}" />ms</td>
	</tr>
	<tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">	
<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>Form #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isCacheN').attr("checked","checked");
<%}%>
</script>

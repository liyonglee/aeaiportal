<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>
<%@page import="java.util.HashMap"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/calendar.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/js/calendar.js"></script>
<%
String portletId = (String)request.getAttribute("portletId");

String namespace = (String)request.getAttribute("NAMESPACE");
String isSetting = (String)request.getAttribute("isSetting");
String isConvertText = (String)request.getAttribute("isConvertText");
String extBarSyntax = (String)request.getAttribute("extBarSyntax");
String onchangeHandler = (String)request.getAttribute("onchangeHandler");
String extendEnumDataURL = (String)request.getAttribute("extendEnumDataURL");
String isExtendLink = (String)request.getAttribute("isExtendLink");
String defaultViriable = (String)request.getAttribute("defaultViriable");

if ("true".equals(isSetting)){
%>
<form style='margin:3px 2px 3px 8px;;padding:2px;' id='<%=namespace%>_search'>
<span id='extQueryBar' style='display:inline-block;margin-bottom:3px;'>
<%=extBarSyntax%>
</span>

<span id='querybtn_span'>
<input type='button' name='btnSearch' onclick='doSearch<portlet:namespace/>();' style="cursor:pointer" id='btnSearch' value='查 询' />
</span>

<%
if ("Y".equals(isExtendLink)){
%>
<span style="float:right;line-height: 25px;">
<%=request.getAttribute("extBarLinkSyntax")%>
</span>
<%}%>

<script language="javascript">
function enterPress<portlet:namespace/>(){ 
	if ( event.keyCode == 13 ) 
    { 
		doSearch<portlet:namespace/>(); 
    } 
}
function doSearch<portlet:namespace/>(){ 
	var params = ''; 
	params = appendParams(params);
	doAjaxRefreshPortalPage(params,{curPortletId:"<%=portletId%>"});
}
function appendParams(params){
	$("#extQueryBar input[type='text']").each(function(index, domEle){
		var paramName = $(domEle).attr("name"); 
		params=params+","+paramName+":"+ $(domEle).val();
	});
	$("#extQueryBar select").each(function(index, domEle){
		var paramName = $(domEle).attr("name"); 
		params=params+","+paramName+":"+ $(domEle).val();
	});
	var checkBoxMap = new Map();
	$("#extQueryBar input[type='checkbox']").each(function(index, domEle){
		var tempCheckBoxName = $(domEle).attr("name");
		if (!checkBoxMap.containsKey(tempCheckBoxName)){
			checkBoxMap.put(tempCheckBoxName,'');
		}
		if ($(domEle).attr('checked')){
			var tempCheckBoxValue = checkBoxMap.get(tempCheckBoxName);
			if (tempCheckBoxValue == ''){
				tempCheckBoxValue = $(domEle).val();				
			}else{
				tempCheckBoxValue = tempCheckBoxValue + ";" + $(domEle).val();
			}
			checkBoxMap.remove(tempCheckBoxName);
			checkBoxMap.put(tempCheckBoxName,tempCheckBoxValue);
		}
	});
	if (!checkBoxMap.isEmpty()){
		for (var i=0;i < checkBoxMap.size();i++){
			var element = checkBoxMap.element(i);
			var paramName = element.key;
			var paramValue = element.value;
			params=params+","+paramName+":" + paramValue;
		}
	}
	
	<%
	if ("Y".equals(isConvertText)){
	%>
	var convertKeys = "";
	$("#extQueryBar input[type='text']").each(function(index, domEle){
		convertKeys = convertKeys+";"+$(domEle).attr("name");
	});
	if (convertKeys.length > 1){
		params=params+",convertKeys:"+convertKeys.substring(1,convertKeys.length);	
	}
	<%}%>
	if (params.length > 1){
		params=params.substring(1,params.length);	
	}
	return params;
}
var __renderPortlet<portlet:namespace/> = function(){
	var data = <%=defaultViriable%>;
	var extendEnumDataURL = "<%=extendEnumDataURL%>";
	if(extendEnumDataURL != null && extendEnumDataURL.trim()!=""){		
		sendAction('${getAjaxDataURL}',{dataType:'text',onComplete:function(responseText){
			data = jQuery.parseJSON(responseText);
		}});
	}
	buildQueryBar<portlet:namespace/>(data);
	doSearch<portlet:namespace/>();
};
__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
__renderPortlet<portlet:namespace/>();

function buildQueryBar<portlet:namespace/>(data){
	for (var i=0;i < data.length;i++){
		var formAtom = data[i];
		var atomId = formAtom.id;
		var atomType = formAtom.type;
		
		if ("SELECT" == atomType){
			$("#"+atomId).empty();
			var values = formAtom.values;
			if (values){
				for (var j=0;j < values.length;j++){
					var temp = values[j];
					var optionText = temp.value;
					var optionValue = temp.code;
					$("#"+atomId).append("<option value='"+optionValue+"'>"+optionText+"</option>");
					if (temp.selected){
						$("#"+atomId).val(optionValue);
					}
				}
			}
		}
		if ("CHECKBOX" == atomType){
			var values = formAtom.values;
			$("#"+atomId+"Span").html("");
			for (var j=0;j < values.length;j++){
				var temp = values[j];
				var text = temp.value;
				var value = temp.code;
				var checkBoxId = atomId+"_"+ j;
				var checkboxHtml = "<label><input type='checkbox' name='" + atomId +"' value='" + value +"' id='" + checkBoxId +"' />";
				checkboxHtml = checkboxHtml + text + "</label>";
				$("#"+atomId+"Span").append(checkboxHtml);	
				if (temp.selected){
					$("#"+checkBoxId).attr("checked",true);
				}
			}
			var $currentbox=$("[name=atomId]:checkbox");
			$currentbox.click(function(){<%=onchangeHandler%>}); 
		}
		if ("TEXT" == atomType){
			var atomValue = formAtom.value;
			$("#"+atomId).val(atomValue);
		}
		if ("DATEPICK" == atomType){
			initCalendar(atomId,'%Y-%m-%d',atomId + 'Picker');
			var atomValue = formAtom.value;
			$("#"+atomId).val(atomValue);
		}
		if ("TIMEPICK" == atomType){
			initCalendar(atomId,'%Y-%m-%d %H:%M',atomId + 'Picker');
			var atomValue = formAtom.value;
			$("#"+atomId).val(atomValue);
		}
	}
}

function doReloadQueryBar<portlet:namespace/>(){
	var params = appendParams('');
	var datas = parsePortalParams(params);
	var targetPage = parsePageId();
	datas = 'targetPage='+targetPage+'&'+datas;
	$.ajax({
	   type: "POST",
	   url: "/portal/ParamInteractive",
	   data:datas,
	   dataType:'text',
	   success:function(msg){
		   __renderPortlet<portlet:namespace/>();			   
	   }
	});
}
</script>
</form>
<%
}else{
out.println("请正确设置属性");
}
%>
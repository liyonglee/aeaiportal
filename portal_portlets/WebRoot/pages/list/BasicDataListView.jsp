<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getDataHtml" var="getDataHtmlURL">
</portlet:resourceURL>
<%
String portletId = (String)request.getAttribute("portletId");
String tdClass = (String)request.getAttribute("tdClass");
if (tdClass != null && !"".equals(tdClass)){
%>
<style type="text/css">
.datalistmenu a{height:22px;line-height:22px;font-size:14px;font-weight: bold;color:black;text-decoration: none; display:block;}
<%if ("1".equals(tdClass)){%>
.datalistmenu a:hover{color:navy;background-color:silver;position:relative;}
<%}else if ("2".equals(tdClass)){%>
.datalistmenu tr td {background-image:url(<%=request.getContextPath()%>/pages/list/images/1.gif);background-repeat:no-repeat; background-position:left;}
.datalistmenu a{margin-left:20px;padding-left: 5px;}
.datalistmenu a:hover{color:navy;background-color:silver;position:relative;}
<%}else if ("3".equals(tdClass)){%>
.datalistmenu tr td {background-image:url(<%=request.getContextPath()%>/pages/list/images/2.gif);background-repeat:no-repeat; background-position:left;}
.datalistmenu a{margin-left:20px;padding-left: 5px;}
.datalistmenu a:hover{color:navy;background-color:silver;position:relative;}
<%}else if ("4".equals(tdClass)){%>
.datalistmenu tr td {background-image:url(<%=request.getContextPath()%>/pages/list/images/3.gif);background-repeat:no-repeat; background-position:left;}
.datalistmenu a{margin-left:20px;padding-left: 5px;}
.datalistmenu a:hover{color:navy;background-color:silver;position:relative;}
<%}else if ("5".equals(tdClass)){%>
.datalistmenu tr td {background-image:url(<%=request.getContextPath()%>/pages/list/images/4.gif);background-repeat:no-repeat; background-position:left;}
.datalistmenu a{margin-left:20px;padding-left: 5px;}
.datalistmenu a:hover{color:navy;background-color:silver;position:relative;}
<%}else if ("6".equals(tdClass)){%>
.datalistmenu tr td {background-image:url(<%=request.getContextPath()%>/pages/list/images/5.gif);background-repeat:no-repeat; background-position:left;}
.datalistmenu a{margin-left:20px;padding-left: 5px;}
.datalistmenu a:hover{color:navy;background-color:silver;position:relative;}
<%}else{%>
.datalistmenu a:hover{color:navy;background-color:silver;position:relative;}
<%}%>
.datalistmenu tr td.selected{background-color:#69C;}
</style>
<div id="<portlet:namespace/>div" style="height:${height}px;padding:2px" class="datalistmenu">
</div>
<script type="text/javascript">
var __renderPortlet<portlet:namespace/> = function(){
	sendAction('${getDataHtmlURL}',{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>div').html(responseText);
	}});
};
__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
__renderPortlet<portlet:namespace/>();
</script>
 <%
 }else{
	out.println("请正确设置相关属性！");
}
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>
<%
String portletId = (String)request.getAttribute("portletId");
String width = (String)request.getAttribute("width");
String isSetting = (String)request.getAttribute("isSetting");
String isMultiple = (String)request.getAttribute("isMultiple");
String widthSyntax = "";
if("true".equals(isSetting)){
	if (width != null && !width.trim().equals("")){
		widthSyntax = "width:"+width + "px;";
	}
%>
<%if("Y".equals(isMultiple)){%>
<link rel="stylesheet" type="text/css" href="/portal_portlets/css/stree.css" />
<script type="text/javascript" language="javascript" src="/portal_portlets/js/pstree.js"></script>
<%}else{%>
<link rel="stylesheet" type="text/css" href="/portal_portlets/css/dtree.css" />
<script type="text/javascript" language="javascript" src="/portal_portlets/js/pdtree.js"></script>
<%}%>
<div id="<portlet:namespace/>Div" style="height:${height}px;padding:2px;margin:3px;overflow:auto;<%=widthSyntax%>">
</div>
<script type="text/javascript">
$(function(){
	var __renderPortlet<portlet:namespace/> = function(){
		sendAction('${getAjaxDataURL}',{dataType:'text',onComplete:function(responseText){
			//alert(responseText);
			eval(responseText);
			<%if("Y".equals(isMultiple)){%>
			var html = tree.toString();
			<%}else{%>			
			var html = d.toString();
			<%}%>
			//alert(html);
			$("#<portlet:namespace/>Div").html(html);
		}});
	};
	<%if("N".equals(isMultiple)){%>
	__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
	<%}%>
	__renderPortlet<portlet:namespace/>();
});
</script> 
<%
}else{
	out.println("请正确设置相关属性！");
} 
%>

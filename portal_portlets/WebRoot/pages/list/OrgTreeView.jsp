<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getDataText" var="getDataTextURL">
	<portlet:param name="dataURL" value="${dataURL}"/>
</portlet:resourceURL>
<%
String isSetting = (String)request.getAttribute("isSetting");
String namespace = (String)request.getAttribute("namespace");
String height = (String)request.getAttribute("height");
String mrpl = (String)request.getAttribute("mrpl");
%>
<script type="text/javascript">
var defaultOrientation = '<%=mrpl%>';
</script>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/base.css" />
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/Spacetree.css" />
<!--[if IE]><script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/js/excanvas.min.js"></script><![endif]--> 
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/jit.js"></script>
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/orgtree.js"></script>
<form id="<portlet:namespace/>OrgTreeConfig">
<div id="infovis" style="height:${height}px;padding:2px;overflow:hidden;"></div>
<table>
    <tr>
        <td><div style="font-size:bold">&nbsp;&nbsp;排列方式&nbsp;&nbsp;</div></td>
        <td>
            <input type="radio" id="r-left" name="orientation" checked="checked" value="left" />left&nbsp;&nbsp;
        </td>
         <td>
            <input type="radio" id="r-top" name="orientation" value="top" />top&nbsp;&nbsp;
         </td>
          <td> 
           <input type="radio" id="r-right" name="orientation" value="right" />right&nbsp;&nbsp;
          </td>
          <td>
            <input type="radio" id="r-bottom" name="orientation" value="bottom" />bottom&nbsp;&nbsp;
          </td>
    </tr>
</table>
<div id="log"></div>
</form>
<script>
<%if ("left".equals(mrpl)){%>
	$('#<portlet:namespace/>OrgTreeConfig #r-left').attr("checked","checked");
<%}else if ("top".equals(mrpl)){%>
	$('#<portlet:namespace/>OrgTreeConfig #r-top').attr("checked","checked");
	
<%}else if ("right".equals(mrpl)){%>
	$('#<portlet:namespace/>OrgTreeConfig #r-right').attr("checked","checked");
<%}else if ("bottom".equals(mrpl)){%>
	$('#<portlet:namespace/>OrgTreeConfig #r-bottom').attr("checked","checked");
<%}%>
var jsonData = null;
sendAction('${getDataTextURL}',{dataType:'text',asynchronous:'false',onComplete:function(responseText){
	if(responseText != null){
		jsonData= eval( '(' + responseText + ')' );
	}
}});
init(jsonData);
</script>
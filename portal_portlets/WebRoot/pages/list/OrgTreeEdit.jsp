<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String isCache = (String)request.getAttribute("isCache"); 
String mrpl = (String)request.getAttribute("mrpl"); 
%>


<form id="<portlet:namespace/>OrgTreeConfig">
<table width="600" border="1" >
    <tr>
		<td width="120">高度&nbsp;<span style="color: red;">*</span></td>
		<td><input type="text" size="50" name="height" id="height" value="${height}" /></td>
	</tr>
	<tr>
      <td width="120">数据路径&nbsp;<span style="color: red;">*</span></td>
      <td>
        <input name="dataURL" type="text" id="dataURL" value="${dataURL}" size="60" /></td>
    </tr>
     <tr>
      <td width="120">默认值</td>
      <td>
        <input type="text" name="defaultVariableValues" id="defaultVariableValues" size="50" value="${defaultVariableValues}" />
      </td>
    </tr>
	<tr>
		<td width="120">命名空间&nbsp;<span style="color: red;">*</span></td>
		<td><input type="text" size="50" name="namespace" id="namespace" value="${namespace}" /></td>
	</tr>
	 <tr>
      <td width="120">默认排列</td>
      <td>
        <input type="radio" name="mrpl" id="mrpll" value="left" />left&nbsp;&nbsp;
        <input type="radio" name="mrpl" id="mrplt" value="top" />top&nbsp;&nbsp;
        <input type="radio" name="mrpl" id="mrplr" value="right" />right&nbsp;&nbsp;
        <input type="radio" name="mrpl" id="mrplb" value="bottom" />bottom&nbsp;&nbsp;
      </td>
    </tr> 
    <tr>
      <td width="120">是否缓存</td>
      <td>
        <input type="radio" name="isCache" id="isCacheY" value="Y" />是&nbsp;&nbsp;
        <input type="radio" name="isCache" id="isCacheN" value="N" />否</td>
    </tr> 
    <tr>
      <td width="120">缓存时间</td>
      <td>
        <input type="text" name="cacheMinutes" id="cacheMinutes" value="${cacheMinutes}" /> 分钟</td>
    </tr> 
    <tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>OrgTreeConfig'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
</table>

</form>
<script language="javascript">	
$("#<portlet:namespace/>OrgTreeConfig #cols").val("${cols}");	
<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>OrgTreeConfig #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>OrgTreeConfig #isCacheN').attr("checked","checked");
<%}%>
<%if ("left".equals(mrpl)){%>
	$('#<portlet:namespace/>OrgTreeConfig #mrpll').attr("checked","checked");
<%}else if ("top".equals(mrpl)){%>
	$('#<portlet:namespace/>OrgTreeConfig #mrplt').attr("checked","checked");
<%}else if ("right".equals(mrpl)){%>
	$('#<portlet:namespace/>OrgTreeConfig #mrplr').attr("checked","checked");
<%}else if ("bottom".equals(mrpl)){%>
	$('#<portlet:namespace/>OrgTreeConfig #mrplb').attr("checked","checked");
<%}%>
</script>
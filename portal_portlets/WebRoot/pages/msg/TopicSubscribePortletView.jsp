<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<%
String isSetting = (String)request.getAttribute("isSetting");
String httpSessionId = request.getSession().getId();
if("true".equals(isSetting)){
%>
<script language="javascript">
var messageHandler = {
	_excute:${jsHandler}
};

$(document).ready(function(){
	var listenerId = "<portlet:namespace/><%=httpSessionId%>";
	amq.addListener(listenerId, 'topic://${topicName}', messageHandler._excute);
});
window.onbeforeunload=function(){
	var listenerId = "<portlet:namespace/><%=httpSessionId%>";
	amq.removeListener(listenerId, 'topic://${topicName}');
	//return "请点击取消留在此页";
}
</script>
<%
}else{
	out.println("请正确相关设置属性！");
} 
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/pagination.css" type="text/css">
<script src="<%=request.getContextPath()%>/js/jquery.pagination.js" type="text/javascript" language="javascript"></script>

<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>

<%
	String portletId = (String)request.getAttribute("portletId");
%>
<div id="Searchresult"></div>
<div class="pagelist" style="height:25px">
<div id="Pagination" style="float:right;margin-top:5px" class="pagination"></div>
</div>
<script>

//首先利用这个js需要进行两次后台的访问。
//第一次是查找到分页的个数，也就是说分几页。
//第二次是查找当前页的数据

var items_per_page=10; 
var pageIndex=0;     
var recordtotal=${total};

$(function(){
	var __renderPortlet<portlet:namespace/> = function(){
        $("#Pagination").pagination(recordtotal,{
	        callback:PageCallback,  
	        items_per_page:items_per_page,  //每页显示的条目数
	        num_display_entries:10,  //默认值10可以不修改
	        num_edge_entries:1,  //两侧显示的首尾分页的条目数
	        prev_text:"上一页",
	        next_text:"下一页", 
	        current_page:pageIndex //当前页索引
        });
	};
	__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
	__renderPortlet<portlet:namespace/>();
});                  

//翻页调用的函数
function PageCallback(index, jq) {

	InitTable(index);
    return false;         
} 

//请求分页数据 
function InitTable(pageIndex) {
	
    sendAction('${getAjaxDataURL}',{data:'pageNum='+pageIndex+'&pageSize='+items_per_page+'&contentId=<%=request.getAttribute("contentId")%>',dataType:'text',onComplete:function(responseText){
		if (responseText){
			$('#Searchresult').html("");
			$('#Searchresult').append(responseText);
		}
	}});
}
</script>
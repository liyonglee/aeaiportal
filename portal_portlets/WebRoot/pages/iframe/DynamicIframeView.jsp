<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getIframeSrc" var="getIframeSrcURL">
</portlet:resourceURL>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
String portletId = (String)request.getAttribute("portletId");
String iframeURL = (String)request.getAttribute("iframeURL");
%>
<% 
if (iframeURL != null){
%>
<iframe id="<portlet:namespace/>DynamicIframe" frameborder="no" border="0" marginwidth="0" marginheight="0" src="<%=iframeURL%>" scrolling="auto"  width="100%" height="${iframeHeight}"></iframe>
<%}else{%>
<iframe id="<portlet:namespace/>DynamicIframe" frameborder="no" border="0" marginwidth="0" marginheight="0" src="" scrolling="auto"  width="100%" height="${iframeHeight}"></iframe>
<script language="javascript">
$(function(){
	var __renderPortlet<portlet:namespace/> = function(){
		sendAction('${getIframeSrcURL}',{dataType:'text',onComplete:function(responseText){
			var url = responseText;
			$('#<portlet:namespace />DynamicIframe').attr('src',url+'randomnumber='+Math.floor(Math.random()*100000));
		}});
	};
	__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
	__renderPortlet<portlet:namespace/>();	
}); 
</script>
<%}%>

<%
}else{
	out.println("请正确设置相关属性！");
}
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<portlet:actionURL name="saveIframe" var="saveIframeURL"></portlet:actionURL>
<portlet:actionURL name="cancelChangeIframe" var="cancelChangeIframeURL"></portlet:actionURL>
<portlet:actionURL name="resetChangeIframe" var="resetChangeIframeURL"></portlet:actionURL>

<script>
    $(function() {
        if('${scroll}' == 'auto'||'${scroll}' == 'yes') {
            $('#<portlet:namespace />iframeConfigDiv1 input[name="scroll"]').attr('checked', 'checked');
        }
        if('${autoExtend}' == 'true') {
            $('#<portlet:namespace />iframeConfigDiv1 input[name="autoExtend"]').attr('checked', 'checked');
        }
        if('${pxOrPercent}' == '%'){
            $('#<portlet:namespace />iframeConfigDiv1 option[value="%"]').attr('selected', 'selected');
        }
        else{
            $('#<portlet:namespace />iframeConfigDiv1 option[value="px"]').attr('selected', 'selected');
        }
     });
     
    function <portlet:namespace />iframeUpdata() {
        submitAction('${saveIframeURL}',{formId:'<portlet:namespace />iframeConfigForm'});
    }
</script>


<div id="<portlet:namespace />iframeConfigDiv1" style="margin: 10px;">
<form id="<portlet:namespace />iframeConfigForm">
<table align="center" style="margin-top: 20px; line-height: 30px;">
<tr>
        <td>
        地址：        </td>
        <td>
        <input type="text" name="iframeSrc" value="${src}" size="60" />        </td>
    </tr>
    <tr>
        <td>
        宽度：        </td>
        <td>
            <input type="text" name="iframeWidth" value="${width}" size="10" />
            <select name="pxOrPercent" >
                    <option value="%">%</option>
                 	<option value="px">PX</option>
       	    </select>        </td>
    </tr>
    <tr>
        <td>
        高度：        </td>
        <td>
        <input type="text" name="iframeHeight" value="${height}" size="10" />(单位：px)</td>
    </tr>
    <tr>
            <td>是否显示滚动条：</td>
            <td>
            <input type="checkbox" name="scroll" value="true" />            </td>
        </tr>
    <tr>
            <td>是否自动伸展高度</td>
            <td>
            <input type="checkbox" name="autoExtend" value="true" />            </td>
      </tr>        
    <tr>
        <td colspan="2" align="center">
        <input onclick="<portlet:namespace />iframeUpdata();" type="button" value="保存" />
        <input onclick="fireAction('${cancelChangeIframeURL}');" type="button" value="取消" />
        <input onclick="fireAction('${resetChangeIframeURL}');" type="button" value="重置" />        </td>
    </tr>
</table>
</form>
</div>

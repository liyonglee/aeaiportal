package com.agileai.portal.portlets.list;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;

import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.portlets.BaseMashupPortlet;

public class BasicDataListPortlet extends BaseMashupPortlet {
	
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String height = preferences.getValue("height", null);
		String tdClass = preferences.getValue("tdClass", null);

		request.setAttribute("height", height);
		request.setAttribute("tdClass", tdClass);
		request.setAttribute("portletId", request.getWindowID());
		super.doView(request, response);
	}
	
	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		String tdClass = preferences.getValue("tdClass", null);
		String height = preferences.getValue("height", null);
		String dataURL = preferences.getValue("dataURL", null);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		String cacheMinutes = preferences.getValue("cacheMinutes", defaultCacheMinutes);
		
		request.setAttribute("defaultVariableValues", defaultVariableValues);
		request.setAttribute("tdClass", tdClass);
		request.setAttribute("height", height);
		request.setAttribute("dataURL", dataURL);
		request.setAttribute("isCache", isCache);
		request.setAttribute("cacheMinutes", cacheMinutes);
		
		super.doEdit(request, response);
	}
	
	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,IOException, PreferenceException {
		
		String defaultVariableValues = request.getParameter("defaultVariableValues");
		String height = request.getParameter("height");
		String tdClass = request.getParameter("tdClass");
		String dataURL = request.getParameter("dataURL");
		String isCache = request.getParameter("isCache");
		String cacheMinutes = request.getParameter("cacheMinutes");
		
		PreferencesWrapper preferWapper = new PreferencesWrapper();
		preferWapper.setValue("defaultVariableValues", defaultVariableValues);
		preferWapper.setValue("tdClass", tdClass);
		preferWapper.setValue("height", height);
		preferWapper.setValue("dataURL", dataURL);
		preferWapper.setValue("isCache", isCache);
		preferWapper.setValue("cacheMinutes", cacheMinutes);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
}
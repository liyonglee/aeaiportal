package com.agileai.portal.portlets.msg;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;

import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.util.StringUtil;

public class TopicSubscribePortlet extends GenericPotboyPortlet {
	private static final String NewLine = "\r\n";
	private String getDefaultJsHandler(){
		StringBuffer result = new StringBuffer();
		result.append("function(message){").append(NewLine);
		result.append("	 if (message){").append(NewLine);	
		result.append("		var messageText = $(message).text();").append(NewLine);
		result.append("		var jsonObject = jQuery.parseJSON(messageText);").append(NewLine);
		result.append("		var params = jsonObject.someCode + \":\" + jsonObject.someCodeValue;").append(NewLine);
		result.append("		doAjaxRefreshPortalPage(params);").append(NewLine);
		result.append("  }").append(NewLine);
		result.append("}").append(NewLine);
		return result.toString();
	}
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String topicName = preferences.getValue("topicName", null);
		String jsHandler = preferences.getValue("jsHandler", null);

		boolean isSetting = StringUtil.isNotNullNotEmpty(topicName) 
				&& StringUtil.isNotNullNotEmpty(jsHandler);
		
		request.setAttribute("topicName", topicName);
		request.setAttribute("jsHandler", jsHandler);
		request.setAttribute("isSetting", String.valueOf(isSetting));
		
		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String topicName = preferences.getValue("topicName", null);
		String jsHandler = preferences.getValue("jsHandler", getDefaultJsHandler());

		request.setAttribute("topicName", topicName);
		request.setAttribute("jsHandler", jsHandler);
				
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String topicName = request.getParameter("topicName");
		String jsHandler = request.getParameter("jsHandler");

		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("topicName", topicName);
		preferWapper.setValue("jsHandler", jsHandler);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
}


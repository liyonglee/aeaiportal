package com.agileai.portal.portlets.iframe;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;

import com.agileai.portal.driver.Resource;
import com.agileai.portal.driver.common.PortletVariableHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.portlets.BaseMashupPortlet;
import com.agileai.portal.portlets.PortletCacheManager;
import com.agileai.util.StringUtil;

public class GridIframePortlet extends BaseMashupPortlet {
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);

		String height = preferences.getValue("height", null);
		String cols = preferences.getValue("cols", null);
		String rows = preferences.getValue("rows", "3");
		String isSetting = preferences.getValue("isSetting", null);
		String isLs = preferences.getValue("isLs", "Y");
		String selectedDim = preferences.getValue("selectedDim", null);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		PortletVariableHelper variableHelper = new PortletVariableHelper(request,defaultVariableValues);
		
		request.setAttribute("height", height);
		request.setAttribute("cols", cols);
		request.setAttribute("rows", rows);
		request.setAttribute("isSetting", isSetting);
		request.setAttribute("isLs", isLs);
		
		selectedDim = variableHelper.getRealValue("selectedDim");
		if (selectedDim == null || selectedDim.trim().equals("")){
			selectedDim = variableHelper.getRealValue("coalId");			
		}
		request.setAttribute("selectedDim", selectedDim);
		
		request.setAttribute("portletId", request.getWindowID());
		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);

		String height = preferences.getValue("height", null);
		String cols = preferences.getValue("cols", null);
		String rows = preferences.getValue("rows", "3");
		String isSetting = preferences.getValue("isSetting", null);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		String isLs = preferences.getValue("isLs","Y");
		String cacheMinutes = preferences.getValue("cacheMinutes", defaultCacheMinutes);
		String dataURL = preferences.getValue("dataURL", null);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		
		request.setAttribute("height", height);
		request.setAttribute("cols", cols);
		request.setAttribute("rows", rows);
		request.setAttribute("isSetting", isSetting);
		request.setAttribute("isCache", isCache);
		request.setAttribute("cacheMinutes", cacheMinutes);
		request.setAttribute("isLs", isLs);
		request.setAttribute("defaultVariableValues", defaultVariableValues);
		request.setAttribute("dataURL", dataURL);
		
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String height = request.getParameter("height");
		String cols = request.getParameter("cols");
		String rows = request.getParameter("rows");
		String isCache = request.getParameter("isCache");
		String cacheMinutes = request.getParameter("cacheMinutes");
		String isLs = request.getParameter("isLs");
		String defaultVariableValues = request.getParameter("defaultVariableValues");
		String dataURL = request.getParameter("dataURL");
		
		boolean isSetting = false;
		isSetting = StringUtil.isNotNullNotEmpty(height) && StringUtil.isNotNullNotEmpty(cols);
		
		PreferencesWrapper preferWrapper = new PreferencesWrapper();
		
		preferWrapper.setValue("height", height);
		preferWrapper.setValue("cols", cols);
		preferWrapper.setValue("rows", rows);
		preferWrapper.setValue("isSetting", String.valueOf(isSetting));
		preferWrapper.setValue("isCache", isCache);
		preferWrapper.setValue("cacheMinutes", cacheMinutes);
		preferWrapper.setValue("isLs", isLs);
		preferWrapper.setValue("defaultVariableValues", defaultVariableValues);
		preferWrapper.setValue("dataURL", dataURL);
		
		
		PreferencesHelper.savePublicPreferences(request, preferWrapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
	@Resource(id="getAjaxData")
	public void getAjaxData(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		String ajaxData = "";
		try {
			PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
			String cacheMinutes = (String)preferences.getValue("cacheMinutes",defaultCacheMinutes);
			String isCache = preferences.getValue("isCache", defaultIsCache);
			String dataURL = getDataURL(request);
			ajaxData = PortletCacheManager.getOnly().getCachedData(isCache, dataURL, cacheMinutes);
		} catch (Exception e) {
			this.logger.error("获取取数据失败getAjaxData", e);
		}
		PrintWriter writer = response.getWriter();
		writer.print(ajaxData);
		writer.close();
	}
}

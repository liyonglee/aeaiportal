package com.agileai.portal.controller.bar;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.bar.ExtQueryBarConfigManage;

public class ExtQueryBarConfigManageEditHandler
        extends StandardEditHandler {
    public ExtQueryBarConfigManageEditHandler() {
        super();
        this.listHandlerClass = ExtQueryBarConfigManageListHandler.class;
        this.serviceId = buildServiceId(ExtQueryBarConfigManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("FIELD_TYPE",
                     FormSelectFactory.create("BAR_FIELD_TYPE")
                                      .addSelectedValue(getOperaAttributeValue("FIELD_TYPE",
                                                                               "TEXT")));
        setAttribute("TRIGER_CASCADE",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("TRIGER_CASCADE",
                                                                               "N")));
        setAttribute("BREAK_LINE",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("BREAK_LINE",
                                                                               "N")));
        setAttribute("PAGE_CODE",getOperaAttributeValue("PAGE_CODE",param.get("pageId")));
        setAttribute("PORTLET_CODE",getOperaAttributeValue("PORTLET_CODE",param.get("portletId")));
        
        setAttribute("MULTI_SPLITER",getOperaAttributeValue("MULTI_SPLITER",";"));
        setAttribute("DISPLAY_LENGTH",getOperaAttributeValue("DISPLAY_LENGTH","100"));
        setAttribute("SORT_NO",getOperaAttributeValue("SORT_NO","1"));
        
    }

    protected ExtQueryBarConfigManage getService() {
        return (ExtQueryBarConfigManage) this.lookupService(this.getServiceId());
    }
}

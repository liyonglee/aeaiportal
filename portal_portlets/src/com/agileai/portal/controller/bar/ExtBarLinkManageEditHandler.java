package com.agileai.portal.controller.bar;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.bar.ExtBarLinkManage;

public class ExtBarLinkManageEditHandler
        extends StandardEditHandler {
    public ExtBarLinkManageEditHandler() {
        super();
        this.listHandlerClass = ExtBarLinkManageListHandler.class;
        this.serviceId = buildServiceId(ExtBarLinkManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("NEW_PAGE",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("NEW_PAGE",
                                                                               "N")));
        this.setAttribute("PAGE_ID", this.getAttribute("PAGE_ID",param.get("pageId")));
        this.setAttribute("PORTLET_ID", this.getAttribute("PORTLET_ID",param.get("portletId")));
        this.setAttribute("SORT_NO", this.getAttribute("SORT_NO","0"));
    }
    
    protected ExtBarLinkManage getService() {
        return (ExtBarLinkManage) this.lookupService(this.getServiceId());
    }
}

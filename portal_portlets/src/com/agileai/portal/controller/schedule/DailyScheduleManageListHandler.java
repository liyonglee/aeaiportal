package com.agileai.portal.controller.schedule;

import java.util.Date;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.bizmoduler.schedule.DailyScheduleManage;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class DailyScheduleManageListHandler
        extends StandardListHandler {
    public DailyScheduleManageListHandler() {
        super();
        this.editHandlerClazz = DailyScheduleManageEditHandler.class;
        this.serviceId = buildServiceId(DailyScheduleManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    	User user = (User)this.getUser();
    	this.setAttribute("userCode", user.getUserCode());
    }

    protected void initParameters(DataParam param) {
    	User user = (User)this.getUser();
        initParamItem(param, "userCode",user.getUserCode());
        String date = param.get("date");
        Date initDate = null;
        if (StringUtil.isNullOrEmpty(date)){
        	initDate = new Date();
        }else{
        	initDate = DateUtil.getDate(date);
        }
        String sDateDefaultValue = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getBeginOfMonth(initDate));
        String eDateDefaultValue = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(initDate, DateUtil.DAY, 1));
        
        initParamItem(param, "sDate", param.get("sDate", sDateDefaultValue));
        initParamItem(param, "eDate", param.get("eDate", eDateDefaultValue));
    }

    protected DailyScheduleManage getService() {
        return (DailyScheduleManage) this.lookupService(this.getServiceId());
    }
}

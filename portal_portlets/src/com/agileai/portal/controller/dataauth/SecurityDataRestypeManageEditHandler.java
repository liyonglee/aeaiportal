package com.agileai.portal.controller.dataauth;

import com.agileai.domain.*;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.portal.bizmoduler.dataauth.SecurityDataRestypeManage;

public class SecurityDataRestypeManageEditHandler
        extends StandardEditHandler {
	
    public SecurityDataRestypeManageEditHandler() {
        super();
        this.listHandlerClass = SecurityDataRestypeManageListHandler.class;
        this.serviceId = buildServiceId(SecurityDataRestypeManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected SecurityDataRestypeManage getService() {
        return (SecurityDataRestypeManage) this.lookupService(this.getServiceId());
    }
}

package com.agileai.portal.controller.list;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.portal.bizmoduler.list.ExtTreelistTreeManage;

public class ExtTreelistParentSelectHandler
        extends TreeSelectHandler {
    public ExtTreelistParentSelectHandler() {
        super();
        this.serviceId = buildServiceId(ExtTreelistTreeManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "LIST_ID",
                                                  "LIST_NAME", "LIST_PID");

        String excludeId = param.get("LIST_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

    protected ExtTreelistTreeManage getService() {
        return (ExtTreelistTreeManage) this.lookupService(this.getServiceId());
    }
}

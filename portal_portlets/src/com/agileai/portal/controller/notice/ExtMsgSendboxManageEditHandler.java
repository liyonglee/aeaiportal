package com.agileai.portal.controller.notice;

import java.util.*;
import com.agileai.domain.*;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.*;
import com.agileai.portal.bizmoduler.notice.ExtMsgSendboxManage;
import com.agileai.portal.message.service.NoticeSendProxy;

public class ExtMsgSendboxManageEditHandler
        extends StandardEditHandler {
	
    public ExtMsgSendboxManageEditHandler() {
        super();
        this.listHandlerClass = ExtMsgSendboxManageListHandler.class;
        this.serviceId = buildServiceId(ExtMsgSendboxManage.class);
    }
    
    @PageAction
	public ViewRenderer viewDetail(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);	
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
	public ViewRenderer doBackAction(DataParam param){
		return new DispatchRenderer(getHandlerURL(listHandlerClass));
	}

    protected void processPageAttributes(DataParam param) {
        setAttribute("MSG_NOTICE_TYPE",
                FormSelectFactory.create("INFORM_WAY")
                                 .addSelectedValue(getOperaAttributeValue("MSG_NOTICE_TYPE",
                                                                          "portalIM")));
        setAttribute("userCode", param.get("userCode"));
        setAttribute("msgTheme", param.get("msgTheme"));
    }
    
	public ViewRenderer doSaveAction(DataParam param){
		
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			String receiveUsers = param.get("MSG_RECEIVEOR_ID");
			List<String> userids = new ArrayList<String>();
			String[] receiveUsersArray = receiveUsers.split(",");
			for (int i = 0; i < receiveUsersArray.length; i++) {
				userids.add(receiveUsersArray[i]);
			}
				
			//发送消息
			NoticeSendProxy port = getProxyService();
	        port.sendByIds(userids,
	        		param.get("MSG_THEME"),
	        		param.get("MSG_NOTICE_CONTENT"),
	        		param.get("userCode"));
	        
	/*	        List<String> roles = new ArrayList<String>();
		        roles.add("00000000-0000-0000-00000000000000000");
		        roles.add("002E35E3-AB9E-4F63-874E-7EE113049F17");
		        
		        port.sendByRoles(roles,
		        		param.get("MSG_THEME"),
		        		param.get("MSG_NOTICE_CONTENT"),
		        		param.get("userCode"));*/
	        
	/*	        List<String> groups = new ArrayList<String>();
		        groups.add("00000000-0000-0000-00000000000000000");
		        groups.add("16057F2A-A610-4006-AA19-CF6B40488535");
		        
		        port.sendByGroups(groups,
		        		param.get("MSG_THEME"),
		        		param.get("MSG_NOTICE_CONTENT"),
		        		param.get("userCode"));*/
			
			//getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			//getService().updateRecord(param);	
		}
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}

    protected ExtMsgSendboxManage getService() {
        return (ExtMsgSendboxManage) this.lookupService(this.getServiceId());
    }
    
    protected NoticeSendProxy getProxyService() {
        return (NoticeSendProxy) this.lookupService("noticeSendProxy");
    }
}

package com.agileai.portal.controller.notice;

import com.agileai.domain.*;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.portal.bizmoduler.notice.ExtMsgSendboxManage;

public class ExtMsgSendboxManageListHandler
        extends StandardListHandler {
    public ExtMsgSendboxManageListHandler() {
        super();
        this.editHandlerClazz = ExtMsgSendboxManageEditHandler.class;
        this.serviceId = buildServiceId(ExtMsgSendboxManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "msgTheme", "");
    }

    protected ExtMsgSendboxManage getService() {
        return (ExtMsgSendboxManage) this.lookupService(this.getServiceId());
    }
}

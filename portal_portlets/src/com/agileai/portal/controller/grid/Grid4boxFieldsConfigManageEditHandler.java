package com.agileai.portal.controller.grid;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.grid.Grid4boxFieldsConfigManage;
import com.agileai.util.ListUtil;

public class Grid4boxFieldsConfigManageEditHandler
        extends StandardEditHandler {
    public Grid4boxFieldsConfigManageEditHandler() {
        super();
        this.listHandlerClass = Grid4boxFieldsConfigManageListHandler.class;
        this.serviceId = buildServiceId(Grid4boxFieldsConfigManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("FIELD_TYPE",
                     FormSelectFactory.create("COLUMN_TYPE")
                                      .addSelectedValue(getOperaAttributeValue("FIELD_TYPE",
                                                                               "col_data")));
        setAttribute("ALIGN_TYPE",
                     FormSelectFactory.create("ALIGN_TYPE")
                                      .addSelectedValue(getOperaAttributeValue("ALIGN_TYPE",
                                                                               "left")));
        setAttribute("HD_ALIGN_TYPE",
                     FormSelectFactory.create("ALIGN_TYPE")
                                      .addSelectedValue(getOperaAttributeValue("HD_ALIGN_TYPE",
                                                                               "left")));
        setAttribute("CAN_MOVE",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("CAN_MOVE",
                                                                               "Y")));
        setAttribute("CAN_SORT",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("CAN_SORT",
                                                                               "Y")));
        setAttribute("IS_HIDDEN",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("IS_HIDDEN",
                                                                               "N")));
        setAttribute("IS_FROZE",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("IS_FROZE",
                                                                               "N")));
        setAttribute("IS_GROUP",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("IS_GROUP",
                                                                               "N")));
        setAttribute("RENDER_TYPE",
                     FormSelectFactory.create("FIELD_RENDERER_TYPE")
                                      .addSelectedValue(getOperaAttributeValue("RENDER_TYPE",
                                                                               "")));
        setAttribute(param, "gridId");
    }
	public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			int maxSortNO = getService().getMaxSortNO(param);
			param.put("SORT_NO",maxSortNO);
			getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updateRecord(param);	
		}
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	public ViewRenderer doCheckUniqueAction(DataParam param){
		String responseText = "";
		List<DataRow> records = getService().findRecords(param);
		if (!ListUtil.isNullOrEmpty(records)){
			responseText = defDuplicateMsg;
		}
		return new AjaxRenderer(responseText);
	}	
    protected Grid4boxFieldsConfigManage getService() {
        return (Grid4boxFieldsConfigManage) this.lookupService(this.getServiceId());
    }
}

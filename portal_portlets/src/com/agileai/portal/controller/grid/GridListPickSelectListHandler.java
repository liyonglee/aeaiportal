package com.agileai.portal.controller.grid;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.grid.GridListPickSelect;
import com.agileai.util.StringUtil;

public class GridListPickSelectListHandler
        extends PickFillModelHandler {
    public GridListPickSelectListHandler() {
        super();
        this.serviceId = buildServiceId(GridListPickSelect.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
    }
    @PageAction
    public ViewRenderer copyGridListRecords(DataParam param){
    	ViewRenderer result = null;
    	String selectPageId = param.get("targetIdValue");
    	String selectPortletId = param.get("targetNameValue");
    	String currentPageId = param.get("currentPageId");
    	String currentPortletId = param.get("currentPortletId");
    	String currentGridId = param.get("currentGridId");
    	
    	GridListPickSelect gridListPickSelect = getService();
    	if (StringUtil.isNotNullNotEmpty(currentGridId)){
    		gridListPickSelect.copyGridListRecords(selectPageId, selectPortletId, currentGridId);
        	result = new RedirectRenderer(getHandlerURL(Grid4boxFieldsConfigManageListHandler.class)
        			+"&gridId="+currentGridId);
    	}else{
    		gridListPickSelect.copyGridListRecords(selectPageId, selectPortletId, currentPageId, currentPortletId);
        	result = new RedirectRenderer(getHandlerURL(GridFieldsConfigManageListHandler.class)
        			+"&pageCode="+currentPageId+"&portletCode="+currentPortletId);    		
    	}
    	return result;
    }
    protected GridListPickSelect getService() {
        return (GridListPickSelect) this.lookupService(this.getServiceId());
    }
}

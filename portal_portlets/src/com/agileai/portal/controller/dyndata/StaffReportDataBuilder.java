package com.agileai.portal.controller.dyndata;

import java.util.Date;

import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.util.DateUtil;

public class StaffReportDataBuilder {
	private DataSet data = new DataSet();

	public StaffReportDataBuilder(){
		String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL).substring(0,7);
		String lastDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(new Date(), DateUtil.MONTH, -1)).substring(0,7);
		String preDate =DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(new Date(), DateUtil.MONTH, -2)).substring(0,7);
				
		DataRow row1 = new DataRow("DATE",preDate,"MANAGE","projectmanager",
			    "BUS_MARKET","154","BUS_FINANCE","63","BUS_RESEARCH","220","BUS_LOGISTIC","136",
			    "SUB_MARKET","113","SUB_FINANCE","84","SUB_RESEARCH","73","SUB_LOGISTIC","77");
		DataRow row2 = new DataRow("DATE",preDate,"MANAGE","seniormanager",
			    "BUS_MARKET","241","BUS_FINANCE","133","BUS_RESEARCH","128","BUS_LOGISTIC","231",
			    "SUB_MARKET","83","SUB_FINANCE","53","SUB_RESEARCH","118","SUB_LOGISTIC","58");
		DataRow row3 = new DataRow("DATE",preDate,"MANAGE","consultant",
			    "BUS_MARKET","74","BUS_FINANCE","160","BUS_RESEARCH","189","BUS_LOGISTIC","214",
			    "SUB_MARKET","23","SUB_FINANCE","20","SUB_RESEARCH","37","SUB_LOGISTIC","29");
		DataRow row4 = new DataRow("DATE",lastDate,"MANAGE","projectmanager",
			    "BUS_MARKET","146","BUS_FINANCE","208","BUS_RESEARCH","344","BUS_LOGISTIC","239",
			    "SUB_MARKET","83","SUB_FINANCE","119","SUB_RESEARCH","91","SUB_LOGISTIC","29");
		DataRow row5 = new DataRow("DATE",lastDate,"MANAGE","seniormanager",
			    "BUS_MARKET","57","BUS_FINANCE","231","BUS_RESEARCH","345","BUS_LOGISTIC","220",
			    "SUB_MARKET","48","SUB_FINANCE","77","SUB_RESEARCH","25","SUB_LOGISTIC","101");
		DataRow row6 = new DataRow("DATE",lastDate,"MANAGE","consultant",
			    "BUS_MARKET","246","BUS_FINANCE","89","BUS_RESEARCH","84","BUS_LOGISTIC","102",
			    "SUB_MARKET","102","SUB_FINANCE","62","SUB_RESEARCH","61","SUB_LOGISTIC","22");
		DataRow row7 = new DataRow("DATE",currentDate,"MANAGE","projectmanager",
			    "BUS_MARKET","136","BUS_FINANCE","167","BUS_RESEARCH","167","BUS_LOGISTIC","125",
			    "SUB_MARKET","106","SUB_FINANCE","116","SUB_RESEARCH","109","SUB_LOGISTIC","41");
		DataRow row8 = new DataRow("DATE",currentDate,"MANAGE","seniormanager",
			    "BUS_MARKET","199","BUS_FINANCE","144","BUS_RESEARCH","233","BUS_LOGISTIC","176",
			    "SUB_MARKET","76","SUB_FINANCE","106","SUB_RESEARCH","78","SUB_LOGISTIC","22");
		DataRow row9 = new DataRow("DATE",currentDate,"MANAGE","consultant",
			    "BUS_MARKET","223","BUS_FINANCE","132","BUS_RESEARCH","255","BUS_LOGISTIC","196",
			    "SUB_MARKET","50","SUB_FINANCE","48","SUB_RESEARCH","97","SUB_LOGISTIC","55");
		
		data.addDataRow(row1);
		data.addDataRow(row2);
		data.addDataRow(row3);
		data.addDataRow(row4);
		data.addDataRow(row5);
		data.addDataRow(row6);
		data.addDataRow(row7);
		data.addDataRow(row8);
		data.addDataRow(row9);
	}
	
	public DataRow getXmlData(String date, String level){
		DataRow xmlRow = new DataRow("BUS_MARKET","","BUS_FINANCE","","BUS_RESEARCH","","BUS_LOGISTIC","","SUB_MARKET","","SUB_FINANCE","","SUB_RESEARCH","","SUB_LOGISTIC","");
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			if (date.equals(row.get("DATE"))&&level.equals(row.get("MANAGE"))) {
				xmlRow = row;
			}
		}
		return xmlRow;
	}
	
	public DataSet getGridJsonData(String date, String level){
		DataSet gridSet = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			if (date.equals(row.get("DATE"))&&level.equals(row.get("MANAGE"))) {
				if(level.equals("projectmanager")){
					level = "项目经理";
				}
				if(level.equals("seniormanager")){
					level = "高级经理";
				}
				if(level.equals("consultant")){
					level = "顾问";
				}
				DataRow row1 = new DataRow(),row2 = new DataRow(),row3 = new DataRow();
				row1.put("ROWID","1","DATE",date,"LEVEL",level,"CLASSIFY","总部","MARKET",row.get("BUS_MARKET"),"FINANCE",row.get("BUS_FINANCE"),"RESEARCH",row.get("BUS_RESEARCH"),"LOGISTIC",row.get("BUS_LOGISTIC"));
				row2.put("ROWID","2","DATE",date,"LEVEL",level,"CLASSIFY","分支机构","MARKET",row.get("SUB_MARKET"),"FINANCE",row.get("SUB_FINANCE"),"RESEARCH",row.get("SUB_RESEARCH"),"LOGISTIC",row.get("SUB_LOGISTIC"));
				String totalMarket = String.valueOf(Integer.parseInt(row.getString("BUS_MARKET"))+Integer.parseInt(row.getString("SUB_MARKET")));
				String totalFinance = String.valueOf(Integer.parseInt(row.getString("BUS_FINANCE"))+Integer.parseInt(row.getString("SUB_FINANCE")));
				String totalDevelope = String.valueOf(Integer.parseInt(row.getString("BUS_RESEARCH"))+Integer.parseInt(row.getString("SUB_RESEARCH")));
				String totalLogistic = String.valueOf(Integer.parseInt(row.getString("BUS_LOGISTIC"))+Integer.parseInt(row.getString("SUB_LOGISTIC")));
				row3.put("ROWID","3","DATE",date,"LEVEL",level,"CLASSIFY","合计","MARKET",totalMarket,"FINANCE",totalFinance,"RESEARCH",totalDevelope,"LOGISTIC",totalLogistic);
				gridSet.addDataRow(row1);
				gridSet.addDataRow(row2);
				gridSet.addDataRow(row3);
			}		
		}
		return gridSet;
	}
	
	public DataSet getBoxJsonData(String level,String classify){
		DataSet boxSet = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			if (level.equals(row.get("MANAGE"))) {
				DataRow boxRow = new DataRow();
				if(classify.equals("head")){
					boxRow.put("DATE",row.get("DATE"),"MARKET", row.get("BUS_MARKET"),"FINANCE",row.get("BUS_FINANCE"),"RESEARCH",row.get("BUS_RESEARCH"),"LOGISTIC",row.get("BUS_LOGISTIC"));
				}
				else if(classify.equals("branch")){
					boxRow.put("DATE",row.get("DATE"),"MARKET",row.get("SUB_MARKET"),"FINANCE",row.get("SUB_FINANCE"),"RESEARCH",row.get("SUB_RESEARCH"),"LOGISTIC",row.get("SUB_LOGISTIC"));
				}
				boxSet.addDataRow(boxRow);		
			}		
		}
		return boxSet;
	}
}

package com.agileai.portal.controller.dyndata;

import java.text.DecimalFormat;
import java.util.HashMap;

import com.agileai.domain.DataMap;
import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.util.DateUtil;

public class PartnersPerformanceDataBuilder {
	private DataSet data = new DataSet();
	public PartnersPerformanceDataBuilder(){
		String currentDate = DateUtil.getYear();
		DataRow row1 = new DataRow("ID","1","TOTAL_DATE",currentDate,"PER_PARTNER","softdevelopers",
				"ORG","市场部","CH_TASK","30","CH_REALITY","28","OPEN_TASK","40","OPEN_REALITY","35");
		DataRow row2 = new DataRow("ID","2","TOTAL_DATE",currentDate,"PER_PARTNER","softdevelopers",
				"ORG","研发部","CH_TASK","30","CH_REALITY","28","OPEN_TASK","40","OPEN_REALITY","35");
		DataRow row3 = new DataRow("ID","3","TOTAL_DATE",currentDate,"PER_PARTNER","softsupplier",
				"ORG","研发部","CH_TASK","37","CH_REALITY","35","OPEN_TASK","50","OPEN_REALITY","45");
		DataRow row4 = new DataRow("ID","3","TOTAL_DATE",currentDate,"PER_PARTNER","softsupplier",
				"ORG","市场部","CH_TASK","35","CH_REALITY","34","OPEN_TASK","45","OPEN_REALITY","20");
		DataRow row5 = new DataRow("ID","5","TOTAL_DATE",currentDate,"PER_PARTNER","softagent",
				"ORG","研发部","CH_TASK","56","CH_REALITY","30","OPEN_TASK","60","OPEN_REALITY","55");
		DataRow row6 = new DataRow("ID","6","TOTAL_DATE",currentDate,"PER_PARTNER","softagent",
				"ORG","市场部","CH_TASK","51","CH_REALITY","34","OPEN_TASK","48","OPEN_REALITY","46");
		
		data.addDataRow(row1);
		data.addDataRow(row2);
		data.addDataRow(row3);
		data.addDataRow(row4);
		data.addDataRow(row5);
		data.addDataRow(row6);
	}
	public DataRow getXmlData(String date, String partner){
		DataRow xmlRow = new DataRow("CH_TASK","0","CH_REALITY","0","OPEN_TASK","0","OPEN_REALITY","0");
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);			
			if (date.equals(row.get("TOTAL_DATE"))&&partner.equals(row.get("PER_PARTNER"))) {
				int chtask = Integer.parseInt(xmlRow.getString("CH_TASK"))+Integer.parseInt(row.getString("CH_TASK"));
				int chreality = Integer.parseInt(xmlRow.getString("CH_REALITY"))+Integer.parseInt(row.getString("CH_REALITY"));
				int opentask = Integer.parseInt(xmlRow.getString("OPEN_TASK"))+Integer.parseInt(row.getString("OPEN_TASK"));
				int openreality = Integer.parseInt(xmlRow.getString("OPEN_REALITY"))+Integer.parseInt(row.getString("OPEN_REALITY"));
				double chRatio = (double)chreality/chtask*100;
				DecimalFormat s = new DecimalFormat("#.00");
				double openRatio = (double)openreality/opentask*100;
				xmlRow.put("CH_TASK",String.valueOf(chtask),"CH_REALITY",String.valueOf(chreality),"CH_RATIO",s.format(chRatio),"OPEN_TASK",String.valueOf(opentask),"OPEN_REALITY",String.valueOf(openreality),"OPEN_RATIO",s.format(openRatio));
			}
		}
		return xmlRow;
	}
	public DataSet getGridJsonData(String date, String partner){
		DataSet gridSet = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			if (date.equals(row.get("TOTAL_DATE"))&&partner.equals(row.get("PER_PARTNER"))) {
				if(row.get("PER_PARTNER").equals("softdevelopers")){
					row.put("PER_PARTNER", "开发商");
				}else if(row.get("PER_PARTNER").equals("softsupplier")){
					row.put("PER_PARTNER", "供应商");
				}else if(row.get("PER_PARTNER").equals("softagent")){
					row.put("PER_PARTNER", "代理商");
				}
				gridSet.addDataRow(row);
			}
		}
		return gridSet;
	}
	public DataRow getHtmlJsonData(String id){
		DataRow htmlRow = new DataRow();
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			if (id.equals(row.get("ID"))) {
				if(row.get("PER_PARTNER").equals("softdevelopers")){
					row.put("PER_PARTNER", "开发商");
				}else if(row.get("PER_PARTNER").equals("softsupplier")){
					row.put("PER_PARTNER", "供应商");
				}else if(row.get("PER_PARTNER").equals("softagent")){
					row.put("PER_PARTNER", "代理商");
				}
				htmlRow = row;
			}		
		}
		return htmlRow;
	}
	public DataSet getBoxJsonData(String partner,String classify){
		DataSet boxSet = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			if (partner.equals(row.get("PER_PARTNER"))) {
				if(row.get("PER_PARTNER").equals("softdevelopers")){
					row.put("PER_PARTNER", "开发商");
				}else if(row.get("PER_PARTNER").equals("softsupplier")){
					row.put("PER_PARTNER", "供应商");
				}else if(row.get("PER_PARTNER").equals("softagent")){
					row.put("PER_PARTNER", "代理商");
				}
				DataRow boxRow = new DataRow();
				if(classify.equals("develop")){
					boxRow.put("PER_PARTNER",row.get("PER_PARTNER"),"ORG",row.get("ORG"),"TOTAL_DATE",row.get("TOTAL_DATE"),"TASK",row.get("CH_TASK"),"REALITY",row.get("CH_REALITY"));
				}
				else if(classify.equals("charge")){
					boxRow.put("PER_PARTNER",row.get("PER_PARTNER"),"ORG",row.get("ORG"),"TOTAL_DATE",row.get("TOTAL_DATE"),"TASK", row.get("OPEN_TASK"),"REALITY",row.get("OPEN_REALITY"));
				}
				boxSet.addDataRow(boxRow);
			}
		}
		return boxSet;
	}
}

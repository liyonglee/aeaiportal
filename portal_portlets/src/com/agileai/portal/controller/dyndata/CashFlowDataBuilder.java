package com.agileai.portal.controller.dyndata;

import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;

public class CashFlowDataBuilder {
	private DataSet data = new DataSet();

	public CashFlowDataBuilder(){
		
		DataRow row1 = new DataRow("ID","0","MONTH","01","ERP_IN","14","ERP_OUT","12","DOCK_IN","13","DOCK_OUT","11","WEB_IN","9","WEB_OUT","7","DESCRIPTION","这是一月份的现金流情况");
		DataRow row2 = new DataRow("ID","1","MONTH","02","ERP_IN","20","ERP_OUT","16","DOCK_IN","17","DOCK_OUT","14","WEB_IN","13","WEB_OUT","10","DESCRIPTION","这是二月份的现金流情况");
		DataRow row3 = new DataRow("ID","2","MONTH","03","ERP_IN","16","ERP_OUT","14","DOCK_IN","15","DOCK_OUT","13","WEB_IN","9","WEB_OUT","8","DESCRIPTION","这是三月份的现金流情况");
		DataRow row4 = new DataRow("ID","3","MONTH","04","ERP_IN","17","ERP_OUT","13","DOCK_IN","14","DOCK_OUT","12","WEB_IN","7","WEB_OUT","6","DESCRIPTION","这是四月份的现金流情况");
		DataRow row5 = new DataRow("ID","4","MONTH","05","ERP_IN","19","ERP_OUT","16","DOCK_IN","16","DOCK_OUT","15","WEB_IN","8","WEB_OUT","6","DESCRIPTION","这是五月份的现金流情况");
		DataRow row6 = new DataRow("ID","5","MONTH","06","ERP_IN","15","ERP_OUT","13","DOCK_IN","15","DOCK_OUT","14","WEB_IN","10","WEB_OUT","8","DESCRIPTION","这是六月份的现金流情况");
		DataRow row7 = new DataRow("ID","6","MONTH","07","ERP_IN","21","ERP_OUT","17","DOCK_IN","12","DOCK_OUT","10","WEB_IN","7","WEB_OUT","6","DESCRIPTION","这是七月份的现金流情况");
		DataRow row8 = new DataRow("ID","7","MONTH","08","ERP_IN","18","ERP_OUT","15","DOCK_IN","16","DOCK_OUT","14","WEB_IN","6","WEB_OUT","5","DESCRIPTION","这是八月份的现金流情况");
		DataRow row9 = new DataRow("ID","8","MONTH","09","ERP_IN","16","ERP_OUT","14","DOCK_IN","15","DOCK_OUT","13","WEB_IN","8","WEB_OUT","7","DESCRIPTION","这是九月份的现金流情况");
		DataRow row10 = new DataRow("ID","9","MONTH","10","ERP_IN","19","ERP_OUT","16","DOCK_IN","13","DOCK_OUT","10","WEB_IN","9","WEB_OUT","7","DESCRIPTION","这是十月份的现金流情况");
		DataRow row11 = new DataRow("ID","10","MONTH","11","ERP_IN","20","ERP_OUT","17","DOCK_IN","14","DOCK_OUT","11","WEB_IN","7","WEB_OUT","6","DESCRIPTION","这是十一月份的现金流情况");
		DataRow row12 = new DataRow("ID","11","MONTH","12","ERP_IN","22","ERP_OUT","18","DOCK_IN","15","DOCK_OUT","12","WEB_IN","11","WEB_OUT","9","DESCRIPTION","这是十二月份的现金流情况");
		
		data.addDataRow(row1);
		data.addDataRow(row2);
		data.addDataRow(row3);
		data.addDataRow(row4);
		data.addDataRow(row5);
		data.addDataRow(row6);
		data.addDataRow(row7);
		data.addDataRow(row8);
		data.addDataRow(row9);
		data.addDataRow(row10);
		data.addDataRow(row11);
		data.addDataRow(row12);
	}
	
	public DataRow getXmlData(String quarter, String project,String chartType){
		DataRow xmlRow = new DataRow();
		if (quarter.equals("1")) {
			if(project.equals("erpdev")){
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(0).getString("ERP_IN"),"SECOND",data.getDataRow(1).getString("ERP_IN"),"THIRD",data.getDataRow(2).getString("ERP_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(0).getString("ERP_OUT"),"SECOND",data.getDataRow(1).getString("ERP_OUT"),"THIRD",data.getDataRow(2).getString("ERP_OUT"));
				}
			}else if(project.equals("docking")){
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(0).getString("DOCK_IN"),"SECOND",data.getDataRow(1).getString("DOCK_IN"),"THIRD",data.getDataRow(2).getString("DOCK_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(0).getString("DOCK_OUT"),"SECOND",data.getDataRow(1).getString("DOCK_OUT"),"THIRD",data.getDataRow(2).getString("DOCK_OUT"));
				}
			}else{
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(0).getString("WEB_IN"),"SECOND",data.getDataRow(1).getString("WEB_IN"),"THIRD",data.getDataRow(2).getString("WEB_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(0).getString("WEB_OUT"),"SECOND",data.getDataRow(1).getString("WEB_OUT"),"THIRD",data.getDataRow(2).getString("WEB_OUT"));
				}
			}
		}else if(quarter.equals("2")){
			if(project.equals("erpdev")){
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(3).getString("ERP_IN"),"SECOND",data.getDataRow(4).getString("ERP_IN"),"THIRD",data.getDataRow(5).getString("ERP_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(3).getString("ERP_OUT"),"SECOND",data.getDataRow(4).getString("ERP_OUT"),"THIRD",data.getDataRow(5).getString("ERP_OUT"));
				}
			}else if(project.equals("docking")){
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(3).getString("DOCK_IN"),"SECOND",data.getDataRow(4).getString("DOCK_IN"),"THIRD",data.getDataRow(5).getString("DOCK_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(3).getString("DOCk_OUT"),"SECOND",data.getDataRow(4).getString("DOCK_OUT"),"THIRD",data.getDataRow(5).getString("DOCK_OUT"));
				}
			}else{
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(3).getString("WEB_IN"),"SECOND",data.getDataRow(4).getString("WEB_IN"),"THIRD",data.getDataRow(5).getString("WEB_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(3).getString("WEB_OUT"),"SECOND",data.getDataRow(4).getString("WEB_OUT"),"THIRD",data.getDataRow(5).getString("WEB_OUT"));
				}
			}
		}else if(quarter.equals("3")){
			if(project.equals("erpdev")){
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(6).getString("ERP_IN"),"SECOND",data.getDataRow(7).getString("ERP_IN"),"THIRD",data.getDataRow(8).getString("ERP_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(6).getString("ERP_OUT"),"SECOND",data.getDataRow(7).getString("ERP_OUT"),"THIRD",data.getDataRow(8).getString("ERP_OUT"));
				}
			}else if(project.equals("docking")){
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(6).getString("DOCK_IN"),"SECOND",data.getDataRow(7).getString("DOCK_IN"),"THIRD",data.getDataRow(8).getString("DOCK_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(6).getString("DOCk_OUT"),"SECOND",data.getDataRow(7).getString("DOCK_OUT"),"THIRD",data.getDataRow(8).getString("DOCK_OUT"));
				}
			}else{
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(6).getString("WEB_IN"),"SECOND",data.getDataRow(7).getString("WEB_IN"),"THIRD",data.getDataRow(8).getString("WEB_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(6).getString("WEB_OUT"),"SECOND",data.getDataRow(7).getString("WEB_OUT"),"THIRD",data.getDataRow(8).getString("WEB_OUT"));
				}
			}
		}else{
			if(project.equals("erpdev")){
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(9).getString("ERP_IN"),"SECOND",data.getDataRow(10).getString("ERP_IN"),"THIRD",data.getDataRow(11).getString("ERP_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(9).getString("ERP_OUT"),"SECOND",data.getDataRow(10).getString("ERP_OUT"),"THIRD",data.getDataRow(11).getString("ERP_OUT"));
				}
			}else if(project.equals("docking")){
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(9).getString("DOCK_IN"),"SECOND",data.getDataRow(10).getString("DOCK_IN"),"THIRD",data.getDataRow(11).getString("DOCK_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(9).getString("DOCk_OUT"),"SECOND",data.getDataRow(10).getString("DOCK_OUT"),"THIRD",data.getDataRow(11).getString("DOCK_OUT"));
				}
			}else{
				if(chartType.equals("line")){
					xmlRow.put("FIRST",data.getDataRow(9).getString("WEB_IN"),"SECOND",data.getDataRow(10).getString("WEB_IN"),"THIRD",data.getDataRow(11).getString("WEB_IN"));
				}else{
					xmlRow.put("FIRST",data.getDataRow(9).getString("WEB_OUT"),"SECOND",data.getDataRow(10).getString("WEB_OUT"),"THIRD",data.getDataRow(11).getString("WEB_OUT"));
				}
			}
		}
		return xmlRow;
	}
	
	public DataSet getGridJsonData(String quarter, String project ){
		DataSet gridSet = new DataSet();
		if(quarter.equals("1")){
			for(int i=0;i<3;i++){
				DataRow row = new DataRow();
				row.put("ID",data.getDataRow(i).get("ID"),"MONTH",data.getDataRow(i).get("MONTH"));
				if(project.equals("erpdev")){
					row.put("PROJECT","ERP系统开发","INFLOW",data.getDataRow(i).get("ERP_IN"),"OUTFLOW",data.getDataRow(i).get("ERP_OUT"));
				}else if(project.equals("docking")){
					row.put("PROJECT","系统对接","INFLOW",data.getDataRow(i).get("DOCK_IN"),"OUTFLOW",data.getDataRow(i).get("DOCK_OUT"));
				}else{
					row.put("PROJECT","网站设计","INFLOW",data.getDataRow(i).get("WEB_IN"),"OUTFLOW",data.getDataRow(i).get("WEB_OUT"));
				}
				gridSet.addDataRow(row);
			}
		}else if(quarter.equals("2")){
			for(int i=3;i<6;i++){
				DataRow row = new DataRow();
				row.put("ID",data.getDataRow(i).get("ID"),"MONTH",data.getDataRow(i).get("MONTH"));
				if(project.equals("erpdev")){
					row.put("PROJECT","ERP系统开发","INFLOW",data.getDataRow(i).get("ERP_IN"),"OUTFLOW",data.getDataRow(i).get("ERP_OUT"));
				}else if(project.equals("docking")){
					row.put("PROJECT","系统对接","INFLOW",data.getDataRow(i).get("DOCK_IN"),"OUTFLOW",data.getDataRow(i).get("DOCK_OUT"));
				}else{
					row.put("PROJECT","网站设计","INFLOW",data.getDataRow(i).get("WEB_IN"),"OUTFLOW",data.getDataRow(i).get("WEB_OUT"));
				}
				gridSet.addDataRow(row);
			}
		}else if(quarter.equals("3")){
			for(int i=6;i<9;i++){
				DataRow row = new DataRow();
				row.put("ID",data.getDataRow(i).get("ID"),"MONTH",data.getDataRow(i).get("MONTH"));
				if(project.equals("erpdev")){
					row.put("PROJECT","ERP系统开发","INFLOW",data.getDataRow(i).get("ERP_IN"),"OUTFLOW",data.getDataRow(i).get("ERP_OUT"));
				}else if(project.equals("docking")){
					row.put("PROJECT","系统对接","INFLOW",data.getDataRow(i).get("DOCK_IN"),"OUTFLOW",data.getDataRow(i).get("DOCK_OUT"));
				}else{
					row.put("PROJECT","网站设计","INFLOW",data.getDataRow(i).get("WEB_IN"),"OUTFLOW",data.getDataRow(i).get("WEB_OUT"));
				}
				gridSet.addDataRow(row);
			}
		}else{
			for(int i=9;i<12;i++){
				DataRow row = new DataRow();
				row.put("ID",data.getDataRow(i).get("ID"),"MONTH",data.getDataRow(i).get("MONTH"));
				if(project.equals("erpdev")){
					row.put("PROJECT","ERP系统开发","INFLOW",data.getDataRow(i).get("ERP_IN"),"OUTFLOW",data.getDataRow(i).get("ERP_OUT"));
				}else if(project.equals("docking")){
					row.put("PROJECT","系统对接","INFLOW",data.getDataRow(i).get("DOCK_IN"),"OUTFLOW",data.getDataRow(i).get("DOCK_OUT"));
				}else{
					row.put("PROJECT","网站设计","INFLOW",data.getDataRow(i).get("WEB_IN"),"OUTFLOW",data.getDataRow(i).get("WEB_OUT"));
				}
				gridSet.addDataRow(row);
			}
		}
		return gridSet;
	}
	
	public DataSet getBoxJsonData(String quarter, String project,String chartType){
		DataSet boxSet = new DataSet();
		DataSet gridSet = this.getGridJsonData(quarter, project);
		for(int i=0;i<3;i++){
			DataRow row = new DataRow();
			row.put("ID",gridSet.getDataRow(i).get("ID"),"MONTH",gridSet.getDataRow(i).get("MONTH"),"PROJECT",gridSet.getDataRow(i).get("PROJECT"));
			if(chartType.equals("line")){
				row.put("FLOW",gridSet.getDataRow(i).get("INFLOW"));
			}else{
				row.put("FLOW",gridSet.getDataRow(i).get("OUTFLOW"));
			}
			boxSet.addDataRow(row);
		}
		return boxSet;
	}
	
	public DataRow getHtmlJsonData(String id,String project){
		DataRow htmlRow = new DataRow();
		DataRow row = data.getDataRow(Integer.parseInt(id));
		try{
			htmlRow.put("ID",row.get("ID"),"MONTH",row.get("MONTH"),"PROJECT",project,"DESCRIPTION",row.get("DESCRIPTION"));
			if(project.equals("ERP系统开发")){
				htmlRow.put("INFLOW",row.get("ERP_IN"),"OUTFLOW",row.get("ERP_OUT"));
			}else if(project.equals("系统对接")){
				htmlRow.put("INFLOW",row.get("DOCK_IN"),"OUTFLOW",row.get("DOCK_OUT"));
			}else{
				htmlRow.put("INFLOW",row.get("WEB_IN"),"OUTFLOW",row.get("WEB_OUT"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlRow;
	}
}

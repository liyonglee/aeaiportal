package com.agileai.portal.controller.dyndata;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.XmlUtil;

public class StaffReportDataProviderHandler extends SimpleHandler{	

	public StaffReportDataProviderHandler(){       
		super();
	} 
	
	public ViewRenderer prepareDisplay(DataParam param) {
		String resourceType = param.get("resourceType");//Histogram、PieChart、DataGrid、BoxData、HtmlData
		StaffReportDataBuilder builder = new StaffReportDataBuilder();
		String datas = null;
		if (resourceType.equals("Histogram")){			
			String date = param.get("year")+"-"+param.get("month");
			String level = param.get("level");
			DataRow xmlRow = builder.getXmlData(date, level);
			datas = this.buildHistogramXML(xmlRow,level);
		}			
		else if (resourceType.equals("BarChart")){
			String date = param.get("year")+"-"+param.get("month");
			String level = param.get("level");
			DataRow xmlRow = builder.getXmlData(date, level);
			datas = this.buildBarChartXML(xmlRow,level);
		}
		else if (resourceType.equals("DataGrid")){
			String date = param.get("year")+"-"+param.get("month");
			String level = param.get("level");
			DataSet gridSet = builder.getGridJsonData(date, level);
			datas = this.buildDataGridJson(gridSet);
		}
		else if (resourceType.equals("BoxData")){
			String level = param.get("level");
			String classify = param.get("classify");
			DataSet boxSet = builder.getBoxJsonData(level,classify);
			datas = this.buildBoxJson(boxSet);
		}
		else if (resourceType.equals("HtmlData")){
			String date = param.get("date");
			String level = param.get("level");
			if(level.equals("项目经理")){
				level = "projectmanager";
			}
			if(level.equals("高级经理")){
				level = "seniormanager";
			}
			if(level.equals("顾问")){
				level = "consultant";
			}
			String rowId = param.get("rowId");
			DataSet gridSet = builder.getGridJsonData(date, level);
			datas = this.buildHtmlJson(gridSet,rowId);
		}
		this.setAttribute("datas",datas);
		return new LocalRenderer(getPage());
	}
	
	private String buildDataGridJson(DataSet set) {
		String result = null;
		String fields[] = {"ROWID","DATE","LEVEL","CLASSIFY","MARKET","FINANCE","RESEARCH","LOGISTIC"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<set.size();i++){
				DataRow row = set.getDataRow(i);
				JSONObject object = new JSONObject();
				for(int j=0;j<row.size();j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}	
				jsonArray.put(object);
			}
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String buildBoxJson(DataSet set) {
		String result = null;
		String fields[] = {"DATE","MARKET","FINANCE","RESEARCH","LOGISTIC"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<set.size();i++){
				JSONObject object = new JSONObject();
				DataRow row = set.getDataRow(i);
				for(int j=0;j<row.size();j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}
				jsonArray.put(object);
			}									
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String buildHtmlJson(DataSet set,String rowId){
		String result = null;
		String fields[] = {"ROWID","CLASSIFY","MARKET","FINANCE","RESEARCH","LOGISTIC"};
		try {
			JSONObject object = new JSONObject();
			DataRow row = set.getDataRow(Integer.parseInt(rowId)-1);
			for(int i=0;i<row.size()-2;i++){
				object.put(fields[i], row.stringValue(fields[i]));
			}
			result = object.toString();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private String buildBarChartXML(DataRow row,String level) {
		String count1 = row.stringValue("SUB_MARKET");			
		String count2 = row.stringValue("SUB_FINANCE");			
		String count3 = row.stringValue("SUB_RESEARCH");			
		String count4 = row.stringValue("SUB_LOGISTIC");
		String result = "";		
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart");
		charElement.addAttribute("formatNumberScale","0");
		charElement.addAttribute("caption","分支机构月报总数");
		charElement.addAttribute("clickURL", "javascript:doPopupWebPage('targetPage:03/PopupDataGridReport.ptml,level:"+level+",classify:branch',{width:'900',height:'350'})");

		Element setElement1 = charElement.addElement("set");
		setElement1.addAttribute("value", count1);
		setElement1.addAttribute("label", "市场部");
		
		Element setElement2 = charElement.addElement("set");
		setElement2.addAttribute("value", count2);
		setElement2.addAttribute("label", "财务部");
		
		Element setElement3 = charElement.addElement("set");
		setElement3.addAttribute("value", count3);
		setElement3.addAttribute("label", "研发部");
		
		Element setElement4 = charElement.addElement("set");
		setElement4.addAttribute("value", count4);
		setElement4.addAttribute("label", "后勤部");
		
		result = document.asXML();
		return result;
	}

	private String buildHistogramXML(DataRow row,String level){
		String count1 = row.stringValue("BUS_MARKET");
		String count2 = row.stringValue("BUS_FINANCE");
		String count3 = row.stringValue("BUS_RESEARCH");
		String count4 = row.stringValue("BUS_LOGISTIC");
		String result = "";
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart");
		charElement.addAttribute("formatNumberScale","0");
		charElement.addAttribute("caption","总部月报总数");
		charElement.addAttribute("yAxisName","月报总数");
		charElement.addAttribute("xAxisName","部门名称");
		charElement.addAttribute("showNames","1");
		charElement.addAttribute("clickURL", "javascript:doPopupWebPage('targetPage:03/PopupDataGridReport.ptml,level:"+level+",classify:head',{width:'900',height:'350'})");

		Element setElement1 = charElement.addElement("set");
		setElement1.addAttribute("value", count1);
		setElement1.addAttribute("label", "市场部");
		
		Element setElement2 = charElement.addElement("set");
		setElement2.addAttribute("value", count2);
		setElement2.addAttribute("label", "财务部");
		
		Element setElement3 = charElement.addElement("set");
		setElement3.addAttribute("value", count3);
		setElement3.addAttribute("label", "研发部");
		
		Element setElement4 = charElement.addElement("set");
		setElement4.addAttribute("value", count4);
		setElement4.addAttribute("label", "后勤部");
		
		result = document.asXML();
		return result;
	}
}

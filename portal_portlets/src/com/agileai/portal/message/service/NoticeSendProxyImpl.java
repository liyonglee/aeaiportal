package com.agileai.portal.message.service;

import java.util.List;

import javax.jws.WebService;

import com.agileai.common.AppConfig;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.portal.bizmoduler.notice.ExtMsgContentManage;
import com.agileai.portal.bizmoduler.notice.SecurityUserQuery;
import com.agileai.portal.message.MessageHandler;

@WebService(serviceName = "NoticeSend", endpointInterface = "com.agileai.portal.message.service.NoticeSendProxy")
public class NoticeSendProxyImpl implements NoticeSendProxy {
	private MessageHandler messageHandler = null;
	private AppConfig appConfig = null;
	
	public NoticeSendProxyImpl(){
		
	}
	
	private MessageHandler getMessageHandler(){
		if (this.messageHandler == null){
			this.messageHandler = new MessageHandler(this.appConfig);
		}
		return this.messageHandler;
	}
	
	@Override
	public void sendByIds(List<String> userIds, String msgTheme,
			String msgContent, String senderId) {
		
		String[] _usercodes = getUserService().getUserByIds(userIds);
		doSaveMsg(listToString(userIds), msgTheme, msgContent, senderId);
		
		getMessageHandler().publishNotice(_usercodes);
	}

	@Override
	public void sendByRoles(List<String> roleIds, String msgTheme,
			String msgContent, String senderId) {
		
		DataRow returnValue = getUserService().getUserByRoleId(roleIds); 
		String[] _usercodes = (String[]) returnValue.get("userCodes");
		String[] _userids = (String[]) returnValue.get("userIds");
		doSaveMsg(converToString(_userids), msgTheme, msgContent, senderId);
		
		getMessageHandler().publishNotice(_usercodes);
	}

	@Override
	public void sendByGroups(List<String> groupIds, String msgTheme,
			String msgContent, String senderId) {
		
		DataRow returnValue = getUserService().getUserByGroupId(groupIds);
		String[] _usercodes = (String[]) returnValue.get("userCodes");
		String[] _userids = (String[]) returnValue.get("userIds");
		doSaveMsg(converToString(_userids), msgTheme, msgContent, senderId);
		
		getMessageHandler().publishNotice(_usercodes);
	}
	
	private void doSaveMsg(String usersids, String msgTheme, String msgContent, String senderCode) {
		DataParam param = new DataParam();
		param.put("MSG_THEME", msgTheme);
		param.put("MSG_RECEIVEOR_ID", usersids);
		param.put("MSG_NOTICE_CONTENT", msgContent);
		param.put("MSG_SENDER_CODE", senderCode);
		
		getNoticeService().createRecord(param);
	}

	private String listToString(List<String> list) {  
	    StringBuilder sb = new StringBuilder();  
	    if (list != null && list.size() > 0) {  
	        for (int i = 0; i < list.size(); i++) {  
	            if (i < list.size() - 1) {  
	                sb.append(list.get(i) + ",");  
	            } else {  
	                sb.append(list.get(i));  
	            }  
	        }  
	    }  
	    return sb.toString();  
	} 
	
	private String converToString(String[] ig) {
	    String str = "";  
	    if (ig != null && ig.length > 0) {  
	        for (int i = 0; i < ig.length; i++) {  
	            str += ig[i] + ",";  
	        }  
	    }  
	    str = str.substring(0, str.length() - 1);  
	    return str;  
	}  
	
    protected SecurityUserQuery getUserService() {
        return (SecurityUserQuery) BeanFactory.instance().getBean("securityUserQueryService");
    }
    
    protected ExtMsgContentManage getNoticeService() {
    	return (ExtMsgContentManage) BeanFactory.instance().getBean("extMsgContentManageService");
    }

	@Override
	public void kickoutOnline(String userId, String loginTime) {
		getMessageHandler().publishKickout(userId, loginTime);
	}

	public AppConfig getAppConfig() {
		return appConfig;
	}

	public void setAppConfig(AppConfig appConfig) {
		this.appConfig = appConfig;
	}
}

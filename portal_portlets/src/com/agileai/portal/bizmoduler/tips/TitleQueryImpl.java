package com.agileai.portal.bizmoduler.tips;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.util.MapUtil;

public class TitleQueryImpl extends StandardServiceImpl implements TitleQuery{
    public TitleQueryImpl() {
    	 super();
    }

	@Override
	public String getTitle(DataParam param) {
		String title = "";
		String type = param.get("type");
		if ("portlet".equals(type)) {
			DataRow row = queryPortletTemptRecord(param);
			if (!MapUtil.isNullOrEmpty(row)){
				title = row.getString("TEMPT_NAME");				
			}
		}else if("page".equals(type)){
			DataRow row = queryPageTemptRecord(param);
			if (!MapUtil.isNullOrEmpty(row)){
				title = row.getString("TEMPT_NAME");				
			}
		}else if("data".equals(type)){
			DataRow row = queryStaticDataRecord(param);
			if (!MapUtil.isNullOrEmpty(row)){
				title = row.getString("DATA_NAME");	
			}
		}else if("menu".equals(type)){
			DataRow row = queryMenuDataRecord(param);
			if (!MapUtil.isNullOrEmpty(row)){
				title = row.getString("MENU_NAME");			
			}
		}else if("column".equals(type)){
			DataRow row = queryColumnRecord(param);
			if (!MapUtil.isNullOrEmpty(row)){
				title = row.getString("COL_NAME");
			}
		}else if("content".equals(type)){
			DataRow row = queryContentRecord(param);
			if (!MapUtil.isNullOrEmpty(row)){
				title = row.getString("INFO_TITLE");
			}
		}
		return title;
	}

	@Override
	public DataRow queryPortletTemptRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getPortletTemptRecord";
		return this.daoHelper.getRecord(statementId, param);
	}

	@Override
	public DataRow queryPageTemptRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getPageTemptRecord";
		return this.daoHelper.getRecord(statementId, param);
	}

	@Override
	public DataRow queryColumnRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getColumnRecord";
		return this.daoHelper.getRecord(statementId, param);
	}

	@Override
	public DataRow queryContentRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getContentRecord";
		return this.daoHelper.getRecord(statementId, param);
	}

	@Override
	public DataRow queryStaticDataRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getStaticDataRecord";
		return this.daoHelper.getRecord(statementId, param);
	}
	
	@Override
	public DataRow queryMenuDataRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getMenuRecord";
		return this.daoHelper.getRecord(statementId, param);
	}	
}
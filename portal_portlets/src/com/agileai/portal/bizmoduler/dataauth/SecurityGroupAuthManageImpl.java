package com.agileai.portal.bizmoduler.dataauth;

import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.portal.bizmoduler.dataauth.SecurityGroupAuthManage;

public class SecurityGroupAuthManageImpl
        extends StandardServiceImpl
        implements SecurityGroupAuthManage {
	
    public SecurityGroupAuthManageImpl() {
        super();
    }

	@Override
	public void saveGroupAuth(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertRecord";
		String _groupIds = param.get("groupIds");
		String[] groupArray = _groupIds.split(",");
		this.deletRecord(param);
		
		for (int i = 0; i < groupArray.length; i++) {
			param.put("GRP_AUTH_ID", KeyGenerator.instance().genKey());
			param.put("GRP_ID", groupArray[i]);
			this.daoHelper.insertRecord(statementId, param);
		}
		
	}

	@Override
	public void delGroupAuth(DataParam param) {
		super.deletRecord(param);
	}
    
}

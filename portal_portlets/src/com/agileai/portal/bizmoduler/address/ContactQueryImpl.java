package com.agileai.portal.bizmoduler.address;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class ContactQueryImpl extends StandardServiceImpl implements ContactQuery{
    public ContactQueryImpl() {
    	 super();
    }

	@Override
	public List<DataRow> findContactRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"findContactRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}

	@Override
	public DataRow queryCurrentRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"getContactRecord";
		return this.daoHelper.getRecord(statementId, param);
	}
    
}

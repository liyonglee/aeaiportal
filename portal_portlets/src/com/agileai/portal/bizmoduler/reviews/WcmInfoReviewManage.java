package com.agileai.portal.bizmoduler.reviews;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface WcmInfoReviewManage
        extends StandardService {
	DataRow getInfoRecord(DataParam param);
}

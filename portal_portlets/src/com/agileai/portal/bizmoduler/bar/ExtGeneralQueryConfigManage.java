package com.agileai.portal.bizmoduler.bar;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface ExtGeneralQueryConfigManage
        extends StandardService {
	public int getMaxSortNO(DataParam param);
	public void updateSortNO(List<DataParam> params);
	
	public void saveDefaultValues(String id,String defaultValues);
}

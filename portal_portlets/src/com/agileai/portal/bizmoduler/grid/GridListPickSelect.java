package com.agileai.portal.bizmoduler.grid;

import com.agileai.hotweb.bizmoduler.core.PickFillModelService;

public interface GridListPickSelect
        extends PickFillModelService {
	void copyGridListRecords(String selectPageId,String selectPortletId,String currentPageId,String currentPortletId);
	void copyGridListRecords(String selectPageId,String selectPortletId,String currentGridId);
}

package com.agileai.portal.bizmoduler.notice;

import java.util.List;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeSelectService;

public abstract interface SecurityUserTreeSelect extends TreeSelectService {
	
	public abstract List<DataRow> findChildGroupRecords(String paramString);
	
	public List<DataRow> retrieveUserList(String resourceType,String resourceId);
}